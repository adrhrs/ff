
<?php
/*
* File Name: employee_model.php
*/

if (!defined('BASEPATH')) exit('No direct script access allowed');
class mdl extends CI_Model

{
  function __construct()
  {

    // Call the Model constructor

    parent::__construct();
  }

  public

  function insertData($table, $data)
  {
    $this->db->insert($table, $data);
  }

  public

  function insertDataGetLast($table, $data)
  {
    $this->db->insert($table, $data);
    if ($this->db->affected_rows()) {
      return $this->db->insert_id();
    }
    else {
      return false;
    }
  }

  public

  function updateData($param, $value, $table, $data)
  {
    $this->db->where($param, $value);
    $this->db->update($table, $data);
  }

  public

  function deleteData($param, $value, $table)
  {
    $this->db->where($param, $value);
    $this->db->delete($table);
  }

  public

  function checkAccount($username, $password)
  {
    $sql = "SELECT * from user where username = '$username' and password = '$password'";
    $query = $this->db->query($sql);
    if ($query->num_rows() == 1) {
      return $query->result();
    }
    else {
      return false;
    }
  }

  // SELECT TJOY

  public

  function getProdusenList()
  {
    $sql = "SELECT * FROM produsen where nama_produsen not like '% Produsen Deleted %' order by nama_produsen asc";
    $result = $this->db->query($sql);
    $return = array();
    if ($result->num_rows() > 0) {
      foreach($result->result_array() as $row) {
        $return[$row['id_produsen']] = $row['nama_produsen'];
      }
    }

    return $return;
  }

  public

  function getProduk()
  {
    $sql = "SELECT *,DATE_FORMAT(a.added_on, '%d %M %Y %T') AS tgl from produk a, produsen b where a.tipe = 'produk' and a.id_produsen = b.id_produsen order by id_produk asc ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public function findProduk($param) {
    $sql = "SELECT *,DATE_FORMAT(a.added_on, '%d %M %Y %T') AS tgl from produk a, produsen b where a.nama not like '%( Product Deleted )%' and a.id_produk = '$param' and a.id_produsen = b.id_produsen

      union

      SELECT *,DATE_FORMAT(a.added_on, '%d %M %Y %T') AS tgl from produk a, produsen b where a.nama not like '%( Product Deleted )%' and a.nama like  '%$param%' and a.id_produsen = b.id_produsen
    ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  // public

  // function cekHapusProduk($id_produk)
  // {
  //   $sql = "SELECT id_produk, 'Jumlah Transaksi' as status, count(id_line) as jml from line_sales where id_produk = $id_produk group by id_produk
  //               union 
  //               SELECT id_produk, 'Jumlah Harga' as status, count(id_agen) as jml from harga_agen  where id_produk = $id_produk group by id_produk";
  //   $query = $this->db->query($sql);
  //   return $query->result();
  // }

  function cekHapusProduk($id_produk)
  {
    $sql = "SELECT * from line_sales where id_produk = $id_produk UNION SELECT * from line_purchase where id_produk = $id_produk UNION SELECT * from inventory where id_produk = $id_produk";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getProdukByID($id)
  {
    $sql = "SELECT *,DATE_FORMAT(a.added_on, '%d %M %Y %T') AS tgl from produk a, produsen b where a.tipe = 'produk' and a.id_produsen = b.id_produsen and id_produk = '$id' order by added_on desc ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getAgen()
  {
    $sql = "SELECT * from agen where nama_agen not like '%( Agen Deleted )%'  order by id_agen ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getSupplier()
  {
    $sql = "SELECT * from supplier where nama_supplier not like '%( Supplier Deleted )%' order by id_supplier ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getAkun()
  {
    $sql = "SELECT * from user ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getProdusen()
  {
    $sql = "SELECT * FROM produsen where nama_produsen not like '% Produsen Deleted %' order by nama_produsen asc";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function getProdusenByID($id_produsen)
  {
    $sql = "SELECT * from produsen where id_produsen = $id_produsen ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getAgenByID($id_agen)
  {
    $sql = "SELECT * from agen where id_agen = $id_agen ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getSupplierByID($id_supplier)
  {
    $sql = "SELECT * from supplier where id_supplier = $id_supplier ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getHargaAgenByID($id_agen)
  {
    $sql = "SELECT * from agen a, harga_agen b, produk c where c.tipe = 'produk' and  a.id_agen = $id_agen and a.id_agen = b.id_agen and b.id_produk = c.id_produk ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function cekharga($id_agen, $id_produk)
  {
    $sql = "SELECT * from harga_agen where id_agen = $id_agen and id_produk = $id_produk ";
    $query = $this->db->query($sql);
    if ($query->num_rows() == 0) {
      return true;
    }
    else {
      return false;
    }
  }

  public

  function getLastSalesID($tipe)
  {
    $sql = "SELECT * from sales_order where tipe = '$tipe' order by id_sales desc limit 1 ";
    $query = $this->db->query($sql);
    if ($query->num_rows() == 0) {
      return 0;
    }
    else {
      return $query->result() [0]->id_sales;
    }
  }

  public

  function getLastSalesStatus($tipe)
  {
    $sql = "SELECT * from sales_order where tipe = '$tipe' order by id_sales desc limit 1 ";
    $query = $this->db->query($sql);
    if ($query->num_rows() == 0) {
      return "close";
    }
    else {
      return $query->result() [0]->status;
    }
  }

  public

  function getLastPurchaseStatus()
  {
    $sql = "SELECT * from purchase_order order by id_purchase desc limit 1 ";
    $query = $this->db->query($sql);
    if ($query->num_rows() == 0) {
      return "close";
    }
    else {
      return $query->result() [0]->status;
    }
  }

  public

  function getLastPurchaseID()
  {
    $sql = "SELECT * from purchase_order order by id_purchase desc limit 1 ";
    $query = $this->db->query($sql);
    if ($query->num_rows() == 0) {
      return 0;
    }
    else {
      return $query->result() [0]->id_purchase;
    }
  }

  public

  function getShoppingCart($id_sales)
  {
    $sql = "SELECT *,DATE_FORMAT(a.tanggal, '%d %M %Y %T') AS tgl, DATE_FORMAT(a.tanggal, '%Y') AS tahun, DATE_FORMAT(a.tanggal, '%m') AS bulan,DATE_FORMAT(a.tanggal, '%d/%m/%Y') AS tgl2,DATE_FORMAT(a.tanggal, '%M/ %Y') AS tgl3 FROM sales_order a, line_sales b, produk c, finance f, inventory i where b.id_inventory = i.id_inventory and a.id_finance = f.id_finance and a.id_sales = b.id_sales and b.id_produk = c.id_produk and a.id_sales = $id_sales order by b.id_line";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getShoppingCartPurchase($id_purchase)
  {
    $sql = "SELECT *,DATE_FORMAT(a.tanggal, '%d %M %Y %T') AS tgl FROM purchase_order a, line_purchase b, produk c, finance f, inventory i where b.id_inventory = i.id_inventory and a.id_finance = f.id_finance and a.id_purchase = b.id_purchase and b.id_produk = c.id_produk and a.id_purchase = $id_purchase order by b.id_line";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getLineByID($id_line)
  {
    $sql = "SELECT * FROM sales_order a, line_sales b, produk c, inventory i where b.id_inventory = i.id_inventory and a.id_sales = b.id_sales and b.id_produk = c.id_produk  and b.id_line = $id_line order by b.id_line";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getLinePurchaseByID($id_line)
  {
    $sql = "SELECT * FROM purchase_order a, line_purchase b, produk c, inventory i where b.id_inventory = i.id_inventory and a.id_purchase = b.id_purchase and b.id_produk = c.id_produk  and b.id_line = $id_line order by b.id_line";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getSalesByID($id_sales)
  {
    $sql = "SELECT * FROM sales_order a join finance f on a.id_finance = f.id_finance where a.id_sales = $id_sales";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getPurchaseByID($id_purchase)
  {
    $sql = "SELECT * FROM purchase_order a join finance f on a.id_finance = f.id_finance where a.id_purchase = $id_purchase";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getKasirTotal($id_sales)
  {
    $sql = "SELECT ifnull(a.id_sales,0) as id_sales, ifnull(f.pajak,0) as pajak, ifnull(sum(qty),0) as qty, ifnull(sum(((100-diskon_produk)/100)*harga*qty),0) as harga FROM `line_sales` a join sales_order b on a.id_sales = b.id_sales join finance f on b.id_finance = f.id_finance join inventory i on a.id_inventory = i.id_inventory where a.id_sales = $id_sales";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getKasirTotalPurchase($id_purchase)
  {
    $sql = "SELECT ifnull(a.id_purchase,0) as id_purchase, ifnull(f.pajak,0) as pajak, ifnull(sum(qty),0) as qty, ifnull(sum(((100-diskon_produk)/100)*harga*qty),0) as harga, ifnull(((sum(((100-diskon_produk)/100)*harga*qty))+(pajak/100)*(sum(((100-diskon_produk)/100)*harga*qty))),0) as hargapajak FROM `line_purchase` a join purchase_order b on a.id_purchase = b.id_purchase join finance f on b.id_finance = f.id_finance join inventory i on a.id_inventory = i.id_inventory where a.id_purchase = $id_purchase";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getPenjualanKasir($limit, $date_start, $date_finish)
  {

    $sql = "SELECT *,DATE_FORMAT(a.tanggal, '%d %M %Y %T') AS tgl from sales_order a join finance f on a.id_finance = f.id_finance join (SELECT id_sales, sum(qty) as qty, sum(((100-diskon_produk)/100)*harga*qty) as harga from line_sales b join inventory i on b.id_inventory = i.id_inventory  group by id_sales) b on a.id_sales = b.id_sales WHERE a.tipe = 'kasir' and a.status = 'close' and a.tanggal >= '".$date_start."' and a.tanggal <= '".$date_finish."' ORDER by a.tanggal desc LIMIT $limit";
    
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getPenjualanKasirByID($id_sales)
  {
    $sql = "SELECT *, DATE_FORMAT( a.tanggal, '%d %M %Y %T' ) AS tgl, ((100-discount)/100)*harga as harga2 FROM sales_order a JOIN finance f ON a.id_finance = f.id_finance JOIN ( SELECT id_sales, sum( qty ) AS qty, sum( ( ( 100- diskon_produk ) / 100 ) * harga * qty ) AS harga FROM line_sales b JOIN inventory i ON b.id_inventory = i.id_inventory GROUP BY id_sales ) b ON a.id_sales = b.id_sales WHERE a.STATUS = 'close' AND a.id_sales = $id_sales ORDER BY a.tanggal DESC";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getPenjualanAgen($limit, $date_start, $date_finish)
  {
    $sql = "SELECT *,DATE_FORMAT(a.tanggal, '%d %M %Y %T') AS tgl from sales_order a join finance f on a.id_finance = f.id_finance  join (SELECT id_sales, sum(qty) as qty, sum(((100-diskon_produk)/100)*harga*qty) as harga from line_sales b join inventory i on b.id_inventory = i.id_inventory  group by id_sales) b on a.id_sales = b.id_sales JOIN agen c on a.id_agen = c.id_agen WHERE a.tipe = 'so' and a.status = 'close'  and a.tanggal >= '".$date_start."' and a.tanggal <= '".$date_finish."'  ORDER by a.tanggal desc  LIMIT $limit";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function generateDate($date) {
    $year = substr($date,4,4);
    $month = substr($date,2,2);
    $date = substr($date,0,2);

    return $year.'-'.$month.'-'.$date;
  }

  function getPenjualanAgenFiltered($id_agen, $date_start, $date_finish, $page, $limit)
  {

    $where_agen = "";
    if($id_agen != '0') {
      $where_agen = "AND a.id_agen = $id_agen";
    }

    $offset = ($page-1)*$limit;

    $where_start = "";
    if($date_start != '0') {
      $date = $this->generateDate($date_start);
      $where_start = "AND a.tanggal  >= '$date'";
    }

    $where_finish = "";
    if($date_finish != '0') {
      $date = $this->generateDate($date_finish);
      $where_finish = "AND a.tanggal  <= '$date'";
    }

    

    $sql = "SELECT *,
            DATE_FORMAT(a.tanggal, '%d %M %Y %T') AS tgl
      FROM sales_order a
      JOIN finance f ON a.id_finance = f.id_finance
      JOIN
        (SELECT id_sales,
                sum(qty) AS qty,
                sum(((100-diskon_produk)/100)*harga*qty) AS harga
        FROM line_sales b
        JOIN inventory i ON b.id_inventory = i.id_inventory
        GROUP BY id_sales) b ON a.id_sales = b.id_sales
      JOIN agen c ON a.id_agen = c.id_agen
      WHERE a.tipe = 'so'
        AND a.status = 'close'
        ". $where_agen ."
        ". $where_start ."
        ". $where_finish ."

      ORDER BY a.tanggal DESC

      LIMIT $offset, $limit
    
    ";
    
    $query = $this->db->query($sql);
    return $query->result();
  }



  public

  function getPembelianSupplier($limit, $date_start, $date_finish)
  {
    $sql = "SELECT *,DATE_FORMAT(a.tanggal, '%d %M %Y') AS tgl, DATE_FORMAT(TIMESTAMPADD(DAY,3,a.tanggal),'%d %M %Y') as deadline, datediff(TIMESTAMPADD(DAY,3,a.tanggal),NOW()) as sisa from purchase_order a join finance f on a.id_finance = f.id_finance  join (SELECT id_purchase, sum(qty) as qty, sum(((100-diskon_produk)/100)*harga*qty) as harga from line_purchase b join inventory i on b.id_inventory = i.id_inventory  group by id_purchase) b on a.id_purchase = b.id_purchase JOIN supplier c on a.id_supplier = c.id_supplier WHERE a.tipe = 'po' and a.status = 'close' and a.tanggal >= '".$date_start."' and a.tanggal <= '".$date_finish."' ORDER by a.tanggal desc LIMIT $limit";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function getPembelianFiltered($id_supplier, $date_start, $date_finish, $page, $limit)
  {

    $where_sup = "";
    if($id_supplier != '0') {
      $where_sup = "AND a.id_supplier = $id_supplier";
    }

    $offset = ($page-1)*$limit;

    $where_start = "";
    if($date_start != '0') {
      $date = $this->generateDate($date_start);
      $where_start = "AND a.tanggal  >= '$date'";
    }

    $where_finish = "";
    if($date_finish != '0') {
      $date = $this->generateDate($date_finish);
      $where_finish = "AND a.tanggal  <= '$date'";
    }

    

    $sql = "SELECT *,
             DATE_FORMAT(a.tanggal, '%d %M %Y') AS tgl,
             DATE_FORMAT(TIMESTAMPADD(DAY, 3, a.tanggal), '%d %M %Y') AS deadline,
             datediff(TIMESTAMPADD(DAY, 3, a.tanggal), NOW()) AS sisa
      FROM purchase_order a
      JOIN finance f ON a.id_finance = f.id_finance
      JOIN
        (SELECT id_purchase,
                sum(qty) AS qty,
                sum(((100-diskon_produk)/100)*harga*qty) AS harga
         FROM line_purchase b
         JOIN inventory i ON b.id_inventory = i.id_inventory
         GROUP BY id_purchase) b ON a.id_purchase = b.id_purchase
      JOIN supplier c ON a.id_supplier = c.id_supplier
      WHERE a.tipe = 'po'
        AND a.status = 'close'

        ". $where_sup ."
        ". $where_start ."
        ". $where_finish ."


      ORDER BY a.tanggal DESC
      LIMIT $offset, $limit
    
    ";
    
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getPenjualanAgenByID($id_agen)
  {
    $sql = "SELECT *,DATE_FORMAT(a.tanggal, '%d %M %Y %T') AS tgl from sales_order a join (SELECT id_sales, sum(qty) as qty, sum(((100-diskon_produk)/100)*harga*qty) as harga from line_sales b join inventory i on b.id_inventory = i.id_inventory group by id_sales) b on a.id_sales = b.id_sales JOIN agen c on a.id_agen = c.id_agen join finance f on a.id_finance = f.id_finance WHERE c.id_agen = $id_agen and a.tipe = 'so' and a.status = 'close' ORDER by a.tanggal desc";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getSaltipAgen($id_agen)
  {
    $sql = "SELECT sum(bayar-((harga + (pajak/100)*harga)-discount)-kembalian ) as saltip from sales_order a join finance f on a.id_finance = f.id_finance join (SELECT id_sales, sum(qty) as qty, sum(((100-diskon_produk)/100)*harga*qty) as harga from line_sales b join inventory i on b.id_inventory = i.id_inventory  group by id_sales) b on a.id_sales = b.id_sales JOIN agen c on a.id_agen = c.id_agen WHERE a.tipe = 'so' and a.status = 'close' and a.id_agen = $id_agen 

        UNION

        SELECT sum(bayar) from finance where id_people = $id_agen and tipe_transaction = 'cicil' group by id_people";
    $query = $this->db->query($sql);
    return $query->result();
  }


  public

  function getHargaSpesial()
  {
    $sql = "SELECT * from harga_agen";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getHargaSpesialDetail($id_produk, $id_agen)
  {
    $sql = "SELECT * from harga_agen where id_agen = $id_agen and id_produk = $id_produk";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getCicilan($limit)
  {
    $sql = "SELECT *,DATE_FORMAT(f.tanggal, '%d %M %Y %T') AS tgl FROM finance f, agen a where f.id_people = a.id_agen and f.tipe_transaction = 'cicil' order by f.tanggal desc limit $limit";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getCicilanAgenByID($id_agen)
  {
    $sql = "SELECT *,DATE_FORMAT(f.tanggal, '%d %M %Y %T') AS tgl FROM finance f, agen a where f.id_people = a.id_agen and f.tipe_transaction = 'cicil' and f.id_people = $id_agen order by f.tanggal desc";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getMovementSales()
  {
    $sql = "SELECT *, DATE_FORMAT(i.tanggal, '%d %M %Y %T') AS tgl from inventory i join produk p on i.id_produk = p.id_produk join line_sales l on i.id_line = l.id_line where i.tipe_inventory = 'so' or i.tipe_inventory = 'kasir' order by i.tanggal desc limit 100";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getMovementPurchase()
  {
    $sql = "SELECT *, DATE_FORMAT(i.tanggal, '%d %M %Y %T') AS tgl from inventory i join produk p on i.id_produk = p.id_produk join line_purchase l on i.id_line = l.id_line where i.tipe_inventory = 'po' order by i.tanggal desc limit 100";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getStockOpname()
  {
    $sql = 
    
    "SELECT *,
          DATE_FORMAT(s.tanggal, '%d %M %Y %T') AS tgl,
          p.id_produk AS idp
      FROM line_sales l
      JOIN produk p ON l.id_produk = p.id_produk
      JOIN sales_order s ON s.id_sales = l.id_sales
      JOIN agen a ON s.id_agen = a.id_agen
      JOIN inventory i ON i.id_line = l.id_line
      ORDER BY s.tanggal DESC
      LIMIT 1

      ";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getSetting()
  {
    $sql = "SELECT * from setting";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getTrackProduk()
  {
    $sql = "SELECT *, DATE_FORMAT(s.tanggal, '%d %M %Y %T') AS tgl, p.id_produk as idp from line_sales l 
    join produk p on l.id_produk = p.id_produk 
    join sales_order s on s.id_sales = l.id_sales 
    join agen a on s.id_agen = a.id_agen 
    join inventory i on i.id_line = l.id_line 
    order by s.tanggal desc";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getStockList($limit)
  {
    $sql = "

      SELECT X.idp AS id_produk, X.nama, inQty, outQty, (inQty - outQty) AS stockNow 
      
      FROM 
      
      ( SELECT p.id_produk AS idp, p.nama, IFNULL(inQty, 0) AS inQty 
        FROM 
        
        ( SELECT id_produk, SUM(qty) AS inQty FROM inventory WHERE direction = 'in' GROUP BY id_produk ) i 
        RIGHT JOIN produk p ON i.id_produk = p.id_produk ) X, 

      ( SELECT w.id_produk AS idp2, w.nama, IFNULL(outQty, 0) AS outQty 
        FROM 

        ( SELECT id_produk, SUM(qty) AS outQty FROM inventory WHERE direction = 'out' GROUP BY id_produk ) q 
        RIGHT JOIN produk w ON q.id_produk = w.id_produk ) Y 


      WHERE X .idp = Y.idp2 ORDER by stockNow asc limit $limit";
    $query = $this->db->query($sql);
    return $query->result();
  }

  public

  function getStockProduct($id_produk)
  {
    $sql = "

      SELECT X.idp AS id_produk, X.nama, inQty, outQty, (inQty - outQty) AS stockNow 
      
      FROM 
      
      ( SELECT p.id_produk AS idp, p.nama, IFNULL(inQty, 0) AS inQty 
        FROM 
        
        ( SELECT id_produk, SUM(qty) AS inQty FROM inventory WHERE direction = 'in' GROUP BY id_produk ) i 
        RIGHT JOIN produk p ON i.id_produk = p.id_produk ) X, 

      ( SELECT w.id_produk AS idp2, w.nama, IFNULL(outQty, 0) AS outQty 
        FROM 

        ( SELECT id_produk, SUM(qty) AS outQty FROM inventory WHERE direction = 'out' GROUP BY id_produk ) q 
        RIGHT JOIN produk w ON q.id_produk = w.id_produk ) Y 


      WHERE X.idp = Y.idp2 and X.idp = $id_produk";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function getCashFlow()
  {
    $sql = "SELECT *, DATE_FORMAT(tanggal, '%d %M %Y %T') AS tgl from finance order by tanggal desc limit 100";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function getListPajakProduct()
  {

    $sql = "SELECT * from setting where id_setting = 3 or id_setting = 4";
    $query = $this->db->query($sql);
    $res = $query->result();
    $ids = $res[0]->value_str;
    $idp = $res[1]->value_str;

    $sql = "SELECT * FROM (SELECT DISTINCT b.id_produk FROM purchase_order a, line_purchase b WHERE a.id_supplier in (".$ids.") and b.id_purchase = a.id_purchase) x WHERE x.id_produk NOT IN (".$idp.")";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function getSupplierProduct()
  {

    $sql = "SELECT a.id_supplier, b.id_produk,c.nama, count(a.id_purchase) as 'po_count', GROUP_CONCAT(a.id_purchase) as 'po_list' FROM purchase_order a, line_purchase b, produk c where b.id_purchase = a.id_purchase and  b.id_produk = c.id_produk group by a.id_supplier, b.id_produk";
    $query = $this->db->query($sql);
    return $query->result();
  }


}
