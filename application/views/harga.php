<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Agen</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Kelola Detil Agen </h2>
                        <p class="font-bold">Halaman ini bertujuan untuk mengolah agen</p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    
                    <div class="row">
                        <div class="col-lg-6 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Informasi Agen</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-7">

                                            <label>Nama Agen</label>
                                            <br>
                                            <?php echo $agen[0]->nama_agen ?>
                                            <br><br>

                                            <div class="row">

                                                <div class="col-lg-5">

                                                    <label>Kategori</label>
                                                    <br>
                                                    <?php echo $agen[0]->kategori ?>
                                                    <br><br>

                                                </div>

                                                <div class="col-lg-7">
                                                    
                                                    <label>Nomor HP</label>
                                                    <br>
                                                    <?php echo $agen[0]->nomor ?>
                                                    <br><br>
                                                
                                                </div>
                                            </div>

                                            <label>Alamat</label>
                                            <br>
                                            <?php echo $agen[0]->alamat ?>
                                            
                                        </div>
                                        <div class="col-lg-5">


                                                <?php 

                                                    $totq = 0;
                                                    for ($i=0; $i < count($so); $i++) { 

                                                        $totawal = (int)$so[$i]->harga;
                                                        $dscx = (int)$so[$i]->discount;

                                                        $dscval = ($dscx/100)*$totawal;
                                                        $totx = $totawal - $dscval;

                                                        $pjk = (int)$so[$i]->pajak;
                                                        $pjkval = ($pjk/100)*$totx;
                                                        $grandtot = $totx + $pjkval;

                                                        $eue = (int)$so[$i]->bayar-($grandtot)-(int)$so[$i]->kembalian;
                                                        $totq += $eue;
                                                        
                                                    }

                                                    for ($i=0; $i < count($ci); $i++) { 
                                                        
                                                        $cil = (int)$ci[$i]->bayar;
                                                        $totq += $cil;
                                                    }

                                                    $stat = 'Saldo Agen';

                                                    if($totq > 0) {
                                                        $stat = 'Nitip Agen';
                                                    }


                                                ?>

                                                <label><?php echo $stat ?></label>
                                                <h2><?php echo "Rp " . number_format(abs($totq),0,',','.');  ?></h2>

                                            
                                        </div>
                                    </div>
                                </div>
                            
                            </div>

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Transaksi Agen</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter4" placeholder="Search in table">
                                            <table class="footable table table-stripped" data-page-size="10" data-filter=#filter4>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">ID</th>
                                                        
                                                        
                                                        <th class="text-center">Total</th>
                                                        <th class="text-center">Saldo (-) / Nitip (+)</th>
                                                        
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>


                                                <tbody>
                                                    
                                                    <?php for($i=0; $i < count($so); ++$i) { ?>

                                                        <tr>
                                                            <td class="text-center"><?php echo $so[$i]->id_sales ?></td>
                                                            
                                                            
                                                            <td class="text-center">

                                                                <?php 

                                                                $totawal = (int)$so[$i]->harga;
                                                                $dscx = (int)$so[$i]->discount;

                                                                $dscval = ($dscx/100)*$totawal;
                                                                $totx = $totawal - $dscval;

                                                                $pjk = (int)$so[$i]->pajak;
                                                                $pjkval = ($pjk/100)*$totx;
                                                                $grandtot = $totx + $pjkval;

                                                                ?>
                                                    
                                                                <?php 

                                                                    echo "Rp " . number_format($grandtot,0,',','.'); 

                                                                ?>
                                                                    
                                                            </td>
                                                            <td class="text-center"><?php 

                                                                $x = (int)$so[$i]->bayar-($grandtot)-(int)$so[$i]->kembalian;
                                                                echo "Rp " . number_format($x,0,',','.');;

                                                            ?></td>
                                                    <td class="text-center"> 
                                                        
                                                    
                                                                <a class="btn btn-xs btn-primary" href="<?php echo base_url('Apps/nota/so/'.$so[$i]->id_sales) ?>"><span class="fa fa-eye"></span></a>
                                                                <a class="btn btn-xs btn-warning" href="<?php echo base_url('Apps/tambahSO/ops/'.$so[$i]->id_sales) ?>"><span class="fa fa-pencil"></span></a>
                                                                <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#hapus<?php echo $so[$i]->id_sales ?>" ><span class="fa fa-trash"></span></a>

                                                                <div class="modal inmodal" id="hapus<?php echo $so[$i]->id_sales ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/hapusNota')?>

                                                                            <div class="modal-body text-center">
                                                                                <h1>Apakah anda yakin untuk menghapus transaksi <b><?php echo $so[$i]->id_sales ?> ?</b> </h1>
                                                                                <br>
                                                                                <small>Anda tidak dapat mengembalikan transaksi yang telah terhapus</small>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id_sales" value="<?php echo $so[$i]->id_sales ?>">
                                                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                            <?php echo form_close() ?>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    <?php } ?>

                                                </tbody>



                                                <tfoot>
                                                <tr>
                                                    <td colspan="10">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>

                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Cicilan Agen</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter3" placeholder="Search in table">
                                            <table class="footable table table-stripped" data-page-size="10" data-filter=#filter3>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">ID</th>
                                                        <th class="text-center">Tanggal</th>
                                                        <th class="text-center">Total Cicilan</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>


                                                <tbody>
                                                    
                                                    <?php for($i=0; $i < count($ci); ++$i) { ?>

                                                        <tr>
                                                            <td class="text-center"><?php echo $ci[$i]->id_finance ?></td>
                                                            <td class="text-center"><?php echo $ci[$i]->tanggal ?></td>
                                                            <td class="text-center"><?php echo "Rp " . number_format($ci[$i]->bayar,0,',','.'); ?></td>
                                                            <td class="text-center"> 
                                                        
                                                                <a class="btn btn-xs btn-warning" data-toggle="modal" data-target="#editFinance<?php echo $ci[$i]->id_finance ?>" ><span class="fa fa-pencil"></span></a>
                                                                <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#hapusFinance<?php echo $ci[$i]->id_finance ?>" ><span class="fa fa-trash"></span></a>

                                                                <div class="modal inmodal" id="editFinance<?php echo $ci[$i]->id_finance ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/editFinance');?>

                                                                            <div class="modal-body">

                                                                                <p class="text-center">Bagian ini memiliki tujuan untuk mengedit saldo agen</p>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-3 text-right">
                                                                                        <h5><b>NAMA AGEN</b></h5><br>
                                                                                        <h5><b>BAYAR</b></h5><br>
                                                                                    </div>
                                                                                    <div class="col-lg-9">
                                                                                        <input  type="text" readonly="" class="form-control input-sm" value="<?php echo $ci[$i]->nama_agen?>"><br>
                                                                                        <input min="0" type="number" class="form-control input-sm" value="<?php echo $ci[$i]->bayar?>" name="bayar" required="">
                                                                                        <input type="hidden" name="id_finance" value="<?php echo $ci[$i]->id_finance?>">
                                                                                        <input type="hidden" name="id_agen" value="<?php echo $id?>">
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-info">Simpan</button>
                                                                                <button type="button" data-toggle="modal" data-target="#cicilan" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                            <?php echo form_close()?>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal inmodal" id="hapusFinance<?php echo $ci[$i]->id_finance ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/hapusFinance')?>

                                                                            <div class="modal-body text-center">
                                                                                <h1>Apakah anda yakin untuk menghapus cicilan ini ? </h1>
                                                                                <br>
                                                                                <small>Anda tidak dapat mengembalikan transaksi yang telah terhapus</small>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id_finance" value="<?php echo $ci[$i]->id_finance ?>">
                                                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                                <input type="hidden" name="id_agen" value="<?php echo $id?>">
                                                                            </div>

                                                                            <?php echo form_close() ?>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </td>
                                                        </tr>

                                                    <?php } ?>

                                                </tbody>



                                                <tfoot>
                                                <tr>
                                                    <td colspan="10">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>

                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-lg-6 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Harga Spesial</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter5" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="7" data-filter=#filter5>
                                                <thead>
                                                    <tr>
                                                        <th>Nama Produk</th>
                                                        <th class="text-center">Harga</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>



                                                    <?php for($i = 0; $i < count($harga); ++$i) { ?>

                                                        <tr>
                                                            
                                                            <td><?php echo $harga[$i]->nama ?></td>
                                                            <td class="text-center"><?php echo "Rp " . number_format($harga[$i]->harga,0,',','.')  ?></td>
                                                            <td class="text-center">
                                                                
                                                                <a data-toggle="modal" data-target="#edit_x<?php echo $harga[$i]->id_harga ?>" href="" class="btn btn-xs btn-warning"><span class="fa fa-pencil"></span></a>

                                                                <a data-toggle="modal" data-target="#hapus<?php echo $harga[$i]->id_harga ?>" href="" class="btn btn-xs btn-danger"><span class="fa fa-trash"></span></a>

                                                                <div class="modal inmodal" id="edit_x<?php echo $harga[$i]->id_harga ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/editHarga') ?>

                                                                            <div class="modal-header">
                                                                                <h1><b>[<?php echo $harga[$i]->id_harga?>]</b> - <?php echo $harga[$i]->nama?></h1>
                                                                            </div>

                                                                            <div class="modal-body">

                                                                                <div class="row">
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Beli</label><br>
                                                                                        <span><?php echo "Rp " . number_format($harga[$i]->harga_beli,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Normal</label><br>
                                                                                        <span><?php echo "Rp " . number_format($harga[$i]->harga_normal,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Agen</label><br>
                                                                                        <span><?php echo "Rp " . number_format($harga[$i]->harga_agen,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Khusus</label><br>
                                                                                        <span><?php echo "Rp " . number_format($harga[$i]->harga_khusus,0,',','.'); ?></span>
                                                                                    </div>
                                                                                </div>
                                                                                <br><br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-4 text-right">
                                                                                        <h4>Harga Spesial</h4>
                                                                                        <p><i><?php echo $agen[0]->nama_agen ?></p></i>
                                                                                    </div>
                                                                                    <div class="col-lg-5">
                                                                                        <input value="<?php echo $harga[$i]->harga ?>" type="number" min="0" name="harga" class="form-control input-lg" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            
                                                                            <div class="modal-footer">
                                                                                <input type="hidden" value="<?php echo $harga[$i]->id_harga?>" name="id_harga">
                                                                                <input type="hidden" value="<?php echo $agen[0]->id_agen?>" name="id_agen">
                                                                                <button type="submit" class="btn btn-success">Simpan</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                            <?php echo form_close()?>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal inmodal" id="hapus<?php echo $harga[$i]->id_harga ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/hapusharga')?>

                                                                            <div class="modal-body text-center">
                                                                                <h1>Apakah anda yakin untuk menghapus harga spesial dari<br> <b><?php echo $harga[$i]->nama ?> ?</b> </h1>
                                                                                <br>
                                                                                <small>Anda tidak dapat mengembalikan produk yang telah terhapus</small>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id_harga" value="<?php echo $harga[$i]->id_harga?>">
                                                                                <input type="hidden" value="<?php echo $agen[0]->id_agen?>" name="id_agen">
                                                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                            <?php echo form_close() ?>

                                                                        </div>
                                                                    </div>
                                                                </div>


                                                            </td>

                                                        </tr>

                                                    <?php } ?>

                                                </tbody>

                                                <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>

                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Cari Produk</h5>
                                </div>

                                <div class="ibox-content">

                                    <?php echo form_open('Apps/cariProduk')?>
                                        
                                        <label>ID Produk / Nama Produk </label>
                                        <input type="text" name="param" class="form-control" required placeholder="ex : 22 atau Chicken Sausage" >
                                        <input type="hidden" name="tipe" value="agen">
                                        <input type="hidden" name="id_sales" value="<?php echo $id?>">

                                        <br>

                                        <div class="text-right">
                                            <button type="submit" class="btn btn-success text-right ">Cari</button>    
                                        </div>
                                        

                                    <?php echo form_close(); ?>

                                </div>
                            </div>

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h5>Hasil Pencarian Produk </h5>        
                                        </div>
                                       
                                    </div>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="10" data-filter=#filter>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Produk</th>
                                                        
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    <?php $prod = $this->session->flashdata('res');?>

                                                    <?php for ($i=0; $i < count($prod); $i++) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center"><?php echo $prod[$i]->id_produk?></td>
                                                            <td><?php echo $prod[$i]->nama?></td>
                                                            
                                                            <td class="text-center">

                                                                <a data-toggle="modal" data-target="#lihat<?php echo $prod[$i]->id_produk ?>" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a>

                                                                <a data-toggle="modal" data-target="#harga<?php echo $prod[$i]->id_produk ?>" class="btn btn-success btn-xs"><span class="fa fa-plus"></span></a>

                                                                
                                                                <div class="modal inmodal" id="lihat<?php echo $prod[$i]->id_produk ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <div class="modal-header">
                                                                                <h1><b>[<?php echo $prod[$i]->id_produk?>]</b> - <?php echo $prod[$i]->nama?></h1>
                                                                            </div>

                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-lg-3">
                                                                                        <label>Berat Produk</label><br>
                                                                                        <span><?php echo $prod[$i]->berat?> gram</span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Isi per Pack</label><br>
                                                                                        <span><?php echo $prod[$i]->isi?> Item</span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Produsen</label><br>
                                                                                        <span><?php echo $prod[$i]->nama_produsen?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Ditambahkan Pada</label><br>
                                                                                        <span><?php echo $prod[$i]->tgl?> </span>
                                                                                    </div>
                                                                                </div>
                                                                                <br><br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Beli</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_beli,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Normal</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_normal,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Agen</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_agen,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Khusus</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_khusus,0,',','.'); ?></span>
                                                                                    </div>

                                                                                    
                                                                                </div>
                                                                                <br><br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <label>Keterangan</label><br>
                                                                                        <span><?php echo $prod[$i]->keterangan?> </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal inmodal" id="harga<?php echo $prod[$i]->id_produk ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/setHarga') ?>

                                                                            <div class="modal-header">
                                                                                <h1><b>[<?php echo $prod[$i]->id_produk?>]</b> - <?php echo $prod[$i]->nama?></h1>
                                                                            </div>

                                                                            <div class="modal-body">

                                                                                <div class="row">
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Beli</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_beli,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Normal</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_normal,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Agen</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_agen,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Khusus</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_khusus,0,',','.'); ?></span>
                                                                                    </div>
                                                                                </div>
                                                                                <br><br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-4 text-right">
                                                                                        <h4>Harga Spesial</h4>
                                                                                        <p><i><?php echo $agen[0]->nama_agen ?></p></i>
                                                                                    </div>
                                                                                    <div class="col-lg-5">
                                                                                        <input placeholder="ex : <?php echo $prod[$i]->harga_khusus-500 ?>" type="number" min="0" name="harga" class="form-control input-lg" >
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            
                                                                            <div class="modal-footer">
                                                                                <input type="hidden" value="<?php echo $agen[0]->id_agen?>" name="id_agen">
                                                                                <input type="hidden" value="<?php echo $prod[$i]->id_produk?>" name="id_produk">
                                                                                <button type="submit" class="btn btn-success">Simpan</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                            <?php echo form_close()?>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                               
                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                          
                        </div>
                      
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();
            $('.footable2').footable();

        });

    </script>


</body>

</html>
