<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Agen</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Kelola Akun</h2>
                        <p class="font-bold">Halaman ini bertujuan untuk mengolah akun seperti mengganti password dalam aplikasi ini, </p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    
                    <div class="row">
                        <div class="col-lg-6 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Informasi Akun</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="form-group">

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Nama</label><br>
                                                <span><?php echo $akun[0]->nama ?></span>

                                            </div>
                                            <div class="col-lg-4">
                                                <label>Username</label><br>
                                                <span><?php echo $akun[0]->username ?></span>

                                            </div>
                                            <div class="col-lg-4">
                                                <label>Password</label><br>
                                                <span><?php echo $akun[0]->password ?></span>

                                            </div>
                                        </div>


                                        

                                    </div>

                                </div>
                            
                            </div>
                        </div>

                        <div class="col-lg-6 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Edit Akun</h5>
                                </div>

                                <div class="ibox-content">

                                    <?php echo form_open('Apps/editakun')?>

                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>Nama</label><br>
                                                <input type="text" name="nama" class="form-control input-sm" required="" value="<?php echo $akun[0]->nama ?>">

                                            </div>
                                            <div class="col-lg-4">
                                                <label>Username</label><br>
                                                <input type="text" name="username" class="form-control input-sm" required="" value="<?php echo $akun[0]->username ?>">

                                            </div>
                                            <div class="col-lg-4">
                                                <label>Password</label><br>
                                                <input type="text" name="password" class="form-control input-sm" required="" value="<?php echo $akun[0]->password ?>">
                                                <br>
                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-success text-right ">Simpan</button>    
                                                </div>

                                            </div>
                                        </div>

                                        
                                        
                                    </div>

                                    <?php echo form_close(); ?>

                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>


</body>

</html>
