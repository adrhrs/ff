<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Supplier</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Kelola Supplier</h2>
                        <p class="font-bold">Halaman ini bertujuan untuk mengolah supplier dalam aplikasi ini, supplier adalah pihak yang membeli produk</p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo $this->session->flashdata('msg1'); ?>
                    
                    <div class="row">
                        <div class="col-lg-4 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Tambah Supplier</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="form-group">

                                        <?php echo form_open('Apps/tambahsupplier')?>
                                        
                                        <label>Nama Supplier </label>
                                        <input type="text" name="nama_supplier" class="form-control" required placeholder="ex : Pabrik Aneka Makanan" >
                                        <br>


                                        <label>Nomor HP</label>
                                        <input type="number" min="0" name="nomor" class="form-control" placeholder="ex : 082141295967" >
                                        <br>

                                        <label>Alamat</label>
                                        <textarea class="form-control" name="alamat"></textarea>

                                        <br>
                                        <label>NPWP</label>
                                        <input type="text" min="0" name="npwp" class="form-control" placeholder="Angkanya saja" >
                                        <br>
                                        
                                        <br>

                                        <div class="text-right">
                                            <button type="submit" class="btn btn-success text-right ">Tambah</button>    
                                        </div>
                                        

                                        <?php echo form_close(); ?>

                                    </div>

                                </div>
                            
                            </div>
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Pajak Supplier</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="form-group">

                                        <?php echo form_open('Apps/updatelistpajak')?>
                                        
                                        <label>List ID Supplier </label>
                                        <input type="text" name="value_str_sup" class="form-control" required value="<?php echo $setting[2]->value_str ?>" >
                                        
                                        <small>list ID dipisahkan dengan koma</small>
                                        <br><br>

                                        <label>Black List ID Produk </label>
                                        <input type="text" name="value_str_prod" class="form-control" required value="<?php echo $setting[3]->value_str ?>" >
                                        
                                        <small>list ID produk dipisahkan dengan koma</small>
                                        <br><br>

                                        <div class="text-right">
                                            <a href="<?php echo base_url('Apps/list_pajak') ?>" type="submit" class="btn text-right ">List Produk</a> 
                                            <button type="submit" class="btn btn-success text-right ">Update</button>    
                                        </div>
                                        

                                        <?php echo form_close(); ?>

                                    </div>

                                </div>
                            
                            </div>
                        </div>
                            

                        <div class="col-lg-8 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Supplier</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="20" data-filter=#filter>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Supplier</th>
                                                        <th class="text-center">Nomer Telp</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    <?php for ($i=0; $i < count($supplier); $i++) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center"><?php echo $supplier[$i]->id_supplier?></td>
                                                            <td><?php echo $supplier[$i]->nama_supplier?></td>
                                                            <td class="text-center"><?php echo $supplier[$i]->nomor?></td>
                                                            <td class="text-center">

                                                                <a data-toggle="modal" data-target="#lihat<?php echo $supplier[$i]->id_supplier ?>" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a>

                                                                <a data-toggle="modal" data-target="#edit<?php echo $supplier[$i]->id_supplier ?>" class="btn btn-warning btn-xs"><span class="fa fa-pencil"></span></a>

                                                                <a data-toggle="modal" data-target="#hapus<?php echo $supplier[$i]->id_supplier ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>

                                                                <div class="modal inmodal" id="lihat<?php echo $supplier[$i]->id_supplier ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <div class="modal-header">
                                                                                <h1><b>[<?php echo $supplier[$i]->id_supplier?>]</b> - <?php echo $supplier[$i]->nama_supplier?></h1>
                                                                            </div>

                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-lg-6">
                                                                                        <label>Nomor HP</label><br>
                                                                                        <span><?php echo $supplier[$i]->nomor?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-6">
                                                                                        <label>NPWP</label><br>
                                                                                        <span><?php echo $supplier[$i]->npwp?></span>
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <label>Alamat</label><br>
                                                                                        <span><?php echo $supplier[$i]->alamat?></span>
                                                                                    </div>
                                                                                </div>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        
                                                                                        <table class="table table-stripped">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th>ID Produk</th>
                                                                                                    <th>Nama Produk</th>
                                                                                                    <th>Jumlah PO</th>
                                                                                                     <th>PO ID list</th> 
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tbody>
                                                                                                <?php for ($j=0; $j < count($produk); $j++) { ?>
                                                                                                    <?php if($produk[$j]->id_supplier == $supplier[$i]->id_supplier) { ?>

                                                                                                        <tr>
                                                                                                            <td><?php echo $produk[$j]->id_produk ?></td>
                                                                                                            <td><?php echo $produk[$j]->nama ?></td>
                                                                                                            <td><?php echo $produk[$j]->po_count ?> PO</td>
                                                                                                             <td><?php 

                                                                                                             $li = (explode(",",$produk[$j]->po_list));
                                                                                                             $t = array_slice($li,0,10);
                                                                                                             if(count($li) > 10) {
                                                                                                                echo implode(", ",$t)." ...";
                                                                                                             } else {
                                                                                                                echo implode(", ",$t);
                                                                                                             }
                                                                                                             

                                                                                                             ?></td> 
                                                                                                        </tr>


                                                                                                <?php }} ?>
                                                                                            </tbody>
                                                                                        </table>
                                                                                    </div>
                                                                                </div>
                                                                            </div>


                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal inmodal" id="edit<?php echo $supplier[$i]->id_supplier ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/editsupplier')?>

                                                                            <div class="modal-header">

                                                                                <h1><input type="form-control" name="nama_supplier" value="<?php echo $supplier[$i]->nama_supplier?>"></h1>

                                                                            </div>

                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                
                                                                                    <div class="col-lg-6">
                                                                                        <label>Nomor HP</label><br>
                                                                                        <input type="number" min="0" name="nomor" class="form-control" value="<?php echo $supplier[$i]->nomor?>">
                                                                                        
                                                                                    </div>

                                                                                    <div class="col-lg-6">
                                                                                        <label>NPWP</label><br>
                                                                                        <input type="number" min="0" required pattern="[0-9]{15}" name="npwp" class="form-control" value="<?php echo $supplier[$i]->npwp?>">
                                                                                        
                                                                                    </div>
                                                                                    
                                                                                </div>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <label>Alamat</label><br>
                                                                                        <input type="text" name="alamat" class="form-control" value="<?php echo $supplier[$i]->alamat?>">
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-success">Simpan</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                                <input type="hidden" name="id_supplier" value="<?php echo $supplier[$i]->id_supplier?>">
                                                                            </div>

                                                                            <?php echo form_close() ?>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal inmodal" id="hapus<?php echo $supplier[$i]->id_supplier ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content animated bounceInUp">


                                                                            <?php echo form_open('Apps/hapussupplier')?>

                                                                            <div class="modal-body text-center">
                                                                                <h1>Apakah anda yakin untuk menghapus <b><?php echo $supplier[$i]->nama_supplier ?> ?</b> </h1>
                                                                                <br>
                                                                                <small>Anda tidak dapat mengembalikan produk yang telah terhapus</small>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id_supplier" value="<?php echo $supplier[$i]->id_supplier?>">
                                                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                            <?php echo form_close(); ?>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>


</body>

</html>
