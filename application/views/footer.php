<!-- Mainly scripts -->
    <script src="<?php echo base_url('assets/js/jquery-2.1.1.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>

    <script src="<?php echo base_url('assets/js/plugins/metisMenu/jquery.metisMenu.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/slimscroll/jquery.slimscroll.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datapicker/bootstrap-datepicker.js')?>"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?php echo base_url('assets/js/inspinia.js')?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/pace/pace.min.js')?>"></script>

    <script src="<?php echo base_url('assets/js/plugins/footable/footable.all.min.js')?>"></script>


    <script type="text/javascript">
    $(document).ready(function() {
        // console.log("Time until DOMready: ", Date.now() - timerStart);

        $.ajaxSetup({
            data: {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            }
        });

    });
    $(window).load(function() {

        var x = document.getElementsByTagName("p");

        var err_str = "";
        
        for (var i = 0; i < x.length; i++) {
            var err = x[i].outerText

            if(err.includes("Message:") | err.includes("Filename:") | err.includes("Line Number:")  ) {
                var err_temp = "[ "+err+" (" + i + ") ]"
                err_str = err_str + err_temp
            }

            
        }

        // console.log(err_str)

        var loadtime = Date.now() - timerStart
        var url = window.location.pathname
        var backend_time = <?php echo $backend_time; ?>

        console.log("Time until everything loaded: ", loadtime, backend_time );
        document.getElementById("dur").innerHTML = (loadtime) / 1000 + " Detik";

        let token = '<?php echo $this->security->get_csrf_hash() ?>';

        $.ajax({
            method: "POST",
            url: "<?php echo site_url('apps/log'); ?>",
            data: {
                loadtime: loadtime,
                url: url,
                err_str: backend_time
            },
            dataType: "JSON",
            success: function(res) {
                console.log(res);
            }
        })


    });
    </script>