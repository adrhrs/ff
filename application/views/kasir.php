<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Kasir</title>

     <?php include('header.php')?>

     <script src="<?php echo base_url('assets/js/jquery-1.12.4.min.js')?>"></script>

     <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script> -->

    <?php if(count($sc) > 0) { ?>
    <?php $totx = ((100-(int)$sc[0]->discount)/100)*(int)$tot[0]->harga; ?>

    <script  type="text/javascript">
    $(document).ready(function() {
        $('#pages').keyup(function(ev){
            var bayar = <?php echo $totx?>;
            var total = $('#pages').val() - bayar;
            var bilangan = (total).toFixed(0);

            var number_string = bilangan.toString(),
                sisa    = number_string.length % 3,
                rupiah  = number_string.substr(0, sisa),
                ribuan  = number_string.substr(sisa).match(/\d{3}/g);
                    
            if (ribuan) {
                separator = sisa ? '.' : '.';
                rupiah += separator + ribuan.join('.');
            }

            $('#total').html('Rp '+rupiah);
        });
    });
    </script>
    <?php } ?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-10">
                        <h2>Kasir</h2>
                        <p class="font-bold">Halaman khusus untuk mengelola transaksi kasir</p>
                    </div>
                    <div class="col-sm-2 text-right">
                        <h1>ID <b><?php echo $id?></b></h1>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>

                    

                    <div class="row">
                        <div class="col-lg-12 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Cari Produk</h5>
                                </div>

                                <div class="ibox-content">

                                    <?php echo form_open('Apps/cariProduk')?>
                                        
                                        <label>ID Produk / Nama Produk </label>
                                        <input type="text" name="param" class="form-control" required placeholder="ex : 22 atau Chicken Sausage" >
                                        <input type="hidden" name="tipe" value="kasir">
                                        <input type="hidden" name="id_sales" value="<?php echo $id?>">
                                        <br>

                                        <div class="text-right">
                                            <button type="submit" class="btn btn-success text-right ">Cari</button>    
                                        </div>
                                        

                                    <?php echo form_close(); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Produk</h5>
                                </div>

                                <div class="ibox-content">

                                    <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">

                                    <table class="footable table table-stripped" data-page-size="5" data-filter=#filter>
                                        <thead>
                                            <tr>
                                                <th class="text-center" data-type="numeric">ID</th>
                                                <th>Nama </th>
                                                <th class="text-center" >Harga </th>
                                                <th class="text-center">Ukuran</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            <?php $prod = $this->session->flashdata('res');?>

                                            <?php for ($i=0; $i < count($prod); $i++) { ?>
                                                
                                                <tr>
                                                    <td class="text-center"><?php echo $prod[$i]->id_produk?></td>
                                                    <td><a data-toggle="modal" data-target="#lihat<?php echo $prod[$i]->id_produk ?>" href=""><?php echo $prod[$i]->nama?></a></td>
                                                    <td class="text-center">  <span><?php echo "Rp " . number_format($prod[$i]->harga_normal,0,',','.'); ?></span></td>
                                                    <td class="text-center"><?php echo $prod[$i]->berat?> gr</td>
                                                    <td class="text-center">

                                                        <a href="<?php echo base_url('Apps/operasiKasir/tambah/'.$id.'/'.$prod[$i]->id_produk) ?>" class="btn btn-danger btn-xs"><span class="fa fa-plus"></span> <b>TAMBAH</b></a>

                                                        <div class="modal inmodal" id="lihat<?php echo $prod[$i]->id_produk ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog modal-lg">
                                                                <div class="modal-content animated bounceInUp">

                                                                    <div class="modal-header">
                                                                        <h1><b>[<?php echo $prod[$i]->id_produk?>]</b> - <?php echo $prod[$i]->nama?></h1>
                                                                    </div>

                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-lg-3">
                                                                                <label>Berat Produk</label><br>
                                                                                <span><?php echo $prod[$i]->berat?> gram</span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Isi per Pack</label><br>
                                                                                <span><?php echo $prod[$i]->isi?> Item</span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Produsen</label><br>
                                                                                <span><?php echo $prod[$i]->nama_produsen?> Item</span>
                                                                            </div>
                                                                            
                                                                            <div class="col-lg-3">
                                                                                <label>Ditambahkan Pada</label><br>
                                                                                <span><?php echo $prod[$i]->tgl?> </span>
                                                                            </div>
                                                                        </div>
                                                                        <br><br>
                                                                        <div class="row">
                                                                            <div class="col-lg-3">
                                                                                <label>Harga Beli</label><br>
                                                                                <span><?php echo "Rp " . number_format($prod[$i]->harga_beli,0,',','.'); ?></span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Harga Normal</label><br>
                                                                                <span><?php echo "Rp " . number_format($prod[$i]->harga_normal,0,',','.'); ?></span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Harga Agen</label><br>
                                                                                <span><?php echo "Rp " . number_format($prod[$i]->harga_agen,0,',','.'); ?></span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Harga Khusus</label><br>
                                                                                <span><?php echo "Rp " . number_format($prod[$i]->harga_khusus,0,',','.'); ?></span>
                                                                            </div>

                                                                            
                                                                        </div>
                                                                        <br><br>
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <label>Keterangan</label><br>
                                                                                <span><?php echo $prod[$i]->keterangan?> </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    
                                                </tr>

                                            <?php } ?>
                                            
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="51">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            
                            </div>
                        </div>

                        <div id="sc" class="col-lg-12 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h5>Daftar Belanja</h5>
                                        </div>
                                        <div class="col-lg-6 text-right">
                                            <a data-toggle="modal" data-target="#hapus" class="btn btn-white"><span class="fa fa-trash"></span> </a>

                                            <div class="modal inmodal" id="hapus" tabindex="-1" role="dialog" aria-hidden="true">

                                                <div class="modal-dialog">
                                                    <div class="modal-content animated bounceInUp">

                                                        <?php echo form_open('Apps/hapusNota')?>

                                                        <div class="modal-body text-center">
                                                            <h1>Apakah anda yakin untuk menghapus transaksi ini </h1>
                                                            
                                                        </div>

                                                        <div class="modal-footer">
                                                            <input type="hidden" name="id_sales" value="<?php echo $id ?>">
                                                            <button type="submit" class="btn btn-danger">Hapus</button>
                                                            <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                        </div>

                                                        <?php echo form_close() ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="ibox-content">

                                    <table class="table table-stripped">

                                        <thead>
                                            <tr>
                                                <th class="text-center">No.</th>
                                                <th>Nama Produk</th>
                                                <th class="text-center">Harga @</th>
                                                <th class="text-center">Jumlah</th>
                                                <th class="text-center">Sub-Total</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <?php for ($i=0; $i < count($sc) ; $i++) { ?>

                                                <tr>
                                                    <td class="text-center"><?php echo ($i+1)?></td>
                                                    <td><?php echo $sc[$i]->nama?></td>
                                                    <td class="text-center"><?php echo "Rp " . number_format($sc[$i]->harga,0,',','.'); ?></td>
                                                    <td class="text-center"><?php echo $sc[$i]->qty?></td>
                                                    <td class="text-center">
                                                        
                                                        <?php 

                                                            $harga = (int)$sc[$i]->harga;
                                                            $qty = (int)$sc[$i]->qty;

                                                            echo "Rp " . number_format($harga*$qty,0,',','.');

                                                            $cek = '';
                                                            if($qty == 1) {
                                                                $cek = 'disabled';
                                                            }

                                                        ?>
                                                            
                                                    </td>
                                                    <td class="text-center">
                                                        <a href="<?php echo base_url('Apps/operasiKasir/inc/'.$sc[$i]->id_sales.'/'.$sc[$i]->id_line) ?>" class="btn btn-xs btn-success" ><span class="fa fa-plus"></span></a>
                                                        <a <?php echo $cek ?> href="<?php echo base_url('Apps/operasiKasir/dec/'.$sc[$i]->id_sales.'/'.$sc[$i]->id_line) ?>" class="btn btn-xs btn-warning" href=""><span class="fa fa-minus"></span></a>
                                                        <a data-toggle="modal" data-target="#qty<?php echo $sc[$i]->id_line ?>" class="btn btn-xs btn-white" ><span class="fa fa-expand"></span></a>
                                                        <a href="<?php echo base_url('Apps/operasiKasir/delete/'.$sc[$i]->id_sales.'/'.$sc[$i]->id_line) ?>" class="btn btn-xs btn-white" href=""><span class="fa fa-trash"></span></a>

                                                        <div class="modal inmodal" id="qty<?php echo $sc[$i]->id_line ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content animated bounceInUp">

                                                                    <div class="modal-header">
                                                                        <h1> <span class="fa fa-expand text-info"></span> &nbsp&nbsp Set Jumlah Produk</h1>
                                                                    </div>

                                                                    <?php echo form_open('Apps/setQty')?>

                                                                    <div class="modal-body">

                                                                        <div class="row">
                                                                            <div class="col-lg-3 text-right">
                                                                                <h5><b>JUMLAH</b></h5><br>
                                                                                
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                <input min="0" value="<?php echo $sc[$i]->qty?>" type="number" class="form-control input-sm" name="qty" required="">
                                                                                <input type="hidden" name="id_line" value="<?php echo $sc[$i]->id_line?>">
                                                                                <input type="hidden" name="id_sales" value="<?php echo $sc[$i]->id_sales?>">
                                                                                <input type="hidden" name="tipe" value="kasir">
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <button type="submit" class="btn btn-success" >Simpan</button>
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close() ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                
                                            <?php }?>

                                        </tbody>
                                        
                                    </table>

                                    <?php if(count($sc) > 0){ ?>
                                    
                                    <div class="row">
                                        <?php echo form_open('Apps/simpanKasir')?>
                                        <div class="col-lg-1 text-right">
                                            
                                        </div>
                                        <div class="col-lg-2 text-left ">
                                            <h5><b>Total Harga Awal</b></h5>
                                            <h2><?php echo "Rp " . number_format((int)$tot[0]->harga,0,',','.'); ?></h2>
                                            <br>

                                            
                                            <h5><b>Total Harga Akhir</b></h5>
                                            <h2>
                                                
                                                <?php echo "Rp " . number_format($totx,0,',','.'); ?>
                                                    
                                            </h2>
                                        </div>
                                        <div class="col-lg-2 text-left ">
                                            <h5><b>Discount (<?php echo (int)$sc[0]->discount?> %)</b></h5>
                                            <h2><?php echo "Rp " . number_format(((int)$sc[0]->discount/100)*(int)$tot[0]->harga,0,',','.');  ?> </h2>
                                        </div>
                                        <div class="col-lg-2 text-left ">
                                            <h5><b>Bayar</b></h5>
                                            <input value="<?php echo $sc[0]->bayar ?>" type="number" name="pages" class="form-control" required="" id="pages" min="<?php echo $totx ?>" />
                                        </div>
                                        <div class="col-lg-2 text-left ">
                                            <h5><b>Kembalian</b></h5>
                                            <h2><span id="total"><?php echo "Rp " . number_format((int)$sc[0]->bayar-($totx),0,',','.'); ?></span></h2>
                                        </div>
                                        <div class="col-lg-2 text-left ">
                                            <br>
                                            <input type="hidden" name="id_sales" value="<?php echo $sc[0]->id_sales?>">
                                            <input type="hidden" name="id_finance" value="<?php echo $sc[0]->id_finance?>">
                                            <input type="hidden" name="total" value="<?php echo (int)$tot[0]->harga ?>">
                                            <a data-toggle="modal" data-target="#discount" class="btn btn-sm btn-block btn-info"><b>DISCOUNT</b></a>
                                            <button type="submit" class="btn btn-sm btn-block btn-primary"><b>SELESAI</b></button>
                                        </div>
                                        <?php echo form_close()?>
  
                                    </div>

                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

    <?php if(count($sc) > 0) { ?>
    <div class="modal inmodal" id="discount" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content animated bounceInDown">

                <div class="modal-header">
                    <h1> Tambahkan Discount</h1>
                </div>

                <?php echo form_open('Apps/setDiscount'); ?>

                <div class="modal-body text-center">

                    <div class="row">
                        <div class="col-lg-3 text-right">
                            <h5><b>DISCOUNT</b></h5>
                            <small class="text-muted">% harga produk</small><br><br>
                            <h5><b>KETERANGAN</b></h5>
                        </div>
                        <div class="col-lg-9">
                            <input max="100" min="0" value="<?php echo $sc[0]->discount?>" type="number" class="form-control input-sm" name="discount"  required="">
                            <br><br>
                            <textarea class="form-control" name="keterangan"><?php echo $sc[0]->ket_kasir?></textarea>
                            <input type="hidden" name="id_sales" value="<?php echo $sc[0]->id_sales?>">
                            <input type="hidden" name="id_finance" value="<?php echo $sc[0]->id_finance?>">
                            <input type="hidden" name="tipe" value="kasir">
                        </div>
                    </div>
                    
                </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info" >Simpan</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                </div>

                <?php echo form_close() ?>

            </div>
        </div>
    </div>

    <div class="modal inmodal" id="new" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content animated bounceInDown">

                <div class="modal-header">
                    <h1> <span class="fa fa-check text-success"></span> Transaksi Baru</h1>
                </div>

                <div class="modal-body text-center">

                    <p>Silahkan membuat transaksi baru, semua transaksi sebelumnya telah selesai</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                </div>

            </div>
        </div>
    </div>

    <?php } ?>

    <div class="modal inmodal" id="old" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content animated bounceInDown">

                <div class="modal-header">
                    <h1> <span class="fa fa-warning text-warning"></span> Transaksi Belum Selesai</h1>
                </div>

                <div class="modal-body text-center">

                    <p>Transaksi sebelumnya belum selesai, silahkan selesaikan terlebih dahulu</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                </div>

            </div>
        </div>
    </div>

   

    <?php include('footer.php')?>

    <?php if($tipe == 'old') {?>

         <script type="text/javascript">
        $(window).on('load',function(){
            $('#old').modal('show');
        });
       </script>

    <?php } else if($tipe == 'new') {?>
         <script type="text/javascript">
            $(window).on('load',function(){
                $('#new').modal('show');
            });
        </script>

    <?php } else {} ?>

   

    <script>

        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>


</body>

</html>
