<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Stock</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Stock Produk</h2>
                        <p class="font-bold">Halaman ini bertujuan untuk memantau jumlah stock masing masing produk</p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo $this->session->flashdata('msg1'); ?>
                    
                    <div class="row">

                       

                        <div class="col-lg-6 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Stock Produk ( 30 Produk Stock Terendah )</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="10" data-filter=#filter>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Produk</th>
                                                        <th class="text-center" data-type="numeric">Stock (pcs)</th>
                                                        <th class="text-center">Status</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php for ($i=0; $i < count($st) ; $i++) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center"><?php echo $st[$i]->id_produk ?></td>
                                                            <td><?php echo $st[$i]->nama ?></td>
                                                            <td class="text-center"><?php echo $st[$i]->stockNow ?></td>
                                                            <td class="text-center">
                                                                
                                                                <?php 

                                                                $stock = (int)$st[$i]->stockNow;
                                                                $med = (int)$set[1]->value;
                                                                $low = (int)$set[0]->value;

                                                                if($stock < 0) {
                                                                    echo '<span class="fa fa-warning text-danger"></span>';
                                                                    echo " ";
                                                                    echo '<span class="fa fa-warning text-danger"></span>';
                                                                } else if($stock <= $low) {
                                                                    echo '<span class="fa fa-warning text-danger"></span>';
                                                                    
                                                                } else if($stock <= $med) {
                                                                    echo '<span class="fa fa-warning text-warning"></span>';
                                                                } else {
                                                                    echo '<span class="fa fa-check-square text-success"></span>';

                                                                }


                                                                ?>

                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Cari Produk</h5>
                                </div>

                                <div class="ibox-content">

                                    <?php echo form_open('Apps/cariProduk')?>
                                        
                                        <label>ID Produk </label>
                                        <input type="number" name="param" class="form-control" required placeholder="ex : 22 " >
                                        <input type="hidden" name="tipe" value="stock">
                                        
                                        <br>

                                        <div class="text-right">
                                            <button type="submit" class="btn btn-success text-right ">Cari</button>    
                                        </div>
                                        

                                    <?php echo form_close(); ?>

                                </div>
                            </div>

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h5>Hasil Pencarian Produk </h5>        
                                        </div>
                                       
                                    </div>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="10" data-filter=#filter>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Produk</th>
                                                        <th class="text-center">Stock</th>
                                                        <th class="text-center">Status</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    <?php $st = $this->session->flashdata('res2');?>

                                                    <?php for ($i=0; $i < count($st) ; $i++) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center"><?php echo $st[$i]->id_produk ?></td>
                                                            <td><?php echo $st[$i]->nama ?></td>
                                                            <td class="text-center"><?php echo $st[$i]->stockNow ?></td>
                                                            <td class="text-center">
                                                                
                                                                <?php 

                                                                $stock = (int)$st[$i]->stockNow;
                                                                $med = (int)$set[1]->value;
                                                                $low = (int)$set[0]->value;

                                                                if($stock < 0) {
                                                                    echo '<span class="fa fa-warning text-danger"></span>';
                                                                    echo " ";
                                                                    echo '<span class="fa fa-warning text-danger"></span>';
                                                                } else if($stock <= $low) {
                                                                    echo '<span class="fa fa-warning text-danger"></span>';
                                                                    
                                                                } else if($stock <= $med) {
                                                                    echo '<span class="fa fa-warning text-warning"></span>';
                                                                } else {
                                                                    echo '<span class="fa fa-check-square text-success"></span>';

                                                                }


                                                                ?>

                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-lg-12 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Track Produk</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter2s" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="10" data-filter=#filter2s>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID Produk</th>
                                                        <th>Nama Produk</th>
                                                        <th class="text-center" data-type="numeric">Jumlah</th>
                                                        <th>Nama Agen</th>
                                                        <th class="text-center" >Tanggal</th>
                                                        <th class="text-center">Refrensi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    <?php for ($i=0; $i < count($tr) ; $i++) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center"><?php echo $tr[$i]->id_produk ?></td>
                                                            <td><?php echo $tr[$i]->nama ?></td>
                                                            <td class="text-center"><?php echo $tr[$i]->qty ?></td>
                                                            <td><?php echo $tr[$i]->nama_agen ?></td>
                                                            <td class="text-center"><?php echo $tr[$i]->tgl ?></td>
                                                            <td class="text-center">
                                                                <a href="<?php echo base_url('Apps/nota/so/'.$tr[$i]->id_sales) ?>" class="btn btn-xs btn-info">Sales Order</a>
                                                                <a href="<?php echo base_url('Apps/harga/'.$tr[$i]->id_agen) ?>" class="btn btn-xs btn-default">Detail Agen</a>
                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="25">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div> -->
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>


</body>

</html>
