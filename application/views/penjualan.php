<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Penjualan</title>

     <?php include('header.php')?>

     

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading  fadeIn">
                    <div class="col-sm-12">
                        <h2>Kelola Penjualan</h2>
                        <p class="font-bold">Halaman ini bertujuan untuk mengolah seluruh transaksi penjualan </p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    
                    <div class="row">

                        <div class="col-lg-4">
                            <div class="ibox ">
                                <div class="ibox-title">

                                    <h5>Pendapatan per Bulan &mdash; <i class="text-muted"><?php echo date('M Y'); ?></i>  </h5>
                                </div>
                                <div class="ibox-content">

                                     <?php $tot_kasir = 0; for($i=0; $i < count($kasir); ++$i) {

                                            $totx = ((100-(int)$kasir[$i]->discount)/100)*(int)$kasir[$i]->harga; 

                                            $tot_kasir += $totx;

                                        } ?>

                                        <?php $tot_agen = 0; for($i=0; $i < count($so); ++$i) { 

                                                    
                                            $totawal = (int)$so[$i]->harga;
                                            $dscx = (int)$so[$i]->discount;

                                            $dscval = ($dscx/100)*$totawal;
                                            $totx = $totawal - $dscval;

                                            $pjk = (int)$so[$i]->pajak;
                                            $pjkval = ($pjk/100)*$totx;
                                            $grandtot = $totx + $pjkval;

                                            $tot_agen = $tot_agen + $grandtot;

                                        }

                                        ?>

                                        <?php $tot_agen = 0; for($i=0; $i < count($so); ++$i) { 

                                                    
                                            $totawal = (int)$so[$i]->harga;
                                            $dscx = (int)$so[$i]->discount;

                                            $dscval = ($dscx/100)*$totawal;
                                            $totx = $totawal - $dscval;

                                            $pjk = (int)$so[$i]->pajak;
                                            $pjkval = ($pjk/100)*$totx;
                                            $grandtot = $totx + $pjkval;

                                            $tot_agen = $tot_agen + $grandtot;

                                        }

                                        ?>

                                        <?php $tot_cil = 0; for ($i=0; $i < count($ci); $i++) { 

                                            $tot_cil += (int)$ci[$i]->bayar;

                                        } ?>

                                    <h1 align="justify" ><?php echo "Rp " . number_format($tot_agen+$tot_kasir,0,',','.'); ?></h1>
                                    <hr>
                                    <div class="stat-percent font-bold text-warning"><?php echo count($so)+count($kasir); ?> <i class="fa fa-shopping-cart"></i></div>

                                    <small>Total Transaksi</small>
                                </div>
                            </div>
                        </div>
                        

                        <div class="col-lg-4">
                            <div class="ibox ">
                                <div class="ibox-title">

                                    <h5>Pendapatan Agen &mdash; <i class="text-muted"><?php echo date('M Y'); ?></i></h5>
                                </div>
                                <div class="ibox-content">

                                    

                                    <h1 align="justify" ><?php echo "Rp " . number_format($tot_agen,0,',','.'); ?> </h1>
                                    <hr>
                                    <div class="stat-percent font-bold text-info"><?php echo count($so); ?> <i class="fa fa-shopping-cart"></i></div>

                                    <small>Total Transaksi</small>
                                </div>
                            </div>
                        </div>

                        

                        <div class="col-lg-4">
                            <div class="ibox ">
                                <div class="ibox-title">

                                    <h5>Pendapatan Kasir &mdash; <i class="text-muted"><?php echo date('M Y'); ?></i></h5>
                                </div>
                                <div class="ibox-content">

                                   

                                    <h1 align="justify" ><?php echo "Rp " . number_format($tot_kasir,0,',','.'); ?> </h1>
                                    <hr>
                                    <div class="stat-percent font-bold text-success"><?php echo count($kasir); ?> <i class="fa fa-shopping-cart"></i></div>

                                    <small>Total Transaksi</small>
                                </div>
                            </div>
                        </div>




                        <div class="col-lg-12  fadeInDown">

                            <div id="kasirx" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Transaksi Kasir</h5>
                                </div>

                                <div class="ibox-content">
                                    <input type="text" class="form-control input-sm m-b-xs" id="filter2" placeholder="Search in table">

                                    <table class="footable table table-stripped" data-page-size="5" data-filter=#filter2>
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Tanggal</th>
                                                <th class="text-center">Total Produk</th>
                                                <th class="text-center">Discount</th>
                                                <th class="text-center">Total Harga</th>
                                                <th class="text-center">Pembayaran</th>
                                                <th class="text-center">Kembalian</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            
                                            <?php for($i=0; $i < count($kasir); ++$i) {?>

                                                <tr>
                                                    <?php $totx = ((100-(int)$kasir[$i]->discount)/100)*(int)$kasir[$i]->harga; ?>

                                                    <td class="text-center"><?php echo $kasir[$i]->id_sales ?></td>
                                                    <td class="text-center"><?php echo $kasir[$i]->tgl?></td>
                                                    <td class="text-center"><?php echo $kasir[$i]->qty?> Unit</td>
                                                    <td class="text-center"><?php echo $kasir[$i]->discount?> %</td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$totx,0,',','.'); ?></td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$kasir[$i]->bayar,0,',','.'); ?></td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$kasir[$i]->bayar-($totx),0,',','.'); ?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-xs btn-primary" href="<?php echo base_url('Apps/nota/kasir/'.$kasir[$i]->id_sales) ?>"><span class="fa fa-eye"></span></a>
                                                        <a class="btn btn-xs btn-warning" href="<?php echo base_url('Apps/kasir/ops/'.$kasir[$i]->id_sales) ?>"><span class="fa fa-pencil"></span></a>
                                                        <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#hapus<?php echo $kasir[$i]->id_sales ?>" ><span class="fa fa-trash"></span></a>

                                                        <div class="modal inmodal" id="hapus<?php echo $kasir[$i]->id_sales ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content  bounceInUp">

                                                                    <?php echo form_open('Apps/hapusNota')?>

                                                                    <div class="modal-body text-center">
                                                                        <h1>Apakah anda yakin untuk menghapus transaksi <b><?php echo $kasir[$i]->id_sales ?> ?</b> </h1>
                                                                        <br>
                                                                        <small>Anda tidak dapat mengembalikan transaksi yang telah terhapus</small>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="id_sales" value="<?php echo $kasir[$i]->id_sales ?>">
                                                                        <input type="hidden" name="id_finance" value="<?php echo $kasir[$i]->id_finance ?>">
                                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close() ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>

                                        <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>

                            <div id="agenx" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <h5>Transaksi Penjualan Agen ( <?php echo $dur; ?> ) </h5>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <button data-toggle="modal" data-target="#agen" class="btn btn-xs btn-info" ><b>+ TAMBAH TRANSAKSI</b></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <input type="text" class="form-control input-sm m-b-xs" id="filter2" placeholder="Search in table">

                                    <table class="footable table table-stripped" data-page-size="5" data-filter=#filter2>
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th>Agent</th>
                                                <th class="text-center">Tanggal</th>
                                                <th class="text-center">Total Harga</th>
                                                <th class="text-center">Pembayaran</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                            
                                            <?php for($i=0; $i < count($so2); ++$i) { ?>

                                                <?php 

                                                $totawal = (int)$so2[$i]->harga;
                                                $dscx = (int)$so2[$i]->discount;

                                                $dscval = ($dscx/100)*$totawal;
                                                $totx = $totawal - $dscval;

                                                $pjk = (int)$so2[$i]->pajak;
                                                $pjkval = ($pjk/100)*$totx;
                                                $grandtot = $totx + $pjkval;

                                                ?>

                                                <tr>
                                                    <td class="text-center"><?php echo $so2[$i]->id_sales ?></td>
                                                    <td ><a href="<?php echo base_url('Apps/harga/'.$so2[$i]->id_agen)?>"><?php echo $so2[$i]->nama_agen?></a></td>
                                                    <td class="text-center"><?php echo $so2[$i]->tgl ?></td>
                                                    <td class="text-center">
                                                    
                                                        <?php 

                                                            echo "Rp " . number_format($grandtot,0,',','.'); 

                                                        ?>
                                                            
                                                    </td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$so2[$i]->bayar,0,',','.'); ?></td>

                                            
                                                    <td class="text-center">
                                                        <a class="btn btn-xs btn-primary" href="<?php echo base_url('Apps/nota/so/'.$so2[$i]->id_sales) ?>"><span class="fa fa-eye"></span></a>
                                                        <a class="btn btn-xs btn-warning" href="<?php echo base_url('Apps/tambahSO/edit/'.$so2[$i]->id_sales) ?>"><span class="fa fa-pencil"></span></a>
                                                        <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#hapus<?php echo $so2[$i]->id_sales ?>" ><span class="fa fa-trash"></span></a>

                                                        <div class="modal inmodal" id="hapus<?php echo $so2[$i]->id_sales ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content  bounceInUp">

                                                                    <?php echo form_open('Apps/hapusNota')?>

                                                                    <div class="modal-body text-center">
                                                                        <h1>Apakah anda yakin untuk menghapus transaksi <b><?php echo $so2[$i]->id_sales ?> ?</b> </h1>
                                                                        <br>
                                                                        <small>Anda tidak dapat mengembalikan transaksi yang telah terhapus</small>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="id_sales" value="<?php echo $so2[$i]->id_sales ?>">
                                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close() ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>



                                        <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>

                            <div id="cicilx" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <h5>Transaksi Cicilan / Utang Agen</h5>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <button data-toggle="modal" data-target="#cicilan" class="btn btn-xs btn-info" ><b>+ TAMBAH CICILAN / UTANG</b></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <input type="text" class="form-control input-sm m-b-xs" id="filter3" placeholder="Search in table">

                                    <table class="footable table table-stripped" data-page-size="5" data-filter=#filter3>
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th>Agent</th>
                                                <th class="text-center">Tanggal</th>
                                                <th class="text-center">Total Cicilan</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>


                                        <tbody>

                                            <?php for ($i=0; $i < count($ci); $i++) { ?>
                                                
                                                <tr>
                                                    <td class="text-center"><?php echo $ci[$i]->id_finance?></td>
                                                    <td ><a href="<?php echo base_url('Apps/harga/'.$ci[$i]->id_agen)?>"><?php echo $ci[$i]->nama_agen?></a></td>
                                                    <td class="text-center"><?php echo $ci[$i]->tgl?></td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$ci[$i]->bayar,0,',','.'); ?></td>
                                                    <td class="text-center">

                                                        <a class="btn btn-xs btn-warning" data-toggle="modal" data-target="#editFinance<?php echo $ci[$i]->id_finance ?>" ><span class="fa fa-pencil"></span></a>
                                                        <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#hapusFinance<?php echo $ci[$i]->id_finance ?>" ><span class="fa fa-trash"></span></a>

                                                        <div class="modal inmodal" id="editFinance<?php echo $ci[$i]->id_finance ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content  bounceInUp">

                                                                    <?php echo form_open('Apps/editFinance');?>

                                                                    <div class="modal-body">

                                                                        <p class="text-center">Bagian ini memiliki tujuan untuk mengedit saldo agen</p>
                                                                        <br>
                                                                        <div class="row">
                                                                            <div class="col-lg-3 text-right">
                                                                                <h5><b>NAMA AGEN</b></h5><br>
                                                                                <h5><b>BAYAR</b></h5><br>
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                <input  type="text" readonly="" class="form-control input-sm" value="<?php echo $ci[$i]->nama_agen?>"><br>
                                                                                <input min="0" type="number" class="form-control input-sm" value="<?php echo $ci[$i]->bayar?>" name="bayar" required="">
                                                                                <input type="hidden" name="id_finance" value="<?php echo $ci[$i]->id_finance?>">
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    
                                                                    <div class="modal-footer">
                                                                        <button type="submit" class="btn btn-info">Simpan</button>
                                                                        <button type="button" data-toggle="modal" data-target="#cicilan" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close()?>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal inmodal" id="hapusFinance<?php echo $ci[$i]->id_finance ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content  bounceInUp">

                                                                    <?php echo form_open('Apps/hapusFinance')?>

                                                                    <div class="modal-body text-center">
                                                                        <h1>Apakah anda yakin untuk menghapus cicilan ini ? </h1>
                                                                        <br>
                                                                        <small>Anda tidak dapat mengembalikan transaksi yang telah terhapus</small>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="id_finance" value="<?php echo $ci[$i]->id_finance ?>">
                                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close() ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </td>

                                                </tr>

                                            <?php }?>

                                        </tbody>

                                        <tfoot>
                                            <tr>
                                                <td colspan="10">
                                                    <ul class="pagination pull-right"></ul>
                                                </td>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>

                            

                        </div>
                        
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

    <div class="modal inmodal" id="agen" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content  bounceInDown">

                <div class="modal-header">
                    <h1> <span class="fa fa-person"></span> Daftar Agen</h1>
                </div>

                <div class="modal-body">

                    <input type="text" class="form-control input-sm m-b-xs" id="filter3x" placeholder="Cari Agen">

                    <table class="footable table table-stripped" data-page-size="10" data-filter=#filter3x>
                        <thead>
                            <tr>
                                <th data-type="number" class="text-center">ID</th>
                                <th>Nama Agen</th>
                                <th class="text-center">Kategori</th>
                                <th class="text-center">Transaksi</th>

                            </tr>
                        </thead>

                        <tbody>

                            <?php for($i=0; $i < count($agen); $i++) { ?>

                                <tr>
                                    <td class="text-center"><?php echo $agen[$i]->id_agen?></td>
                                    <td><?php echo $agen[$i]->nama_agen?></td>
                                    <td class="text-center"><?php echo $agen[$i]->kategori?></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url('Apps/redirectso/'.$agen[$i]->id_agen) ?>" class="btn btn-xs btn-success" >Sales Order</a>

                                    </td>
                                </tr>

                            <?php } ?>

                        </tbody>

                        <tfoot>
                            <tr>
                                <td colspan="10">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal inmodal" id="cicilan" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content  bounceInDown">

                <div class="modal-header">
                    <h1> <span class="fa fa-person"></span> Daftar Agen</h1>
                </div>

                <div class="modal-body">

                    <input type="text" class="form-control input-sm m-b-xs" id="filter45" placeholder="Cari Agen">

                    <table class="footable table table-stripped" data-page-size="10" data-filter=#filter45>
                        <thead>
                            <tr>
                                <th data-type="number" class="text-center">ID</th>
                                <th>Nama Agen</th>
                                <th class="text-center">Kategori</th>
                                <th class="text-center">Transaksi</th>

                            </tr>
                        </thead>

                        <tbody>

                            <?php for($i=0; $i < count($agen); $i++) { ?>

                                <tr>
                                    <td class="text-center"><?php echo $agen[$i]->id_agen?></td>
                                    <td><?php echo $agen[$i]->nama_agen?></td>
                                    <td class="text-center"><?php echo $agen[$i]->kategori?></td>
                                    <td class="text-center">

                                        <a data-toggle="modal" data-target="#cicil<?php echo $agen[$i]->id_agen ?>" data-dismiss="modal" class="btn btn-xs btn-warning" >Cicil / Utang</a>

                                        <div class="modal inmodal" id="cicil<?php echo $agen[$i]->id_agen ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                            <div class="modal-dialog">
                                                <div class="modal-content  bounceInDown">

                                                    <div class="modal-header">
                                                        <h1> <span class="fa fa-dollar text-warning"></span> Cicil / Utang </h1>
                                                        <h4></h4>
                                                    </div>

                                                    <?php echo form_open('Apps/tambahCicilan');?>

                                                    <div class="modal-body">

                                                        <p class="text-center">Di kotak pembayaran apabila hutang maka diberi negatif (-) diawal pembayaran, sedangkan apabila cicilan tidak perlu</p>
                                                        <br>
                                                        <div class="row">
                                                            <div class="col-lg-3 text-right">
                                                                <h5><b>NAMA AGEN</b></h5><br>
                                                                <h5><b>BAYAR</b></h5><br>
                                                            </div>
                                                            <div class="col-lg-9">
                                                                <input  type="text" readonly="" class="form-control input-sm" value="<?php echo $agen[$i]->nama_agen?>"><br>
                                                                <input type="number" class="form-control input-sm" name="bayar" placeholder="ex : 1000000" required="">
                                                                <input type="hidden" name="id_agen" value="<?php echo $agen[$i]->id_agen?>">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-success">Simpan</button>
                                                        <button type="button" data-toggle="modal" data-target="#cicilan" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                    </div>

                                                    <?php echo form_close()?>

                                                </div>
                                            </div>
                                        </div>


                                    </td>
                                </tr>

                            <?php } ?>

                        </tbody>

                        <tfoot>
                            <tr>
                                <td colspan="10">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                </div>

            </div>
        </div>
    </div>

   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();
            // $('.footable2').footable();

        });

    </script>


</body>

</html>
