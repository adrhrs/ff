    <nav class="navbar-default navbar-static-side animated fadeIn" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <?php include('akunlogin.php'); ?>

                

                <li>
                    <a><i class="fa fa-shopping-cart"></i> <span class="nav-label">Transaksi</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo base_url('Apps/redirectkasir') ?>">Kasir <i class="fa fa-calculator pull-right"></i></a></li>
                        <li><a href="<?php echo base_url('Apps/penjualan') ?>">Penjualan <i class="fa fa-arrow-circle-right pull-right"></i></a></li>
                        <li><a href="<?php echo base_url('Apps/pembelian') ?>">Pembelian <i class="fa fa-arrow-circle-left pull-right"></i></a></li>
                    </ul>
                </li>

                <li>
                    <a><i class="fa fa-cubes"></i> <span class="nav-label">Inventori</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo base_url('Apps/Stock') ?>">Stock Produk <i class="fa fa-cube pull-right"></i></a></li>
                        <li><a href="<?php echo base_url('Apps/pergerakan') ?>">Pergerakan <i class="fa fa-retweet pull-right"></i></a></li>
                    </ul>
                </li>

                <li>
                    <a><i class="fa fa-money"></i> <span class="nav-label">Finansial</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo base_url('Apps/cashflow') ?>">Cash Flow<i class="fa fa-dollar pull-right"></i></a></li>
                    </ul>
                </li>

                <li>
                    <a><i class="fa fa-list"></i> <span class="nav-label">Report</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">

                        <li><a href="<?php echo base_url('Apps/histori/kasir/0/0/0/1') ?>">Kasir <i class="fa fa-calculator pull-right"></i></a></li>
                        <li><a href="<?php echo base_url('Apps/histori/penjualan/0/0/0/1') ?>">Penjualan <i class="fa fa-arrow-circle-right pull-right"></i></a></li>
                        <li><a href="<?php echo base_url('Apps/histori/pembelian/0/0/0/1') ?>">Pembelian <i class="fa fa-arrow-circle-left pull-right"></i></a></li>
                        
                    </ul>
                </li>

                <li>
                    <a><i class="fa fa-folder"></i> <span class="nav-label">Master Data</span><span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level collapse">
                        <li><a href="<?php echo base_url('Apps/produk') ?>">Produk <i class="fa fa-leaf pull-right"></i></a></li>
                        <li><a href="<?php echo base_url('Apps/produsen') ?>">Produsen <i class="fa fa-building pull-right"></i></a></li>
                        <li><a href="<?php echo base_url('Apps/agen') ?>">Agen <i class="fa fa-user pull-right"></a></i></li>
                        <li><a href="<?php echo base_url('Apps/supplier') ?>">Supplier <i class="fa fa-truck pull-right"></a></i></li>
                        <li><a href="<?php echo base_url('Apps/akun') ?>">Akun <i class="fa fa-cog pull-right"></a></i></li>
                    </ul>
                </li>

                

             

                <li>
                    <a href="<?php echo base_url('Apps/logout') ?>"><i class="fa fa-arrow-left"></i> <span class="nav-label">Logout</span> </a>
                </li>
                
                <li class="special_link">
                        <a><i class="fa fa-clock-o"></i> <span id="dur" class="nav-label"></span></a>
                    </li>
            </ul>

        </div>
    </nav>