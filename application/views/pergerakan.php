<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Pergerakan</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Pergerakan Produk</h2>
                        <p class="font-bold">Halaman ini bertujuan untuk mengolah pergerakan barang (inventory).</p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo $this->session->flashdata('msg1'); ?>
                    
                    <div class="row">

                        <div class="col-lg-12 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Tambah Stock Opname</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <?php echo form_open('Apps/cariProduk')?>
                                        
                                                <label>ID Produk / Nama Produk </label>
                                                <input type="text" name="param" class="form-control" required placeholder="ex : 22 atau Chicken Sausage" >
                                                <input type="hidden" name="tipe" value="opname">
                                                
                                                <br>

                                                <div class="text-right">
                                                    <button type="submit" class="btn btn-success text-right ">Cari</button>    
                                                </div>
                                                

                                            <?php echo form_close(); ?>
                                        </div>

                                        <div class="col-lg-6">

                                            <label>Hasil Pencarian</label>
                                            <input type="text" class="form-control input-sm m-b-xs" id="filterxx" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="10" data-filter=#filterxx>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Produk</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    <?php $prod = $this->session->flashdata('res');?>

                                                    <?php for ($i=0; $i < count($prod); $i++) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center"><?php echo $prod[$i]->id_produk?></td>
                                                            <td><?php echo $prod[$i]->nama?></td>
                                                            <td class="text-center"><a data-toggle="modal" data-dismiss="modal" data-target="#stc<?php echo $prod[$i]->id_produk?>" class="btn btn-xs btn-success" href="">Stock Opname</td>

                                                            <div class="modal inmodal" id="stc<?php echo $prod[$i]->id_produk ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                <div class="modal-dialog">
                                                                    <div class="modal-content animated bounceInUp">

                                                                        <?php echo form_open('Apps/stockOpname') ?>

                                                                        <div class="modal-header">
                                                                            <h1> <span class="fa fa-cubes text-info"></span> Stock Opname </h1>
                                                                            <h4><?php echo $prod[$i]->nama?></h4>
                                                                        </div>

                                                                        <div class="modal-body">

                                                                            <div class="row">
                                                                                <div class="col-lg-3 text-right">
                                                                                    <h5><b>TIPE</b></h5><br>
                                                                                    <h5><b>JUMLAH</b></h5>
                                                                                    
                                                                                </div>
                                                                                <div class="col-lg-9">
                                                                                    
                                                                                    <?php

                                                                                      $options = array( 

                                                                                      "in" => "Penambahan Stock", 
                                                                                      "out" => "Pengurangan Stock", 

                                                                                      );

                                                                                     

                                                                                      $js = array( 'class' => 'form-control input-sm' );
                                                                                      echo form_dropdown('direction', $options, 'in' ,$js);

                                                                                    ?>

                                                                                    <br>
                                                                                    <input min="0" placeholder="ex : 100" type="number" class="form-control input-sm" name="qty" required="">

                                                                                    <input type="hidden" name="id_produk" value="<?php echo $prod[$i]->id_produk?>">

                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                        <div class="modal-footer">
                                                                            <button type="submit" class="btn btn-success">Simpan</button>
                                                                            <button data-toggle="modal" data-target="#stock" type="button" class="btn btn-white" data-dismiss="modal">Kembali</button>
                                                                        </div>

                                                                        <?php echo form_close() ?>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                            
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Stock Opname</h5>
                                </div>

                                <div class="ibox-content">

                                    

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="5" data-filter=#filter>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Produk</th>
                                                        <th class="text-center">Qty (pcs)</th>
                                                        <th class="text-center">Tipe</th>
                                                        <th class="text-center">Arah</th>
                                                        <th class="text-center">Tanggal</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    

                                                    <?php for ($i=0; $i < count($op); ++$i) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center" data-type="numeric" ><?php echo $op[$i]->id_inventory ?></td>
                                                            <td ><?php echo $op[$i]->nama ?></td>
                                                            <td class="text-center" data-type="numeric" ><?php echo $op[$i]->qty ?></td>
                                                            <td class="text-center" ><?php echo $op[$i]->tipe_inventory ?></td>
                                                            <td class="text-center">
                                                                <?php

                                                                    if($op[$i]->direction == 'out') {
                                                                        echo '<span class= "fa fa-arrow-right text-warning" ></span>';
                                                                    } else {
                                                                        echo '<span class= "fa fa-arrow-left text-info" ></span>';
                                                                    }

                                                                ?>
                                                            </td>

                                                            <td class="text-center" ><?php echo $op[$i]->tgl ?></td>

                                                            <td class="text-center">

                                                                <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#hapusInventory<?php echo $op[$i]->id_inventory ?>" >Hapus</a>

                                                                <div class="modal inmodal" id="hapusInventory<?php echo $op[$i]->id_inventory ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/hapusInventory')?>

                                                                            <div class="modal-body text-center">
                                                                                <h2>Apakah anda yakin untuk menghapus stock opname <br><br><b><?php echo $op[$i]->nama ?> </b> sebanyak <b><?php echo $op[$i]->qty ?> </b> pcs ?</h2>
                                                                                <br>
                                                                                <small>Anda tidak dapat mengembalikan stock opname yang telah terhapus</small>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                            <input type="hidden" name="id_inventory" value="<?php echo $op[$i]->id_inventory ?>">
                                                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                            <?php echo form_close() ?>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="51">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                            

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Pergerakan Penjualan</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filtermv" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="20" data-filter=#filtermv>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Produk</th>
                                                        <th class="text-center">Qty (pcs)</th>
                                                        <th class="text-center">Tipe</th>
                                                        <th class="text-center">Arah</th>
                                                        <th class="text-center">Tanggal</th>
                                                        <th class="text-center">Refrensi</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    

                                                    <?php for ($i=0; $i < count($mv); ++$i) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center" data-type="numeric" ><?php echo $mv[$i]->id_inventory ?></td>
                                                            <td ><?php echo $mv[$i]->nama ?></td>
                                                            <td class="text-center" data-type="numeric" ><?php echo $mv[$i]->qty ?></td>
                                                            <td class="text-center" >
                                                                
                                                                <?php

                                                                    if($mv[$i]->tipe_inventory == 'so') {
                                                                        echo "Agen";
                                                                    } else {
                                                                        echo "Kasir";
                                                                    }

                                                                ?>
                                                                
                                                            </td>
                                                            <td class="text-center">
                                                                <?php

                                                                    if($mv[$i]->direction == 'out') {
                                                                        echo '<span class= "fa fa-arrow-right text-warning" ></span>';
                                                                    } else {
                                                                        echo '<span class= "fa fa-arrow-left text-info" ></span>';
                                                                    }

                                                                ?>
                                                            </td>

                                                            <td class="text-center" ><?php echo $mv[$i]->tgl ?></td>

                                                            <td class="text-center">
                                                                <?php

                                                                    if($mv[$i]->tipe_inventory == 'so') {
                                                                        echo '<a href="'.base_url('Apps/nota/so/'.$mv[$i]->id_sales).'" class="btn btn-xs btn-info">Detail</a>';
                                                                    } else {
                                                                        echo '<a href="'.base_url('Apps/nota/kasir/'.$mv[$i]->id_sales).'" class="btn btn-xs btn-info">Detail</a>';
                                                                    }

                                                                ?>
                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="51s">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Pergerakan Pembelian</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filterpr" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="20" data-filter=#filterpr>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Produk</th>
                                                        <th class="text-center">Qty (pcs)</th>
                                                        <th class="text-center">Tipe</th>
                                                        <th class="text-center">Arah</th>
                                                        <th class="text-center">Tanggal</th>
                                                        <th class="text-center">Refrensi</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    

                                                    <?php for ($i=0; $i < count($pr); ++$i) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center" data-type="numeric" ><?php echo $pr[$i]->id_inventory ?></td>
                                                            <td ><?php echo $pr[$i]->nama ?></td>
                                                            <td class="text-center" data-type="numeric" ><?php echo $pr[$i]->qty ?></td>
                                                            <td class="text-center" >
                                                                
                                                                <?php

                                                                    if($pr[$i]->tipe_inventory == 'po') {
                                                                        echo "PO";
                                                                    } else {
                                                                        echo "Kasir";
                                                                    }

                                                                ?>
                                                                
                                                            </td>
                                                            <td class="text-center">
                                                                <?php

                                                                    if($pr[$i]->direction == 'out') {
                                                                        echo '<span class= "fa fa-arrow-right text-warning" ></span>';
                                                                    } else {
                                                                        echo '<span class= "fa fa-arrow-left text-info" ></span>';
                                                                    }

                                                                ?>
                                                            </td>

                                                            <td class="text-center" ><?php echo $pr[$i]->tgl ?></td>

                                                            <td class="text-center">
                                                                <?php

                                                                        echo '<a href="'.base_url('Apps/nota/po/'.$pr[$i]->id_purchase).'" class="btn btn-xs btn-info">Detail</a>';

                                                                ?>
                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="51s">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

    
   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>


</body>

</html>
