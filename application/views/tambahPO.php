<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Tambah Pembelian</title>

     <?php include('header.php')?>

     <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script> -->
     <script src="<?php echo base_url('assets/js/jquery-1.12.4.min.js')?>"></script>

     <?php if(count($sc) > 0) { ?>

        <?php 

        $totawal = (int)$tot[0]->harga;
        $dscx = (int)$sc[0]->discount;

        $dscval = ($dscx/100)*$totawal;
        $totx = $totawal - $dscval;

        $pjk = (int)$sc[0]->pajak;
        $pjkval = ($pjk/100)*$totx;
        $grandtot = $totx + $pjkval;

        ?>

    <script  type="text/javascript">
    $(document).ready(function() {

        function convertToRupiah(angka)
        {
            var rupiah = '';        
            var angkarev = angka.toString().split('').reverse().join('');
            for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
            return 'Rp '+rupiah.split('',rupiah.length-1).reverse().join('');
        }
        $('#pages').keyup(function(ev){
            var bayar = <?php echo $grandtot?>;
            var total = $('#pages').val() - bayar;
            var bilangan = (total).toFixed(0);

            var rupiah = convertToRupiah(bilangan);


            $('#total').html(rupiah);
        });

        $('#kmb').keyup(function(ev){
            var bayar = $('#pages').val() - <?php echo $grandtot-(int)$sc[0]->discount?>;
            var total = bayar - $('#kmb').val();
            var bilangan = (total).toFixed(0);

            

            if(bilangan<0) {
                $('#stat').html('Saldo di Transaksi Ini');
            } else {
                $('#stat').html('Nitip di Transaksi Ini');
            }

            var rupiah = convertToRupiah(Math.abs(bilangan));

            $('#saltip').html(rupiah);
        });
    });
    </script>

    <?php } ?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-10">
                        <h2>Purchase Order</h2>
                        <p class="font-bold">Halaman khusus untuk mengelola sales order ( transaksi untuk supplier )</p>
                    </div>
                    <div class="col-sm-2 text-right">
                        <h1>PO <b><?php echo $id?></b></h1>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    
                    <div class="row">
                        <div class="col-lg-4 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Informasi supplier</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="form-group">
                                        
                                        <label>Nama supplier</label>
                                        <br>
                                        <?php echo $supplier[0]->nama_supplier ?>
                                        <br><br>

                                        <div class="row">

                                            <div class="col-lg-12">
                                                
                                                <label>Nomor HP</label>
                                                <br>
                                                <?php echo $supplier[0]->nomor ?>
                                                <br><br>
                                            
                                            </div>
                                        </div>

                                        <label>Alamat</label>
                                        <br>
                                        <?php echo $supplier[0]->alamat ?>
                                        <br><br>

                                  
                                    </div>

                                    <hr>

                                    <a data-toggle="modal" data-target="#hapus" class="btn btn-white"><span class="fa fa-trash"></span> </a>

                                    <div class="modal inmodal" id="hapus" tabindex="-1" role="dialog" aria-hidden="true">

                                        <div class="modal-dialog">
                                            <div class="modal-content animated bounceInUp">

                                                <?php echo form_open('Apps/hapusNotaPurchase')?>

                                                <div class="modal-body text-center">
                                                    <h1>Apakah anda yakin untuk menghapus transaksi ini </h1>
                                                    
                                                </div>

                                                <div class="modal-footer">
                                                    <input type="hidden" name="id_purchase" value="<?php echo $id ?>">
                                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                </div>

                                                <?php echo form_close() ?>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            
                            </div>
                        </div>
                        <div class="col-lg-8 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Cari Produk</h5>
                                </div>

                                <div class="ibox-content">

                                    <?php echo form_open('Apps/cariProduk')?>
                                        
                                        <label>ID Produk / Nama Produk </label>
                                        <input type="text" name="param" class="form-control" required placeholder="ex : 22 atau Chicken Sausage" >
                                        <input type="hidden" name="tipe" value="po">
                                        <input type="hidden" name="id_po" value="<?php echo $id?>">
                                        <br>

                                        <div class="text-right">
                                            <button type="submit" class="btn btn-success text-right ">Cari</button>    
                                        </div>
                                        

                                    <?php echo form_close(); ?>

                                </div>
                            </div>

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Produk</h5>
                                </div>

                                <div class="ibox-content">

                                    <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">

                                    <table class="footable table table-stripped" data-page-size="5" data-filter=#filter>
                                        <thead>
                                            <tr>
                                                <th class="text-center" data-type="numeric">ID</th>
                                                <th>Nama Produk</th>
                                                <th class="text-center" >Harga Agen</th>
                                                <th class="text-center">Ukuran</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>

                                            <?php $prod = $this->session->flashdata('res');?>

                                            <?php for ($i=0; $i < count($prod); $i++) { ?>
                                                
                                                <tr>
                                                    <td class="text-center"><?php echo $prod[$i]->id_produk?></td>
                                                    <td><a data-toggle="modal" data-target="#lihat<?php echo $prod[$i]->id_produk ?>" href=""><?php echo $prod[$i]->nama?></a></td>
                                                    <td class="text-center">  <span><?php echo "Rp " . number_format($prod[$i]->harga_agen,0,',','.'); ?></span></td>
                                                    <td class="text-center"><?php echo $prod[$i]->berat?> gr</td>
                                                    <td class="text-center">

                                                        <a href="<?php echo base_url('Apps/operasiPO/tambah/'.$id.'/'.$prod[$i]->id_produk) ?>" class="btn btn-danger btn-xs"><span class="fa fa-plus"></span> <b>TAMBAH</b></a>

                                                        <div class="modal inmodal" id="lihat<?php echo $prod[$i]->id_produk ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog modal-lg">
                                                                <div class="modal-content animated bounceInUp">

                                                                    <div class="modal-header">
                                                                        <h1><b>[<?php echo $prod[$i]->id_produk?>]</b> - <?php echo $prod[$i]->nama?></h1>
                                                                    </div>

                                                                    <div class="modal-body">
                                                                        <div class="row">
                                                                            <div class="col-lg-3">
                                                                                <label>Berat Produk</label><br>
                                                                                <span><?php echo $prod[$i]->berat?> gram</span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Isi per Pack</label><br>
                                                                                <span><?php echo $prod[$i]->isi?> Item</span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Produsen</label><br>
                                                                                <span><?php echo $prod[$i]->nama_produsen?></span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Ditambahkan Pada</label><br>
                                                                                <span><?php echo $prod[$i]->tgl?> </span>
                                                                            </div>
                                                                        </div>
                                                                        <br><br>
                                                                        <div class="row">
                                                                            <div class="col-lg-3">
                                                                                <label>Harga Beli</label><br>
                                                                                <span><?php echo "Rp " . number_format($prod[$i]->harga_beli,0,',','.'); ?></span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Harga Normal</label><br>
                                                                                <span><?php echo "Rp " . number_format($prod[$i]->harga_normal,0,',','.'); ?></span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Harga Agen</label><br>
                                                                                <span><?php echo "Rp " . number_format($prod[$i]->harga_agen,0,',','.'); ?></span>
                                                                            </div>
                                                                            <div class="col-lg-3">
                                                                                <label>Harga Khusus</label><br>
                                                                                <span><?php echo "Rp " . number_format($prod[$i]->harga_khusus,0,',','.'); ?></span>
                                                                            </div>

                                                                            
                                                                        </div>
                                                                        <br><br>
                                                                        <div class="row">
                                                                            <div class="col-lg-12">
                                                                                <label>Keterangan</label><br>
                                                                                <span><?php echo $prod[$i]->keterangan?> </span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>

                                            <?php } ?>
                                            
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td colspan="5">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            
                            </div>
                        </div>

                        <div id="sc" class="col-lg-12 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Belanja</h5>
                                </div>

                                <div class="ibox-content">

                                    <table class="table table-stripped">

                                        <thead>
                                            <tr>
                                                <th class="text-center">No.</th>
                                                <th>Nama Produk</th>
                                                <th class="text-center">Harga @</th>
                                                <th class="text-center">Diskon Produk</th>
                                                <th class="text-center">Jumlah</th>
                                                <th class="text-center">Sub-Total</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                            <?php for ($i=0; $i < count($sc) ; $i++) { ?>

                                                <?php 

                                                $harga = (int)$sc[$i]->harga;
                                                $qty = (int)$sc[$i]->qty;
                                                $dp = (int)$sc[$i]->diskon_produk;

                                                $subx = (((100-$dp)/100)*$harga)*$qty;


                                                ?>

                                                <tr>
                                                    <td class="text-center"><?php echo ($i+1)?></td>
                                                    <td><?php echo $sc[$i]->nama?></td>
                                                    <td class="text-center"><?php echo "Rp " . number_format($sc[$i]->harga,0,',','.'); ?></td>
                                                    <td class="text-center"><?php $dsc = ($dp/100)*$harga; echo "Rp " . number_format($dsc,0,',','.'); ?> - ( <?php echo $sc[$i]->diskon_produk ?>% )</td>
                                                    <td class="text-center"><?php echo $sc[$i]->qty?></td>
                                                    <td class="text-center">
                                                        <?php 

                                                            echo "Rp " . number_format($subx,0,',','.');

                                                            $cek = '';

                                                            if($qty == 1) {
                                                                $cek = 'disabled';
                                                            }

                                                        ?>
                                                            
                                                    </td>
                                                    <td class="text-center">

                                                        <a href="<?php echo base_url('Apps/operasiPO/inc/'.$sc[$i]->id_purchase.'/'.$sc[$i]->id_line) ?>" class="btn btn-xs btn-success" ><span class="fa fa-plus"></span></a>

                                                        <a <?php echo $cek ?> href="<?php echo base_url('Apps/operasiPO/dec/'.$sc[$i]->id_purchase.'/'.$sc[$i]->id_line) ?>" class="btn btn-xs btn-warning" href=""><span class="fa fa-minus"></span></a>

                                                        <a data-toggle="modal" data-target="#qty<?php echo $sc[$i]->id_line ?>" class="btn btn-xs btn-white" ><span class="fa fa-expand"></span></a>

                                                        <a data-toggle="modal" data-target="#harga<?php echo $sc[$i]->id_line ?>" class="btn btn-xs btn-white" ><span class="fa fa-dollar"></span></a>


                                                        <a href="<?php echo base_url('Apps/operasiPO/delete/'.$sc[$i]->id_purchase.'/'.$sc[$i]->id_line) ?>" class="btn btn-xs btn-white" href=""><span class="fa fa-trash"></span></a>

                                                        <div class="modal inmodal" id="qty<?php echo $sc[$i]->id_line ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content animated bounceInUp">

                                                                    <div class="modal-header">
                                                                        <h1> <span class="fa fa-expand text-info"></span> &nbsp&nbsp Set Jumlah Produk</h1>
                                                                    </div>

                                                                    <?php echo form_open('Apps/setQtyPurchase')?>

                                                                    <div class="modal-body">

                                                                        <div class="row">
                                                                            <div class="col-lg-3 text-right">
                                                                                <h5><b>JUMLAH</b></h5><br>
                                                                                
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                <input min="0" value="<?php echo $sc[$i]->qty?>" type="number" class="form-control input-sm" name="qty" required="">
                                                                                <input type="hidden" name="id_line" value="<?php echo $sc[$i]->id_line?>">
                                                                                <input type="hidden" name="id_purchase" value="<?php echo $sc[$i]->id_purchase?>">
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <button type="submit" class="btn btn-success" >Simpan</button>
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close() ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="modal inmodal" id="harga<?php echo $sc[$i]->id_line ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content animated bounceInUp">

                                                                    <div class="modal-header">
                                                                        <h1> <span class="fa fa-dollar text-info"></span> &nbsp&nbsp Set Harga dan Diskon Produk</h1>
                                                                    </div>

                                                                    <?php echo form_open('Apps/setHargaProdukPurchase')?>

                                                                    <div class="modal-body">

                                                                        <div class="row">
                                                                            <div class="col-lg-3 text-right">
                                                                                <h5><b>HARGA</b></h5><br>
                                                                                <h5><b>DISKON PRODUK</b></h5>
                                                                                <small class="text-muted">Potongan harga untuk setiap produk</small>
                                                                                
                                                                            </div>
                                                                            <div class="col-lg-9">
                                                                                
                                                                                <?php

                                                                                  $options = array( 

                                                                                  $sc[$i]->harga_beli => "Rp " . number_format($sc[$i]->harga_beli,0,',','.') . ' - Harga Beli',

                                                                                  $sc[$i]->harga_normal => "Rp " . number_format($sc[$i]->harga_normal,0,',','.') . ' - Harga Normal', 
                                                                                  $sc[$i]->harga_agen => "Rp " . number_format($sc[$i]->harga_agen,0,',','.') . ' - Harga Agen',  
                                                                                  $sc[$i]->harga_khusus => "Rp " . number_format($sc[$i]->harga_khusus,0,',','.') . ' - Harga Khusus' 
                                                                                  );

                                                                                  $extra = array();


                                                                                  $opsi = $options + $extra;

                                                                                  $js = array( 'class' => 'form-control' );
                                                                                  echo form_dropdown('harga', $opsi, $sc[$i]->harga ,$js);

                                                                                ?>

                                                                                <br>
                                                                                <input min="0" value="<?php echo $sc[$i]->diskon_produk?>" type="number" class="form-control input-sm" name="dp" required="">

                                                                                <input type="hidden" name="id_line" value="<?php echo $sc[$i]->id_line?>">
                                                                                <input type="hidden" name="id_purchase" value="<?php echo $sc[$i]->id_purchase?>">
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <button type="submit" class="btn btn-success" >Simpan</button>
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close() ?>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                </tr>
                                                
                                            <?php }?>

                                        </tbody>
                                        
                                    </table>

                                    <?php if(count($sc) > 0){ ?>
                                    
                                    <div class="row">
                                        <?php echo form_open('Apps/simpanKasirPurchase')?>

                                        <div class="col-lg-1">
                                            
                                        </div>

                                        <div class="col-lg-3 text-left ">
                                            <h5><b>Total Harga Awal</b></h5>
                                            <h2><?php echo "Rp " . number_format((int)$tot[0]->harga,0,',','.'); ?></h2>
                                            <br>
                                            <h5><b>Total Harga + Discount ( <?php echo $sc[0]->discount?>% )</b></h5>
                                            <h2><?php echo "Rp " . number_format($totx,0,',','.'); ?></h2>
                                            <br>
                                            <h5><b>Total Harga + Pajak ( <?php echo $sc[0]->pajak ?>% )</b></h5>
                                            <h2><?php echo "Rp " . number_format($grandtot,0,',','.'); ?></h2>
                                            
                                            
                                        </div>
                                        <div class="col-lg-3 text-left ">
                                            <h5><b>Discount</b></h5>
                                            <h2><?php echo "Rp " . number_format($dscval,0,',','.'); ?> - ( <?php echo $dscx?>% )</h2>
                                            <br>
                                            <h5><b>Pajak</b></h5>
                                            <h2><?php echo "Rp " . number_format($pjkval,0,',','.'); ?> - ( <?php echo $pjk?>% )</h2>
                                            <br>
                                            <h5><b>Kembalian</b></h5>
                                            <h2><span id="total"><?php echo "Rp " . number_format((int)$sc[0]->bayar-($grandtot),0,',','.'); ?></span></h2>

                                        </div>
                                        <div class="col-lg-2 text-left ">
                                            <h5><b>Pembayaran</b></h5>
                                            <input value="<?php echo $sc[0]->bayar ?>" type="number" name="pages" class="form-control" required="" id="pages"  />
                                            <br>
                                            <h5><b>Kembalian Asli</b></h5>
                                            <input value="<?php if((int)$sc[0]->kembalian > 0) {echo $sc[0]->kembalian;} ?>" type="number" name="kembalian" class="form-control" required="" id="kmb" />
                                            
                                        </div>
                                       
                                        <div class="col-lg-2 text-left ">
                                            <br>
                                            <input type="hidden" name="id_purchase" value="<?php echo $sc[0]->id_purchase?>">
                                            <input type="hidden" name="id_finance" value="<?php echo $sc[0]->id_finance?>">
                                            <input type="hidden" name="total" value="<?php echo (int)$tot[0]->harga ?>">
                                            <a data-toggle="modal" data-target="#pajak" class="btn btn-sm btn-block btn-danger"><b>PAJAK</b></a>
                                            <a data-toggle="modal" data-target="#discount" class="btn btn-sm btn-block btn-info"><b>DISCOUNT</b></a>
                                            <button type="submit" class="btn btn-sm btn-block btn-primary"><b>SELESAI</b></button>
                                        </div>
                                        <?php echo form_close()?>
  
                                    </div>

                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>
    <?php if(count($sc) > 0) { ?>
    <div class="modal inmodal" id="pajak" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content animated bounceInDown">

                <div class="modal-header">
                    <h1> Atur Pajak</h1>
                </div>

                <?php echo form_open('Apps/setPajakPurchase'); ?>

                <div class="modal-body text-center">

                    <div class="row">
                        <div class="col-lg-3 text-right">
                            <h5><b>PAJAK (%)</b></h5>
                            <small class="text-muted">Pengaturan pajak awal adalah 10 %</small>
                            
                        </div>
                        <div class="col-lg-9">
                            <input max="100" min="0" value="<?php echo $sc[0]->pajak?>" type="number" class="form-control input-sm" name="pajak" placeholder="ex : 10" required="">
                            
                            
                            <input type="hidden" name="id_purchase" value="<?php echo $sc[0]->id_purchase?>">
                            <input type="hidden" name="id_finance" value="<?php echo $sc[0]->id_finance?>">
                            <input type="hidden" name="tipe" value="so">
                        </div>
                    </div>
                    
                </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info" >Simpan</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                </div>

                <?php echo form_close() ?>

            </div>
        </div>
    </div>
    <div class="modal inmodal" id="discount" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content animated bounceInDown">

                <div class="modal-header">
                    <h1> Tambahkan Discount</h1>
                </div>

                <?php echo form_open('Apps/setDiscountPurchase'); ?>

                <div class="modal-body text-center">

                    <div class="row">
                        <div class="col-lg-3 text-right">
                            <h5><b>DISCOUNT</b></h5><br>
                            <h5><b>KETERANGAN</b></h5>
                        </div>
                        <div class="col-lg-9">
                            <input max="<?php echo (int)$tot[0]->harga?>" value="<?php echo $sc[0]->discount?>" type="number" class="form-control input-sm" name="discount" placeholder="ex : 10000" required="">
                            <br>
                            <textarea class="form-control" name="keterangan"><?php echo $sc[0]->ket_kasir?></textarea>
                            <input type="hidden" name="id_purchase" value="<?php echo $sc[0]->id_purchase?>">
                            <input type="hidden" name="id_finance" value="<?php echo $sc[0]->id_finance?>">
                            <input type="hidden" name="tipe" value="so">
                        </div>
                    </div>
                    
                </div>
                
                <div class="modal-footer">
                    <button type="submit" class="btn btn-info" >Simpan</button>
                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                </div>

                <?php echo form_close() ?>

            </div>
        </div>
    </div>
    <?php }?>

    <div class="modal inmodal" id="new" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content animated bounceInDown">

                <div class="modal-header">
                    <h1> <span class="fa fa-check text-success"></span> Transaksi Baru</h1>
                </div>

                <div class="modal-body text-center">

                    <p>Silahkan membuat transaksi baru, semua transaksi sebelumnya telah selesai</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal inmodal" id="old" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content animated bounceInDown">

                <div class="modal-header">
                    <h1> <span class="fa fa-warning text-warning"></span> Transaksi Belum Selesai</h1>
                </div>

                <div class="modal-body text-center">

                    <p>Transaksi sebelumnya belum selesai, silahkan selesaikan terlebih dahulu</p>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                </div>

            </div>
        </div>
    </div>

    <?php 

    

    ?>

   

    <?php include('footer.php')?>

    <?php if($tipe == 'old') {?>

         <script type="text/javascript">
        $(window).on('load',function(){
            $('#old').modal('show');
        });
       </script>

    <?php } else if($tipe == 'new') {?>
         <script type="text/javascript">
            $(window).on('load',function(){
                $('#new').modal('show');
            });
        </script>

    <?php } else {} ?>

   

    <script>

        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>


</body>

</html>
