<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Nota</title>

     <?php include('header.php')?>

     <style type="text/css">
         @media print {
          .printPageButton {
            display: none;
          }
        }
     </style>


</head>

<body class="white-bg">
    <div class="wrapper wrapper-content ">
        <div class="ibox-content">
                <div class="row">
                    <div class="col-xs-6">
                        <address style="font-size: 15px">
                            <strong>TOKO HASIL LAUT</strong><br>
                            Jalan Mojopahit, Gang Sedap Malam<br>
                            Karang Tuban<br>
                            <abbr >Telp :</abbr> 081216741952
                        </address>
                    </div>

                    <div class="col-xs-6 text-right">
                        

                        <?php if($tipe == 'po') {?>

                            <h2><?php echo $sup[0]->nama_supplier; ?></h2>

                            <h4 >No. Nota &mdash; <span class="text-navy"><?php echo $sc[0]->id_purchase ?></span> </h4>

                        <?php } else {?>
                            
                            
                            
                            <?php if($statx == 'agen') { ?>

                                <h2><?php echo $ag[0]->nama_agen; ?></h2>


                            <?php } ?>

                            <h4 >No. Nota &mdash; <span class="text-navy"><?php echo $sc[0]->id_sales ?></span> </h4>


                        <?php }?>

                        <p>
                            <small><strong></strong> <?php echo $sc[0]->tgl ?></small><br/>
                            
                        </p>
                    </div>
                </div>

                <style type="text/css">
                    .table_morecondensed>thead>tr>th, 
                    .table_morecondensed>tbody>tr>th, 
                    .table_morecondensed>tfoot>tr>th, 
                    .table_morecondensed>thead>tr>td, 
                    .table_morecondensed>tbody>tr>td, 
                    .table_morecondensed>tfoot>tr>td{ padding: 2px; }
                </style>


                <div class="table-responsive m-t">
                    <table class="table table_morecondensed">
                        <thead>
                        <tr>
                            <th class="text-center">No.</th>
                            <th class="text-left">Nama Produk</th>
                            <th class="text-center">Jumlah</th>
                            <th class="text-center">HPP @</th>
                            <th class="text-center">PPN @</th>
                            <th class="text-center">Harga @</th>
                            <th class="text-center">Diskon @</th>
                            <th class="text-center">Sub-Total</th>
                        </tr>
                        </thead>
                        <tbody>
                            
                            <?php $tot_hpp = 0; $tot_ppn = 0; ?>

                            <?php for ($i=0; $i < count($sc) ; $i++) { ?>

                                <?php 
                                        $harga = (int)$sc[$i]->harga;
                                        $qty = (int)$sc[$i]->qty;
                                        $dp = (int)$sc[$i]->diskon_produk;

                                        $subx = (((100-$dp)/100)*$harga)*$qty;
                                        $dsc = ($dp/100)*$harga;
                                        $hpp = (100/111)*$sc[$i]->harga;
                                        $hpp2 = ceil($hpp/10)*10;
                                        $ppn = 0.11*$hpp;
                                        $ppn2 = ceil($ppn/10)*10;

                                        $sub_hpp = $hpp*$qty;
                                        $sub_ppn = $ppn*$qty;

                                        $sub_ppn2 = ceil($sub_ppn/10)*10;

                                        $tot_ppn += $sub_ppn;
                                        $tot_hpp += $sub_hpp;

                                        $tot_ppn2 = ceil($tot_ppn/10)*10;
                                        $tot_hpp2 = ceil($tot_hpp/10)*10;
                                ?>

                                <tr>
                                    <td class="text-center"><?php echo ($i+1)?></td>
                                    <td class="text-left"><?php echo $sc[$i]->nama?></td>
                                    <td class="text-center"><?php echo $sc[$i]->qty?> Unit</td>
                                    <td class="text-center"><?php echo "Rp " . number_format($hpp2,0,',','.'); ?></td>
                                    <td class="text-center"><?php echo "Rp " . number_format($ppn2,0,',','.'); ?></td>
                                    <td class="text-center"><?php echo "Rp " . number_format($sc[$i]->harga,0,',','.'); ?></td>
                                    <td class="text-center"><?php  echo "Rp " . number_format($dsc,0,',','.'); ?> - ( <?php echo $sc[$i]->diskon_produk ?>% )</td>
                                    
                                    
                                    
                                    <td class="text-center">
                                        
                                        <?php 

                                            $harga = (int)$sc[$i]->harga;
                                            $qty = (int)$sc[$i]->qty;
                                            $dp = (int)$sc[$i]->diskon_produk;

                                            $subx = (((100-$dp)/100)*$harga)*$qty;

                                            echo "Rp " . number_format($subx,0,',','.');

                                        ?>
                                            
                                    </td>

                                </tr>
                                
                            <?php }?>

                        

                        </tbody>
                    </table>
                </div>

                <div class="row">
                    

                    <div class="col-lg-12">

                        <?php $totx = ((100-(int)$sc[0]->discount)/100)*(int)$tot[0]->harga; ?>

                        <?php if($tipe == 'kasir') { ?>

                        <div class="row text-right">
                            <div class="col-xs-3">

                                <span><strong>Sub-Total :</strong></span>
                                <span><?php echo "Rp " . number_format((int)$tot[0]->harga,0,',','.'); ?></span>
                                
                            </div>
                            <div class="col-xs-3">
                                <span><strong>Discount ( <?php echo (int)$sc[0]->discount?> % ) :</strong></span>
                                <span><?php echo "Rp " . number_format(((int)$sc[0]->discount/100)*(int)$tot[0]->harga,0,',','.'); ?></span>
                            </div>

                            <div class="col-xs-3">

                                <span><strong>Total :</strong></span>
                                <span><?php echo "Rp " . number_format($totx,0,',','.'); ?></span>

                                
                            </div>

                            <div class="col-xs-3">

                                <span><strong>Pembayaran :</strong></span>
                                <td><?php echo "Rp " . number_format((int)$sc[0]->bayar,0,',','.'); ?></td>
                                
                            </div>

                            <div class="col-xs-3">

                                <span><strong>Kembalian :</strong></span>
                                <td><?php echo "Rp " . number_format((int)$sc[0]->bayar-($totx),0,',','.'); ?></td>
                                
                            </div>
              

                        </div>


                        <?php } else {?>

                        <?php 

                        $totawal = (int)$tot[0]->harga;
                        $dscx = (int)$sc[0]->discount;

                        $dscval = ($dscx/100)*$totawal;
                        $totx = $totawal - $dscval;

                        $pjk = (int)$sc[0]->pajak;
                        $pjkval = ($pjk/100)*$totx;
                        $grandtot = $totx + $pjkval;

                        ?>

                        <div class="row text-right">
                            <div class="col-xs-3">

                                <span><strong>Sub-Total :</strong></span>
                                <span><?php echo "Rp " . number_format((int)$tot[0]->harga,0,',','.'); ?></span>
                                
                            </div>
                            <div class="col-xs-3">
                                <span><strong>Discount ( <?php echo $dscx ?>% ) :</strong></span>
                                <span><?php echo "Rp " . number_format($dscval,0,',','.'); ?></span>
                            </div>

                            <div class="col-xs-3">

                                <span><strong>Pajak ( <?php echo $sc[0]->pajak ?>% ) :</strong></span>
                                <span><?php echo "Rp " . number_format(((float)$sc[0]->pajak/100)*((int)$tot[0]->harga),0,',','.'); ?></span>
                                
                            </div>

                            <div class="col-xs-3">

                                <span><strong>Total</strong></span>
                                <span><?php echo "Rp " . number_format($grandtot,0,',','.'); ?></span>
                                
                            </div>

                            <div class="col-xs-3">

                                <span><strong>Pembayaran :</strong></span>
                                <td><?php echo "Rp " . number_format((int)$sc[0]->bayar,0,',','.'); ?></td>
                                
                            </div>

                            <div class="col-xs-3">

                                <span><strong>Kembalian :</strong></span>
                                <td><?php echo "Rp " . number_format((int)$sc[0]->kembalian,0,',','.'); ?></td>
                                
                            </div>

                            <div class="col-xs-3">

                                <span><strong>Total HPP :</strong></span>
                                <td><?php echo "Rp " . number_format((int)$tot_hpp2,0,',','.'); ?></td>
                                
                            </div>

                            <div class="col-xs-3">

                                <span><strong>Total PPN :</strong></span>
                                <td><?php echo "Rp " . number_format((int)$tot_ppn2,0,',','.'); ?></td>
                                
                            </div>
              

                        </div>

                     

                        <?php } ?>
                        
                    </div>

                    <br class="printPageButton"><br><hr class="printPageButton">

                    

                    <?php if($statx == 'agen') { ?>

                        

                    <!-- <h3 class="text-center  text-muted"><em>Histori Pembayaran</em> </h3> -->
                    <br class="printPageButton">
                    

                    <div class="col-lg-12">

                        <table class="table table-hover table_morecondensed table-strip">
                            <thead>
                                <tr>
                                    <th class="text-center">No. Nota</th>
                                    <th class="text-center">Tanggal</th>
                                    <th class="text-center">Total Harga</th>
                                    <th class="text-center">Pembayaran</th>
                                    <th class="text-center">Saldo (-) / Nitip (+)</th>

                                </tr>
                            </thead>
                            <tbody>

                                <?php 

                                $max = count($so);
                                if($max > 2) {
                                    $max = 2;
                                }

                                ?>

                                <?php for($i=0; $i < $max; ++$i) { ?>

                                <tr>
                                    <td class="text-center"><?php echo $so[$i]->id_sales ?></td>
                                    <td class="text-center"><?php echo $so[$i]->tgl ?></td>
                                    <td class="text-center">

                                        <?php 

                                        $totawal = (int)$so[$i]->harga;
                                        $dscx = (int)$so[$i]->discount;

                                        $dscval = ($dscx/100)*$totawal;
                                        $totx = $totawal - $dscval;

                                        $pjk = (int)$so[$i]->pajak;
                                        $pjkval = ($pjk/100)*$totx;
                                        $grandtot = $totx + $pjkval;

                                        ?>
                            
                                        <?php 

                                            echo "Rp " . number_format($grandtot,0,',','.'); 

                                        ?>
                                            
                                    </td>
                                    <td class="text-center"><?php echo "Rp " . number_format($so[$i]->bayar,0,',','.'); ?></td>
                                    <td class="text-center">
                                        <?php 

                                        $x = (int)$so[$i]->bayar-($grandtot)-(int)$so[$i]->kembalian;
                                        echo "Rp " . number_format($x,0,',','.');;

                                    ?></td>

                                    <?php } ?>

                                </tr>
                                
                            </tbody>
                        </table> 
                        <?php 

                            $totq = 0;
                            for ($i=0; $i < count($so); $i++) { 

                                $totawal = (int)$so[$i]->harga;
                                $dscx = (int)$so[$i]->discount;

                                $dscval = ($dscx/100)*$totawal;
                                $totx = $totawal - $dscval;

                                $pjk = (int)$so[$i]->pajak;
                                $pjkval = ($pjk/100)*$totx;
                                $grandtot = $totx + $pjkval;

                                $eue = (int)$so[$i]->bayar-($grandtot)-(int)$so[$i]->kembalian;
                                $totq += $eue;
                                
                            }

                            for ($i=0; $i < count($ci); $i++) { 
                                
                                $cil = (int)$ci[$i]->bayar;
                                $totq += $cil;
                            }

                            $stat = 'Saldo Total Agen';

                            if($totq > 0) {
                                $stat = 'Nitip Total Agen';
                            }


                        ?>

                        <label><?php echo $stat ?></label>
                        <h2 style="font-size: 30px"><?php echo "Rp " . number_format(abs($totq),0,',','.');  ?></h2>
                        
                    </div>

                    <?php } ?>
                </div>


                <div class="well m-t"><strong>Terima Kasih</strong>
                     Telah Berbelanja di Toko Hasil Laut. Selamat Bebelanja Kembali
                </div>
                <div class="row">
                    <div class="col-sm-1 pull-right">
                        <?php if($tipe == 'po') {?>

                            <a class=" printPageButton pull-right btn-block btn btn-xs btn-success" href="<?php echo base_url('Apps/pembelian') ?>">Kembali</a>

                        <?php } else {?>
                            
                            <a class=" printPageButton pull-right btn-block btn btn-xs btn-success" href="<?php echo base_url('Apps/penjualan') ?>">Kembali</a>

                        <?php }?>

                        
                    </div>
                    <div class="col-sm-1 pull-right">
                        <button class=" printPageButton pull-right btn-block btn-primary btn btn-xs" onclick="window.print();">Print</button>
                    </div>

                    <div class="col-sm-2 pull-right">
                        <?php if($tipe == 'so') {?>

                            <a class=" printPageButton pull-right btn-block btn btn-xs btn-danger" href="<?php echo base_url('Apps/exportExcel/'.$id) ?>">Export To Excel</a>

                        <?php } ?>
                    </div>
                </div>
            </div>



</body>

</html>
