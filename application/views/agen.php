<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Agen</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Kelola Agen</h2>
                        <p class="font-bold">Halaman ini bertujuan untuk mengolah agen dalam aplikasi ini, agen adalah pihak yang membeli produk</p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    <?php echo $this->session->flashdata('msg1'); ?>
                    
                    <div class="row">
                        <div class="col-lg-4 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Tambah Agen</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="form-group">

                                        <?php echo form_open('Apps/tambahagen')?>
                                        
                                        <label>Nama Agen </label>
                                        <input type="text" name="nama_agen" class="form-control" required placeholder="ex : Giant Dept. Store" >
                                        <br>

                                        <label>Kategori</label>
                                        <?php

                                          $options = array( 

                                          'Biasa' => 'Biasa', 
                                          'Khusus' => 'Khusus', 

                                          );

                                          $js = array( 'class' => 'form-control' );
                                          echo form_dropdown('kategori', $options, "Biasa" ,$js);

                                        ?>
                                        <br>

                                        <label>Nomor HP</label>
                                        <input type="number" min="0" name="nomor" class="form-control" placeholder="ex : 082141295967" >
                                        <br>

                                        <label>Alamat</label>
                                        <textarea class="form-control" name="alamat"></textarea>

                                        <br>
                                        <label>NPWP</label>
                                        <input type="text" min="0"  name="npwp" class="form-control" placeholder="Angkanya saja" >
                                        <br>
                                        
                                        <br>

                                        <div class="text-right">
                                            <button type="submit" class="btn btn-success text-right ">Tambah</button>    
                                        </div>
                                        

                                        <?php echo form_close(); ?>

                                    </div>

                                </div>
                            
                            </div>
                        </div>
                        <div class="col-lg-8 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Agen</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="20" data-filter=#filter>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Agen</th>
                                                        <th class="text-center">Kategori</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    <?php for ($i=0; $i < count($agen); $i++) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center"><?php echo $agen[$i]->id_agen?></td>
                                                            <td><?php echo $agen[$i]->nama_agen?></td>
                                                            <td class="text-center"><?php echo $agen[$i]->kategori?></td>
                                                            <td class="text-center">

                                                                <a href="<?php echo base_url('Apps/harga/'.$agen[$i]->id_agen) ?>"  class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a>

                                                                <a data-toggle="modal" data-target="#edit<?php echo $agen[$i]->id_agen ?>" class="btn btn-warning btn-xs"><span class="fa fa-pencil"></span></a>

                                                                <a data-toggle="modal" data-target="#hapus<?php echo $agen[$i]->id_agen ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>

                                                            

                                                                <div class="modal inmodal" id="edit<?php echo $agen[$i]->id_agen ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/editagen')?>

                                                                            <div class="modal-header">

                                                                                <h1><input type="form-control" name="nama_agen" value="<?php echo $agen[$i]->nama_agen?>"></h1>

                                                                            </div>

                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-lg-4">
                                                                                        <label>Kategori</label><br>
                                                                                        <?php

                                                                                          $options = array( 

                                                                                          'Biasa' => 'Biasa', 
                                                                                          'Khusus' => 'Khusus', 

                                                                                          );

                                                                                          $js = array( 'class' => 'form-control' );
                                                                                          echo form_dropdown('kategori', $options, $agen[$i]->kategori ,$js);

                                                                                        ?>
                                                                                        
                                                                                        
                                                                                    </div>
                                                                                    <div class="col-lg-4">
                                                                                        <label>Nomor HP</label><br>
                                                                                        <input type="number" min="0" name="nomor" class="form-control" value="<?php echo $agen[$i]->nomor?>">
                                                                                        
                                                                                    </div>

                                                                                    <div class="col-lg-4">
                                                                                        <label>NPWP</label><br>
                                                                                        <input type="number" min="0" required pattern="[0-9]{15}" name="npwp" class="form-control" value="<?php echo $agen[$i]->npwp?>">
                                                                                        
                                                                                    </div>
                                                                                    
                                                                                </div>
                                                                                <br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <label>Alamat</label><br>
                                                                                        <input type="text" name="alamat" class="form-control" value="<?php echo $agen[$i]->alamat?>">
                                                                                        
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-success">Simpan</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                                <input type="hidden" name="id_agen" value="<?php echo $agen[$i]->id_agen?>">
                                                                            </div>

                                                                            <?php echo form_close() ?>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal inmodal" id="hapus<?php echo $agen[$i]->id_agen ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content animated bounceInUp">


                                                                            <?php echo form_open('Apps/hapusagen')?>

                                                                            <div class="modal-body text-center">
                                                                                <h1>Apakah anda yakin untuk menghapus <b><?php echo $agen[$i]->nama_agen ?> ?</b> </h1>
                                                                                <br>
                                                                                <small>Anda tidak dapat mengembalikan produk yang telah terhapus</small>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id_agen" value="<?php echo $agen[$i]->id_agen?>">
                                                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                            <?php echo form_close(); ?>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>


</body>

</html>
