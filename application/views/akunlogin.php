<li class="nav-header">
    <div class="dropdown profile-element">
        <span>
            <img alt="image" class="img-circle" src="<?php echo base_url('assets/img/profile_small.jpg') ?>" />
        </span>
        
        <br><br>

        <a>Halo, <strong class="font-bold font-white"><?php echo $this->session->userdata['logged_in']['nama']?></strong>
        <br>
        <small class="text-muted">Version 1.2</small></a>
    </div>
    <div class="logo-element">
        IN+
    </div>
</li>