<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Produk</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Kelola Produk</h2>
                        <p class="font-bold">Halaman ini bertujuan untuk mengolah produk dalam aplikasi ini, user dapat menambah, mencari, mengedit, dan menghapus produk</p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    
                    <div class="row">
                        <div class="col-lg-4 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Tambah Produk</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="form-group">

                                        <?php echo form_open('Apps/tambahproduk')?>
                                        
                                        <label>Nama Produk</label>
                                        <input type="text" name="nama" class="form-control" required placeholder="ex : Nugget So Good 450 gr Sapi" >
                                        <br>

                                        <label>Produsen Produk</label><br>
                                        <small>Apabila tidak ada, tambahkan melalui menu produsen</small><br><br>
                                        <?php

                                              $js = array( 'class' => 'form-control' );

                                              echo form_dropdown('id_produsen', $produsen, set_value('id_produsen') ,$js);

                                          ?>
                                        <br>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <label>Berat (gr)</label>
                                                <input type="number" min="0" name="berat" class="form-control" required placeholder="ex : 450" >
                                                <br>
                                            </div>

                                            <div class="col-lg-6">
                                                <label>Isi per Pack (pcs)</label>
                                                <input type="number" min="0" name="isi" class="form-control" required placeholder="ex : 25" >
                                                <br>
                                            </div>

                                        </div>

                                        <label>Harga Beli ( dari Supplier )  </label>
                                        <input type="number" min="0" name="harga_beli" class="form-control" required placeholder="ex : 15000" >
                                        <br>

                                        <label>Harga Jual ( Normal )  </label>
                                        <input type="number" min="0" name="harga_normal" class="form-control" required placeholder="ex : 20000" >
                                        <br>

                                        <label>Harga Jual ( Agen )  </label>
                                        <input type="number" min="0" name="harga_agen" class="form-control" required placeholder="ex : 18000" >
                                        <br>

                                        <label>Harga Jual ( Khusus )  </label>
                                        <input type="number" min="0" name="harga_khusus" class="form-control" placeholder="ex : 17000" >
                                        <br>

                                        <label>Keterangan Tambahan</label>
                                        <textarea class="form-control" name="keterangan"></textarea>
                                        <br>

                                        <div class="text-right">
                                            <button type="submit" class="btn btn-success text-right ">Tambah</button>    
                                        </div>

                                        <?php echo form_close(); ?>

                                    </div>

                                </div>
                            
                            </div>
                        </div>

                        <div class="col-lg-8 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Cari Produk</h5>
                                </div>

                                <div class="ibox-content">

                                    <?php echo form_open('Apps/cariProduk')?>
                                        
                                        <label>ID Produk / Nama Produk </label>
                                        <input type="text" name="param" class="form-control" required placeholder="ex : 22 atau Chicken Sausage" >
                                        <input type="hidden" name="tipe" value="produk">
                                        
                                        <br>

                                        <div class="text-right">
                                            <button type="submit" class="btn btn-success text-right ">Cari</button>    
                                        </div>
                                        

                                    <?php echo form_close(); ?>

                                </div>
                            </div>

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h5>Hasil Pencarian Produk </h5>        
                                        </div>
                                       
                                    </div>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="10" data-filter=#filter>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Produk</th>
                                                        <th class="text-center">Ditambahkan</th>
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    <?php $prod = $this->session->flashdata('res');?>

                                                    <?php for ($i=0; $i < count($prod); $i++) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center"><?php echo $prod[$i]->id_produk?></td>
                                                            <td><?php echo $prod[$i]->nama?></td>
                                                            <td class="text-center"><?php echo $prod[$i]->tgl?></td>
                                                            <td class="text-center">

                                                                <a data-toggle="modal" data-target="#lihat<?php echo $prod[$i]->id_produk ?>" class="btn btn-primary btn-xs"><span class="fa fa-eye"></span></a>

                                                                <a data-toggle="modal" data-target="#edit<?php echo $prod[$i]->id_produk ?>" class="btn btn-warning btn-xs"><span class="fa fa-pencil"></span></a>
                                                                <a data-toggle="modal"  data-target="#hapus<?php echo $prod[$i]->id_produk ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>

                                                                <div class="modal inmodal" id="lihat<?php echo $prod[$i]->id_produk ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <div class="modal-header">
                                                                                <h1><b>[<?php echo $prod[$i]->id_produk?>]</b> - <?php echo $prod[$i]->nama?></h1>
                                                                            </div>

                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-lg-3">
                                                                                        <label>Berat Produk</label><br>
                                                                                        <span><?php echo $prod[$i]->berat?> gram</span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Isi per Pack</label><br>
                                                                                        <span><?php echo $prod[$i]->isi?> Item</span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Produsen</label><br>
                                                                                        <span><?php echo $prod[$i]->nama_produsen?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Ditambahkan Pada</label><br>
                                                                                        <span><?php echo $prod[$i]->tgl?> </span>
                                                                                    </div>
                                                                                </div>
                                                                                <br><br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Beli</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_beli,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Normal</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_normal,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Agen</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_agen,0,',','.'); ?></span>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Khusus</label><br>
                                                                                        <span><?php echo "Rp " . number_format($prod[$i]->harga_khusus,0,',','.'); ?></span>
                                                                                    </div>

                                                                                    
                                                                                </div>
                                                                                <br><br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <label>Keterangan</label><br>
                                                                                        <span><?php echo $prod[$i]->keterangan?> </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            
                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal inmodal" id="edit<?php echo $prod[$i]->id_produk ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/editproduk')?>

                                                                            <div class="modal-header">

                                                                                <h1><input type="form-control" name="nama" value="<?php echo $prod[$i]->nama?>"></h1>

                                                                            </div>

                                                                            <div class="modal-body">
                                                                                <div class="row">
                                                                                    <div class="col-lg-3">
                                                                                        <label>Berat Produk</label><br>
                                                                                        <input type="number" min="0" name="berat" class="form-control" value="<?php echo $prod[$i]->berat?>">
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Isi per Pack</label><br>
                                                                                        <input type="number" min="0" name="isi" class="form-control" value="<?php echo $prod[$i]->isi?>">
                                                                                        
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Produsen</label><br>
                                                                                        <?php

                                                                                              $js = array( 'class' => 'form-control' );

                                                                                              echo form_dropdown('id_produsen', $produsen,  $prod[$i]->id_produsen ,$js);

                                                                                          ?>
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Ditambahkan Pada</label><br>
                                                                                        <input type="text" disabled class="form-control" value="<?php echo $prod[$i]->added_on?>">
                                                                                    </div>
                                                                                </div>
                                                                                <br><br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Beli</label><br>
                                                                                        <input type="number" min="0" name="harga_beli" class="form-control" value="<?php echo $prod[$i]->harga_beli?>">
                                                                                        
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Normal</label><br>
                                                                                        <input type="number" min="0" name="harga_normal" class="form-control" value="<?php echo $prod[$i]->harga_normal?>">
                                                                                        
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Agen</label><br>
                                                                                        <input type="number" min="0" name="harga_agen" class="form-control" value="<?php echo $prod[$i]->harga_agen?>">
                                                                                        
                                                                                    </div>
                                                                                    <div class="col-lg-3">
                                                                                        <label>Harga Khusus</label><br>
                                                                                        <input type="number" min="0" name="harga_khusus" class="form-control" value="<?php echo $prod[$i]->harga_khusus?>">
                                                                                        
                                                                                    </div>
                                                                                </div>

                                                                                <br><br>
                                                                                <div class="row">
                                                                                    <div class="col-lg-12">
                                                                                        <label>Keterangan</label><br>
                                                                                        <textarea class="form-control" name="keterangan"><?php echo $prod[$i]->keterangan ?></textarea>
                                                                                    </div>
                                                                                </div>

                                                                            </div>
                                                                            
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-success">Simpan</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                                <input type="hidden" name="id_produk" value="<?php echo $prod[$i]->id_produk?>">
                                                                            </div>

                                                                            <?php echo form_close() ?>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal inmodal" id="hapus<?php echo $prod[$i]->id_produk ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/hapusproduk')?>

                                                                            <div class="modal-body text-center">
                                                                                <h1>Apakah anda yakin untuk menghapus <b><?php echo $prod[$i]->nama ?> ?</b> </h1>
                                                                                <br>
                                                                                <small>Anda tidak dapat mengembalikan produk yang telah terhapus</small>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id_produk" value="<?php echo $prod[$i]->id_produk?>">
                                                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                            <?php echo form_close() ?>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                       
                        <div class="col-lg-12 animated fadeInDown">
                            

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <h5>Daftar Produk </h5>        
                                        </div>
                                       
                                    </div>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            
                                            <table class="table table-stripped" >
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Produk</th>
                                                        <th class="text-center">Ditambahkan</th>
                                                        
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    <?php $prod = $produk;?>

                                                    <?php for ($i=0; $i < count($prod); $i++) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center"><?php echo $prod[$i]->id_produk?></td>
                                                            <td><?php echo $prod[$i]->nama?></td>
                                                            <td class="text-center"><?php echo $prod[$i]->tgl?></td>
                                                            
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>


</body>

</html>
