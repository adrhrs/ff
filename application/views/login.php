<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Login</title>

    <?php include('header.php')?>

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

                <h1 class="logo-name">THL</h1>

            </div>
            <h3>Welcome to Toko Hasil Laut</h3>
            <p>
               Aplikasi ini bertujuan untuk membantu proses administrasi perusahaan 
            </p>
            <p>Masuk Menggunakan Akun Anda</p>
            
            <?php echo form_open('Login/proseslogin')?>

                <div class="form-group animated fadeInLeftBig">
                    <input type="text" class="form-control" placeholder="username" name="username" required="">
                </div>
                <div class="form-group animated fadeInRightBig">
                    <input type="password" class="form-control" placeholder="password" required="" name="password">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b animated fadeInUp">Login</button>
            
            <?php echo form_close()?>

            <p class="m-t animated fadeInUp"> <small>Developed by Adrian Afnandika &copy; 2020</small> </p>
        </div>
    </div>

    <?php include('footer.php')?>

</body>

</html>
