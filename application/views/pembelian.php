<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Pembelian</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Kelola Pembelian</h2>
                        <p class="font-bold">Halaman ini bertujuan untuk mengolah seluruh transaksi pembelilan </p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>

                    <div class="row">
                        <div class="col-lg-4">
                            <div class="ibox ">
                                <div class="ibox-title">

                                    <h5>Pengeluaran Keseluruhan  &mdash; <i class="text-muted"><?php echo date('M Y'); ?></i></h5>
                                </div>
                                <div class="ibox-content">

                                     <?php $tot_po = 0; $jml_lunas = 0; $jml_blm = 0; $tot_lunas = 0; $tot_blm = 0; for($i=0; $i < count($po); ++$i) {

                                            $totawal = (int)$po[$i]->harga;
                                            $dscx = (int)$po[$i]->discount;

                                            $dscval = ($dscx/100)*$totawal;
                                            $totx = $totawal - $dscval;

                                            $pjk = (int)$po[$i]->pajak;
                                            $pjkval = ($pjk/100)*$totx;
                                            $grandtot = $totx + $pjkval;

                                            $tot_po += $grandtot;

                                            if((int)$po[$i]->bayar <= $grandtot-1) { 

                                                $jml_blm += 1;
                                                $tot_blm += $grandtot;

                                            } else {

                                                

                                                $jml_lunas += 1;
                                                $tot_lunas += $grandtot;

                                            }

                                        } ?>

                                   

                                    <h1 align="justify" ><?php echo "Rp " . number_format($tot_po,0,',','.'); ?></h1>
                                    <hr>
                                    <div class="stat-percent font-bold text-warning"><?php echo count($po); ?> <i class="fa fa-shopping-cart"></i></div>

                                    <small>Total Transaksi</small>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-lg-4">
                            <div class="ibox ">
                                <div class="ibox-title">

                                    <h5>Pembelian Lunas  &mdash; <i class="text-muted"><?php echo date('M Y'); ?></i></h5>
                                </div>
                                <div class="ibox-content">

                                    

                                    <h1 align="justify" ><?php echo $jml_lunas ?> PO</h1>
                                    <hr>
                                    <div class="stat-percent font-bold text-info"><?php echo "Rp " . number_format($tot_lunas,0,',','.'); ?> </div>

                                    <small>Pengeluaran Lunas</small>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="ibox ">
                                <div class="ibox-title">

                                    <h5>Pembelian Belum Lunas  &mdash; <i class="text-muted"><?php echo date('M Y'); ?></i></h5>
                                </div>
                                <div class="ibox-content">

                                    

                                    <h1 align="justify" ><?php echo $jml_blm ?> PO</h1>
                                    <hr>
                                    <div class="stat-percent font-bold text-danger"><?php echo "Rp " . number_format($tot_blm,0,',','.'); ?> </div>

                                    <small>Pengeluaran Belum Lunas</small>
                                </div>
                            </div>
                        </div>
                        

                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12 animated fadeInDown">



                            <div id="supplierx" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <h5>Transaksi Pembelian Supplier</h5>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <button data-toggle="modal" data-target="#supplier" class="btn btn-xs btn-info" ><b>+ TAMBAH TRANSAKSI</b></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    <input type="text" class="form-control input-sm m-b-xs" id="filter2" placeholder="Search in table">

                                    <table class="footable table table-stripped" data-page-size="5" data-filter=#filter2>
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th>Supplier</th>
                                                <th class="text-center">Tanggal Pembelian</th>
                                                <th class="text-center">Tanggal Deadline</th>
                                                <th class="text-center">Total Harga</th>
                                                <th class="text-center">Pembayaran</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                            <?php for($i=0; $i < count($po); ++$i) { ?>

                                                <tr>
                                                    <td class="text-center"><?php echo $po[$i]->id_purchase ?></td>
                                                    <td ><a href="<?php echo base_url('Apps/harga/'.$po[$i]->id_supplier)?>"><?php echo $po[$i]->nama_supplier?></a></td>
                                                    <td class="text-center"><?php echo $po[$i]->tgl ?></td>
                                                    <td class="text-center"><?php echo $po[$i]->deadline ?></td>
                                                    <td class="text-center">

                                                        <?php 

                                                        $totawal = (int)$po[$i]->harga;
                                                        $dscx = (int)$po[$i]->discount;

                                                        $dscval = ($dscx/100)*$totawal;
                                                        $totx = $totawal - $dscval;

                                                        $pjk = (int)$po[$i]->pajak;
                                                        $pjkval = ($pjk/100)*$totx;
                                                        $grandtot = $totx + $pjkval;

                                                        ?>
                                                    
                                                        <?php 

                                                            

                                                            echo "Rp " . number_format($grandtot,0,',','.'); 

                                                        ?>
                                                            
                                                    </td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$po[$i]->bayar,0,',','.'); ?></td>

                                                    <td class="text-center">
                                                        <?php if((int)$po[$i]->bayar <= $grandtot-1) { ?>


                                                            <label class="label label-success"><?php echo $po[$i]->sisa?> Hari lagi</label>
                                                            <label class="label label-default">Belum Lunas</label>

                                                        <?php } else { ?>

                                                            <label class="label label-default">Lunas</label>

                                                        <?php }?>
                                                    </td>
                                            
                                                    <td class="text-center">
                                                        <a class="btn btn-xs btn-primary" href="<?php echo base_url('Apps/nota/po/'.$po[$i]->id_purchase) ?>"><span class="fa fa-eye"></span></a>
                                                        <a class="btn btn-xs btn-warning" href="<?php echo base_url('Apps/tambahPO/ops/'.$po[$i]->id_purchase) ?>"><span class="fa fa-pencil"></span></a>
                                                        <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#hapus<?php echo $po[$i]->id_purchase ?>" ><span class="fa fa-trash"></span></a>

                                                        <div class="modal inmodal" id="hapus<?php echo $po[$i]->id_purchase ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content animated bounceInUp">

                                                                    <?php echo form_open('Apps/hapusNota')?>

                                                                    <div class="modal-body text-center">
                                                                        <h1>Apakah anda yakin untuk menghapus transaksi <b><?php echo $po[$i]->id_purchase ?> ?</b> </h1>
                                                                        <br>
                                                                        <small>Anda tidak dapat mengembalikan transaksi yang telah terhapus</small>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="id_purchase" value="<?php echo $po[$i]->id_purchase ?>">
                                                                        <input type="hidden" name="tipe" value="po">
                                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close() ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                  
                                        </tbody>



                                        <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>



                        </div>
                        
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

    <div class="modal inmodal" id="supplier" tabindex="-1" role="dialog" aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content animated bounceInDown">

                <div class="modal-header">
                    <h1> <span class="fa fa-person"></span> Daftar Supplier</h1>
                </div>

                <div class="modal-body">

                    <input type="text" class="form-control input-sm m-b-xs" id="filter3" placeholder="Cari Supplier">

                    <table class="footable table table-stripped" data-page-size="10" data-filter=#filter3>
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th>Nama Supplier</th>
                                <th class="text-center">Transaksi</th>

                            </tr>
                        </thead>

                        <tbody>

                            <?php for($i=0; $i < count($supplier); $i++) { ?>

                                <tr>
                                    <td class="text-center"><?php echo $supplier[$i]->id_supplier?></td>
                                    <td><?php echo $supplier[$i]->nama_supplier?></td>
                                    <td class="text-center">
                                        <a href="<?php echo base_url('Apps/redirectpo/'.$supplier[$i]->id_supplier) ?>" class="btn btn-xs btn-success" >Purchase Order</a>

                                    </td>
                                </tr>

                            <?php } ?>

                        </tbody>

                        <tfoot>
                            <tr>
                                <td colspan="10">
                                    <ul class="pagination pull-right"></ul>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                </div>

            </div>
        </div>
    </div>


   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();
            // $('.footable2').footable();

        });

    </script>


</body>

</html>
