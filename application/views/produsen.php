<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Produsen</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Kelola Produsen</h2>
                        <p class="font-bold">Halaman ini bertujuan untuk mengolah produsen dalam aplikasi ini, produsen / vendor adalah pihak yang memproduksi produk</p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    
                    <div class="row">
                        <div class="col-lg-4 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Tambah Produsen</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="form-group">

                                        <?php echo form_open('Apps/tambahprodusen')?>
                                        
                                        <label>Nama Produsen</label>
                                        <input type="text" name="nama_produsen" class="form-control" required placeholder="ex : Fiesta" >
                                        <br>


                                        <div class="text-right">
                                            <button type="submit" class="btn btn-success text-right ">Tambah</button>    
                                        </div>

                                        <?php echo form_close(); ?>

                                    </div>

                                </div>
                            
                            </div>


                        </div>
                        <div class="col-lg-8 animated fadeInDown">
                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Daftar Produsen</h5>
                                </div>

                                <div class="ibox-content">

                                    <div class="row">
                                        <div class="col-lg-12">

                                            <input type="text" class="form-control input-sm m-b-xs" id="filter" placeholder="Search in table">

                                            <table class="footable table table-stripped" data-page-size="20" data-filter=#filter>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" data-type="numeric">ID</th>
                                                        <th>Nama Produsen</th>
                                                        
                                                        <th class="text-center">Action</th>
                                                    </tr>
                                                </thead>

                                                <tbody>

                                                    <?php for ($i=0; $i < count($produsen); $i++) { ?>
                                                        
                                                        <tr>
                                                            <td class="text-center"><?php echo $produsen[$i]->id_produsen?></td>
                                                            <td><?php echo $produsen[$i]->nama_produsen?></td>
                                                            
                                                            <td class="text-center">

                                                               

                                                                <a data-toggle="modal" data-target="#edit<?php echo $produsen[$i]->id_produsen ?>" class="btn btn-warning btn-xs"><span class="fa fa-pencil"></span></a>
                                                                <a data-toggle="modal" data-target="#hapus<?php echo $produsen[$i]->id_produsen ?>" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>

                                                                <div class="modal inmodal" id="lihat<?php echo $produsen[$i]->id_produsen ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <div class="modal-header">
                                                                                <h1><b>[<?php echo $produsen[$i]->id_produsen?>]</b> - <?php echo $produsen[$i]->nama_produsen?></h1>
                                                                            </div>


                                                                            <div class="modal-footer">
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal inmodal" id="edit<?php echo $produsen[$i]->id_produsen ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog modal-lg">
                                                                        <div class="modal-content animated bounceInUp">

                                                                            <?php echo form_open('Apps/editprodusen')?>

                                                                            <div class="modal-header">

                                                                                <h1><input type="form-control" name="nama_produsen" value="<?php echo $produsen[$i]->nama_produsen?>"></h1>

                                                                            </div>

                                                                            
                                                                            <div class="modal-footer">
                                                                                <button type="submit" class="btn btn-success">Simpan</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                                <input type="hidden" name="id_produsen" value="<?php echo $produsen[$i]->id_produsen?>">
                                                                            </div>

                                                                            <?php echo form_close() ?>

                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="modal inmodal" id="hapus<?php echo $produsen[$i]->id_produsen ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                                    <div class="modal-dialog">
                                                                        <div class="modal-content animated bounceInUp">

                                                                          

                                                                            <?php echo form_open('Apps/hapusprodusen')?>

                                                                            <div class="modal-body text-center">
                                                                                <h1>Apakah anda yakin untuk menghapus <b><?php echo $produsen[$i]->nama_produsen ?> ?</b> </h1>
                                                                                <br>
                                                                                <small>Anda tidak dapat mengembalikan produk yang telah terhapus</small>
                                                                            </div>

                                                                            <div class="modal-footer">
                                                                                <input type="hidden" name="id_produsen" value="<?php echo $produsen[$i]->id_produsen?>">
                                                                                <button type="submit" class="btn btn-danger">Hapus</button>
                                                                                <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                            </div>

                                                                            <?php echo form_close();  ?>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                    
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td colspan="5">
                                                        <ul class="pagination pull-right"></ul>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();

        });

    </script>


</body>

</html>
