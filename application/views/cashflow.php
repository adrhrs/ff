<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Cash Flow</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Cash Flow </h2>
                        <p class="font-bold">Halaman Cash Flow bertujuan untuk menampilkan alur uang masuk atau keluar</p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    
                    <div class="row">

                        <div class="col-lg-4">
                            <div class="ibox ">
                                <div class="ibox-title">

                                    <h5>Kas Masuk</h5>
                                </div>
                                <div class="ibox-content">

                                     <?php $tot_masuk = 0; $tot_keluar = 0; for($i=0; $i < count($cf); ++$i) {

                                        if($cf[$i]->tipe_transaction == 'po' ) { 

                                            $tot_keluar += $cf[$i]->bayar;
                                            $tot_masuk += $cf[$i]->kembalian;

                                        } else {

                                            $tot_keluar += $cf[$i]->kembalian;
                                            $tot_masuk += $cf[$i]->bayar;
                                             

                                        } 


                                        } ?>

                                   

                                    <h1 align="justify" ><?php echo "Rp " . number_format($tot_masuk,0,',','.'); ?></h1>
                                    <hr>
                    
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="ibox ">
                                <div class="ibox-title">

                                    <h5>Kas Keluar</h5>
                                </div>
                                <div class="ibox-content">

                                    <h1 align="justify" ><?php echo "Rp " . number_format($tot_keluar,0,',','.'); ?></h1>
                                    <hr>
                
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="ibox ">
                                <div class="ibox-title">

                                    <h5>Margin</h5>
                                </div>
                                <div class="ibox-content">

                                    <h1 align="justify" ><?php echo "Rp " . number_format($tot_masuk-$tot_keluar,0,',','.'); ?></h1>
                                    <hr>
                
                                </div>
                            </div>
                        </div>
                        

                        <div class="col-lg-12 animated fadeInDown">

                            <div id="" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Alur Kas Pemasukan</h5>
                                </div>

                                <div class="ibox-content">
                                    <input type="text" class="form-control input-sm m-b-xs" id="filter2" placeholder="Search in table">

                                    <table class="footable table table-stripped" data-page-size="5" data-filter=#filter2>
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Tanggal</th>
                                                <th class="text-center">Transaksi</th>
                                                <th class="text-center">Arah</th>
                                                <th class="text-center">Jumlah</th>
                                                <th class="text-center">Refrensi</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            
                                            <?php for($i=0; $i < count($cf); ++$i) {?>

                                                <tr>

                                                    <td class="text-center"><?php echo $cf[$i]->id_finance ?></td>
                                                    <td class="text-center"><?php echo $cf[$i]->tgl?></td>
                                                    <td class="text-center">
                                                        <?php

                                                        if($cf[$i]->tipe_transaction == 'so') {
                                                            echo 'Pembayaran Sales Order';
                                                        } else if($cf[$i]->tipe_transaction == 'kasir'){
                                                            echo 'Pembayaran Kasir';
                                                        } else if($cf[$i]->tipe_transaction == 'cicil') {
                                                            echo 'Pembayaran Cicilan Agen';
                                                        } else if($cf[$i]->tipe_transaction == 'op_in') {
                                                            echo 'Cash Opname - Masuk';
                                                        } else if($cf[$i]->tipe_transaction == 'op_out') {
                                                            echo 'Cash Opname - Keluar';
                                                        } else  {
                                                            echo 'Kembalian Purchase Order';
                                                        }

                                                        ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <span class= "fa fa-arrow-left text-info" ></span>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php if($cf[$i]->tipe_transaction == 'po' or $cf[$i]->tipe_transaction == 'op_out' ) { ?>

                                                            <?php echo "Rp " . number_format((int)$cf[$i]->kembalian,0,',','.'); ?>

                                                        <?php } else {?>

                                                            <?php echo "Rp " . number_format((int)$cf[$i]->bayar,0,',','.'); ?>

                                                        <?php } ?>


                                                        
                                                            
                                                    </td>
                                                    <td class="text-center">
                                                        <?php

                                                        if($cf[$i]->tipe_transaction == 'so') {
                                                            echo '<a href="'.base_url('Apps/nota/so/'.$cf[$i]->id_transaction).'" class="btn btn-xs btn-info">Detail</a>';
                                                        } else if($cf[$i]->tipe_transaction == 'kasir') {
                                                            echo '<a href="'.base_url('Apps/nota/kasir/'.$cf[$i]->id_transaction).'" class="btn btn-xs btn-info">Detail</a>';
                                                        } else if($cf[$i]->tipe_transaction == 'po') {
                                                            echo '<a href="'.base_url('Apps/nota/po/'.$cf[$i]->id_transaction).'" class="btn btn-xs btn-info">Detail</a>';
                                                        } else if($cf[$i]->tipe_transaction == 'cicil') {
                                                            echo '<a href="'.base_url('Apps/harga/'.$cf[$i]->id_people).'" class="btn btn-xs btn-info">Detail</a>';
                                                        } else {
                                                            echo '<a href="'.base_url('Apps/hapusCashOpname/'.$cf[$i]->id_finance).'" class="btn btn-xs btn-info">Hapus</a>';
                                                        }

                                                        ?>
                                                    </td>
                                                    
                                                </tr>

                                            <?php } ?>

                                        </tbody>

                                        <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                         

                       
                        </div>

                        <div class="col-lg-12 animated fadeInDown">

                            <div id="" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Alur Kas Pengeluaran</h5>
                                </div>

                                <div class="ibox-content">
                                    <input type="text" class="form-control input-sm m-b-xs" id="filter2" placeholder="Search in table">

                                    <table class="footable table table-stripped" data-page-size="5" data-filter=#filter2>
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Tanggal</th>
                                                <th class="text-center">Transaksi</th>
                                                <th class="text-center">Arah</th>
                                                <th class="text-center">Jumlah</th>
                                                <th class="text-center">Refrensi</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            
                                            <?php for($i=0; $i < count($cf); ++$i) {?>

                                                <tr>

                                                    <td class="text-center"><?php echo $cf[$i]->id_finance ?></td>
                                                    <td class="text-center"><?php echo $cf[$i]->tgl?></td>
                                                    <td class="text-center">
                                                        <?php

                                                        if($cf[$i]->tipe_transaction == 'so') {
                                                            echo 'Kembalian Sales Order';
                                                        } else if($cf[$i]->tipe_transaction == 'kasir'){
                                                            echo 'Kembalian Kasir';
                                                        } else if($cf[$i]->tipe_transaction == 'cicil') {
                                                            echo 'Kembalian Cicilan Agen';
                                                        } else if($cf[$i]->tipe_transaction == 'op_in') {
                                                            echo 'Cash Opname - Keluar';
                                                        } else if($cf[$i]->tipe_transaction == 'op_out') {
                                                            echo 'Cash Opname - Masuk';
                                                        } else  {
                                                            echo 'Pembayaran Purchase Order';
                                                        }

                                                        ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <span class= "fa fa-arrow-right text-warning" ></span>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php if($cf[$i]->tipe_transaction == 'po' or $cf[$i]->tipe_transaction == 'op_out' ) { ?>

                                                            <?php echo "Rp " . number_format((int)$cf[$i]->bayar,0,',','.'); ?>

                                                        <?php } else {?>

                                                            <?php echo "Rp " . number_format((int)$cf[$i]->kembalian,0,',','.'); ?>

                                                        <?php } ?>
                                                        
                                                            
                                                    </td>
                                                    <td class="text-center">
                                                        <?php

                                                        if($cf[$i]->tipe_transaction == 'so') {
                                                            echo '<a href="'.base_url('Apps/nota/so/'.$cf[$i]->id_transaction).'" class="btn btn-xs btn-info">Detail</a>';
                                                        } else if($cf[$i]->tipe_transaction == 'kasir') {
                                                            echo '<a href="'.base_url('Apps/nota/kasir/'.$cf[$i]->id_transaction).'" class="btn btn-xs btn-info">Detail</a>';
                                                        } else if($cf[$i]->tipe_transaction == 'po') {
                                                            echo '<a href="'.base_url('Apps/nota/po/'.$cf[$i]->id_transaction).'" class="btn btn-xs btn-info">Detail</a>';
                                                        } else if($cf[$i]->tipe_transaction == 'cicil') {
                                                            echo '<a href="'.base_url('Apps/harga/'.$cf[$i]->id_people).'" class="btn btn-xs btn-info">Detail</a>';
                                                        }

                                                        ?>
                                                    </td>
                                                    
                                                </tr>

                                            <?php } ?>

                                        </tbody>

                                        <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>
                         

                       
                        </div>

                        <div class="col-lg-12 animated fadeInDown">

                            <div id="" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Cash Opname</h5>
                                </div>

                                <div class="ibox-content">

                                    <?php echo form_open('Apps/cashOpname')?>

                                        <div class="row">

                                            <div class="col-lg-6">
                                                <label>Nominal Uang</label>
                                                <input type="number" name="bayar" class="form-control" required placeholder="ex : 100000" >
                                            </div>

                                            <div class="col-lg-3">
                                                <label>Arah Cash Flow</label>
                                                <select required="" name="tipe_transaction" class="form-control">
                                                    <option value="op_in">Masuk</option>
                                                    <option value="op_out">Keluar</option>
                                                </select>
                                            </div>

                                            <div class="col-lg-3">
                                                <br>
                                                <button type="submit" class="btn btn-success btn-block text-right ">Simpan</button>    
                                            </div>
                                            
                                        </div>

                                    <?php echo form_close(); ?>
                            

                                </div>
                            </div>
                        </div>
                       
                      
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

   

    <?php include('footer.php')?>

    <script>

        $(document).ready(function() {

            $('.footable').footable();
            $('.footable2').footable();

        });

    </script>


</body>

</html>
