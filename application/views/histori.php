<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Toko Hasil Laut | Report</title>

     <?php include('header.php')?>

</head>

<body class="">

    <div id="wrapper">

        <?php include('sidebar.php') ?>

        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">

            </div>
                <div class="row wrapper border-bottom white-bg page-heading animated fadeIn">
                    <div class="col-sm-12">
                        <h2>Report <?php echo $type ?></h2>
                        <p class="font-bold">Halaman ini bertujuan untuk mengolah seluruh transaksi penjualan </p>
                    </div>

                </div>

                <div class="wrapper wrapper-content">

                    <?php echo $this->session->flashdata('msg'); ?>
                    
                    <div class="row">
                        <?php if($type != 'kasir') {?>

                            <div id="ft" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <h5>Filter</h5>
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="ibox-content">
                                    <div class="row">

                                        <?php echo form_open('Apps/filterHistori')?>

                                        <div class="col-sm-3">
                                            <h5>NAMA AGEN / SUPPLIER</h5>

                                            <select required class="form-control m-b" name="id_agen">
                                                
                                                <option value="0"> --- Semuanya --- </option>
                                                <?php for($i=0; $i < count($agen); ++$i) { ?>
                                                    <option
                                                        <?php if($id_agen == $agen[$i]->id_agen ) { echo 'selected'; } ?>
                                                     value="<?php echo $agen[$i]->id_agen ?>"><?php echo $agen[$i]->nama_agen ?></option>
                                                <?php } ?>

                                                <?php for($i=0; $i < count($supplier); ++$i) { ?>
                                                    <option
                                                        <?php if($id_supplier == $supplier[$i]->id_supplier ) { echo 'selected'; } ?>
                                                     value="<?php echo $supplier[$i]->id_supplier ?>"><?php echo $supplier[$i]->nama_supplier ?>
                                                         
                                                     </option>
                                                <?php } ?>

                                            </select>
                                        </div>
                                        <div class="col-sm-3">
                                            <h5>TANGGAL MULAI</h5>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i>

                                                    <?php 

                                                        $tgl_mulai = '1970-01-01';
                                                        if($date_start != '0') {

                                                            $year = substr($date_start,4,4);
                                                            $month = substr($date_start,2,2);
                                                            $date = substr($date_start,0,2);

                                                            $tgl_mulai = $year.'-'.$month.'-'.$date;
                                                        }

                                                        $tgl_selesai = '2025-01-01';
                                                        if($date_finish != '0') {

                                                            $year = substr($date_finish,4,4);
                                                            $month = substr($date_finish,2,2);
                                                            $date = substr($date_finish,0,2);

                                                            $tgl_selesai = $year.'-'.$month.'-'.$date;
                                                        }

                                                    ?>

                                                </span><input required name="date_start" type="date" value="<?php echo $tgl_mulai; ?>" class="form-control" >
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <h5>TANGGAL SELESAI</h5>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i>
                                                </span><input required name="date_finish" type="date" value="<?php echo $tgl_selesai; ?>" class="form-control" >
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <h5>&nbsp;</h5>
                                            <input type="hidden" name="type" value="<?php echo $type; ?>">
                                            <button type="submit" class="btn btn-block btn-info">Cari</button>
                                        </div>

                                        <?php echo form_close() ?>

                                    </div>
                                </div>
                            </div>


                        <?php } ?>

                        

                        <?php if($type == 'penjualan') {?>

                            

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="ibox ">
                                        <div class="ibox-title">

                                            <h5>Pendapatan Agen</h5>
                                        </div>
                                        <div class="ibox-content">

                                            <?php $tot_agen = 0; $tot_ppn_agen = 0;  for($i=0; $i < count($dt); ++$i) { 

                                                                                                
                                            $totawal = (int)$dt[$i]->harga;
                                            $dscx = (int)$dt[$i]->discount;

                                            $dscval = ($dscx/100)*$totawal;
                                            $totx = $totawal - $dscval;

                                            $pjk = (int)$dt[$i]->pajak;
                                            $pjkval = ($pjk/100)*$totx;
                                            $grandtot = $totx + $pjkval;

                                            $tot_agen = $tot_agen + $grandtot;

                                            $tot_hpp = (100/110)*$totawal;
                                            $tot_ppn = 0.1*$tot_hpp;

                                            $tot_ppn_agen = $tot_ppn_agen + $tot_ppn;

                                            } ?>

                                            <h1 align="justify" ><?php echo "Rp " . number_format($tot_agen,0,',','.'); ?> </h1>
                                            <hr>
                                            <div class="stat-percent font-bold text-info"><?php echo count($dt); ?> <i class="fa fa-shopping-cart"></i></div>

                                            <small>Total Transaksi</small>

                                             
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-6">
                                    <div class="ibox ">
                                        <div class="ibox-title">

                                            <h5>Total PPN</h5>
                                        </div>
                                        <div class="ibox-content">



                                            <h1 align="justify" ><?php echo "Rp " . number_format($tot_ppn_agen,0,',','.'); ?> </h1>
                                            <hr>
                                            <div class="stat-percent font-bold text-info"><?php echo count($dt); ?> <i class="fa fa-shopping-cart"></i></div>

                                            <small>Total Transaksi</small>

                                            
                                        </div>
                                    </div>
                                </div>

                        
                            

                            </div>                            

                        

                            <div id="agenx" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <h5>Transaksi Penjualan Agen </h5>
                                        </div>
                                        <div class="col-lg-4 ">
                                            <h5 class="pull-right"><?php echo $tot; ?> Transaksi ditemukan</h5>
                                        </div>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                  
                                    <table class="table table-hover table-stripped" >
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th>Agent</th>
                                                <th class="text-center">Tanggal</th>
                                                <th class="text-center">Total Harga</th>
                                                <th class="text-center">Pembayaran</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                            
                                            <?php for($i=0; $i < count($so); ++$i) { ?>

                                                <?php 

                                                $totawal = (int)$so[$i]->harga;
                                                $dscx = (int)$so[$i]->discount;

                                                $dscval = ($dscx/100)*$totawal;
                                                $totx = $totawal - $dscval;

                                                $pjk = (int)$so[$i]->pajak;
                                                $pjkval = ($pjk/100)*$totx;
                                                $grandtot = $totx + $pjkval;

                                                ?>

                                                <tr>
                                                    <td class="text-center"><?php echo $so[$i]->id_sales ?></td>
                                                    <td ><a href="<?php echo base_url('Apps/harga/'.$so[$i]->id_agen)?>"><?php echo $so[$i]->nama_agen?></a></td>
                                                    <td class="text-center"><?php echo $so[$i]->tgl ?></td>
                                                    <td class="text-center">
                                                    
                                                        <?php 

                                                            echo "Rp " . number_format($grandtot,0,',','.'); 

                                                        ?>
                                                            
                                                    </td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$so[$i]->bayar,0,',','.'); ?></td>

                                            
                                                    <td class="text-center">
                                                        <a class="btn btn-xs btn-primary" href="<?php echo base_url('Apps/nota/so/'.$so[$i]->id_sales) ?>"><span class="fa fa-eye"></span></a>
                                                        <a class="btn btn-xs btn-warning" href="<?php echo base_url('Apps/tambahSO/edit/'.$so[$i]->id_sales) ?>"><span class="fa fa-pencil"></span></a>
                                                        <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#hapus<?php echo $so[$i]->id_sales ?>" ><span class="fa fa-trash"></span></a>

                                                        <div class="modal inmodal" id="hapus<?php echo $so[$i]->id_sales ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content animated bounceInUp">

                                                                    <?php echo form_open('Apps/hapusNota')?>

                                                                    <div class="modal-body text-center">
                                                                        <h1>Apakah anda yakin untuk menghapus transaksi <b><?php echo $so[$i]->id_sales ?> ?</b> </h1>
                                                                        <br>
                                                                        <small>Anda tidak dapat mengembalikan transaksi yang telah terhapus</small>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="id_sales" value="<?php echo $so[$i]->id_sales ?>">
                                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close() ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>

                                    </table>
                                    <div class="btn-group text-right">

                                        <?php for($i=1; $i <= $tot_page; ++$i) { ?>

                                            <?php if($i == $page) { ?>

                                                <a href="<?php echo base_url('Apps/histori/'.$type.'/'.$id_agen.'/'.$date_start.'/'.$date_finish.'/'.$i)?>" class="btn btn-white active"><?php echo $i; ?></a>    

                                            <?php  } else { ?>

                                                <a href="<?php echo base_url('Apps/histori/'.$type.'/'.$id_agen.'/'.$date_start.'/'.$date_finish.'/'.$i)?>" class="btn btn-white"><?php echo $i; ?></a>
                                        <?php } }?>
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>

                        <?php } ?>

                        <?php if($type == 'kasir') {?>

                            <div id="kasirx" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>Transaksi Kasir</h5>
                                </div>

                                <div class="ibox-content">

                                    <table class="table table-hover table-stripped">
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th class="text-center">Tanggal</th>
                                                <th class="text-center">Total Produk</th>
                                                <th class="text-center">Discount</th>
                                                <th class="text-center">Total Harga</th>
                                                <th class="text-center">Pembayaran</th>
                                                <th class="text-center">Kembalian</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            
                                            <?php for($i=0; $i < count($kasir); ++$i) {?>

                                                <tr>
                                                    <?php $totx = ((100-(int)$kasir[$i]->discount)/100)*(int)$kasir[$i]->harga; ?>

                                                    <td class="text-center"><?php echo $kasir[$i]->id_sales ?></td>
                                                    <td class="text-center"><?php echo $kasir[$i]->tgl?></td>
                                                    <td class="text-center"><?php echo $kasir[$i]->qty?> Unit</td>
                                                    <td class="text-center"><?php echo $kasir[$i]->discount?> %</td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$totx,0,',','.'); ?></td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$kasir[$i]->bayar,0,',','.'); ?></td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$kasir[$i]->bayar-($totx),0,',','.'); ?></td>
                                                    <td class="text-center">
                                                        <a class="btn btn-xs btn-primary" href="<?php echo base_url('Apps/nota/kasir/'.$kasir[$i]->id_sales) ?>"><span class="fa fa-eye"></span></a>
                                                        <a class="btn btn-xs btn-warning" href="<?php echo base_url('Apps/kasir/ops/'.$kasir[$i]->id_sales) ?>"><span class="fa fa-pencil"></span></a>
                                                        <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#hapus<?php echo $kasir[$i]->id_sales ?>" ><span class="fa fa-trash"></span></a>

                                                        <div class="modal inmodal" id="hapus<?php echo $kasir[$i]->id_sales ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content animated bounceInUp">

                                                                    <?php echo form_open('Apps/hapusNota')?>

                                                                    <div class="modal-body text-center">
                                                                        <h1>Apakah anda yakin untuk menghapus transaksi <b><?php echo $kasir[$i]->id_sales ?> ?</b> </h1>
                                                                        <br>
                                                                        <small>Anda tidak dapat mengembalikan transaksi yang telah terhapus</small>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="id_sales" value="<?php echo $kasir[$i]->id_sales ?>">
                                                                        <input type="hidden" name="id_finance" value="<?php echo $kasir[$i]->id_finance ?>">
                                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close() ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>

                                        <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>

                        <?php } ?>

                        <?php if($type == 'pembelian') {?>

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="ibox ">
                                        <div class="ibox-title">

                                            <h5>Pengeluaran Keseluruhan</h5>
                                        </div>
                                        <div class="ibox-content">

                                             <?php $tot_po = 0; $jml_lunas = 0; $jml_blm = 0; $tot_lunas = 0; $tot_blm = 0; for($i=0; $i < count($po); ++$i) {

                                                    $totawal = (int)$po[$i]->harga;
                                                    $dscx = (int)$po[$i]->discount;

                                                    $dscval = ($dscx/100)*$totawal;
                                                    $totx = $totawal - $dscval;

                                                    $pjk = (int)$po[$i]->pajak;
                                                    $pjkval = ($pjk/100)*$totx;
                                                    $grandtot = $totx + $pjkval;

                                                    $tot_po += $grandtot;

                                                    if((int)$po[$i]->bayar <= $grandtot-1) { 

                                                        $jml_blm += 1;
                                                        $tot_blm += $grandtot;

                                                    } else {

                                                        

                                                        $jml_lunas += 1;
                                                        $tot_lunas += $grandtot;

                                                    }

                                                } ?>

                                           

                                            <h1 align="justify" ><?php echo "Rp " . number_format($tot_po,0,',','.'); ?></h1>
                                            <hr>
                                            <div class="stat-percent font-bold text-warning"><?php echo count($po); ?> <i class="fa fa-shopping-cart"></i></div>

                                            <small>Total Transaksi</small>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="ibox ">
                                        <div class="ibox-title">

                                            <h5>Pembelian Lunas</h5>
                                        </div>
                                        <div class="ibox-content">

                                            

                                            <h1 align="justify" ><?php echo $jml_lunas ?> PO</h1>
                                            <hr>
                                            <div class="stat-percent font-bold text-info"><?php echo "Rp " . number_format($tot_lunas,0,',','.'); ?> </div>

                                            <small>Pengeluaran Lunas</small>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4">
                                    <div class="ibox ">
                                        <div class="ibox-title">

                                            <h5>Pembelian Belum Lunas</h5>
                                        </div>
                                        <div class="ibox-content">

                                            

                                            <h1 align="justify" ><?php echo $jml_blm ?> PO</h1>
                                            <hr>
                                            <div class="stat-percent font-bold text-danger"><?php echo "Rp " . number_format($tot_blm,0,',','.'); ?> </div>

                                            <small>Pengeluaran Belum Lunas</small>
                                        </div>
                                    </div>
                                </div>
                            

                            </div>

                            <div id="supplierx" class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <h5>Transaksi Pembelian Supplier</h5>
                                        </div>
                                        <div class="col-lg-4 text-right">
                                            <button data-toggle="modal" data-target="#supplier" class="btn btn-xs btn-info" ><b>+ TAMBAH TRANSAKSI</b></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="ibox-content">
                                    

                                    <table class="table table-hover table-stripped" data-page-size="5" data-filter=#filter2>
                                        <thead>
                                            <tr>
                                                <th class="text-center">ID</th>
                                                <th>Supplier</th>
                                                <th class="text-center">Tanggal Pembelian</th>
                                                <th class="text-center">Tanggal Deadline</th>
                                                <th class="text-center">Total Harga</th>
                                                <th class="text-center">Pembayaran</th>
                                                <th class="text-center">Status</th>
                                                <th class="text-center">Action</th>
                                            </tr>
                                        </thead>


                                        <tbody>
                                            <?php for($i=0; $i < count($po); ++$i) { ?>

                                                <tr>
                                                    <td class="text-center"><?php echo $po[$i]->id_purchase ?></td>
                                                    <td ><?php echo $po[$i]->nama_supplier?></td>
                                                    <td class="text-center"><?php echo $po[$i]->tgl ?></td>
                                                    <td class="text-center"><?php echo $po[$i]->deadline ?></td>
                                                    <td class="text-center">

                                                        <?php 

                                                        $totawal = (int)$po[$i]->harga;
                                                        $dscx = (int)$po[$i]->discount;

                                                        $dscval = ($dscx/100)*$totawal;
                                                        $totx = $totawal - $dscval;

                                                        $pjk = (int)$po[$i]->pajak;
                                                        $pjkval = ($pjk/100)*$totx;
                                                        $grandtot = $totx + $pjkval;

                                                        ?>
                                                    
                                                        <?php 

                                                            

                                                            echo "Rp " . number_format($grandtot,0,',','.'); 

                                                        ?>
                                                            
                                                    </td>
                                                    <td class="text-center"><?php echo "Rp " . number_format((int)$po[$i]->bayar,0,',','.'); ?></td>

                                                    <td class="text-center">
                                                        <?php if((int)$po[$i]->bayar <= $grandtot-1) { ?>


                                                            <label class="label label-success"><?php echo $po[$i]->sisa?> Hari lagi</label>
                                                            <label class="label label-default">Belum Lunas</label>

                                                        <?php } else { ?>

                                                            <label class="label label-default">Lunas</label>

                                                        <?php }?>
                                                    </td>
                                            
                                                    <td class="text-center">
                                                        <a class="btn btn-xs btn-primary" href="<?php echo base_url('Apps/nota/po/'.$po[$i]->id_purchase) ?>"><span class="fa fa-eye"></span></a>
                                                        <a class="btn btn-xs btn-warning" href="<?php echo base_url('Apps/tambahPO/ops/'.$po[$i]->id_purchase) ?>"><span class="fa fa-pencil"></span></a>
                                                        <a class="btn btn-xs btn-danger" data-toggle="modal" data-target="#hapus<?php echo $po[$i]->id_purchase ?>" ><span class="fa fa-trash"></span></a>

                                                        <div class="modal inmodal" id="hapus<?php echo $po[$i]->id_purchase ?>" tabindex="-1" role="dialog" aria-hidden="true">

                                                            <div class="modal-dialog">
                                                                <div class="modal-content animated bounceInUp">

                                                                    <?php echo form_open('Apps/hapusNota')?>

                                                                    <div class="modal-body text-center">
                                                                        <h1>Apakah anda yakin untuk menghapus transaksi <b><?php echo $po[$i]->id_purchase ?> ?</b> </h1>
                                                                        <br>
                                                                        <small>Anda tidak dapat mengembalikan transaksi yang telah terhapus</small>
                                                                    </div>

                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="id_purchase" value="<?php echo $po[$i]->id_purchase ?>">
                                                                        <input type="hidden" name="tipe" value="po">
                                                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                                                        <button type="button" class="btn btn-white" data-dismiss="modal">Tutup</button>
                                                                    </div>

                                                                    <?php echo form_close() ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            <?php } ?>
                                  
                                        </tbody>



                                        <tfoot>
                                        <tr>
                                            <td colspan="10">
                                                <ul class="pagination pull-right"></ul>
                                            </td>
                                        </tr>
                                        </tfoot>

                                    </table>
                                </div>
                            </div>

                       

                        <?php } ?>




                        
                    </div>

                </div>

                <?php include('copyright.php')?>

        </div>
    </div>

    
   

    <?php include('footer.php')?>

   <script>
   $(document).ready(function(){

    var mem = $('#data_1 .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

   })
   </script>

</body>

</html>
