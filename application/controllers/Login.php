<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        $this->load->helper('text');
        $this->load->helper('form');
        $this->load->model('mdl');
        $this->load->library('image_lib');
        $this->load->library('form_validation');


        if ((isset($this->session->userdata['logged_in']))) {

            redirect('Apps');

        }
        
    }
    
    public function index() {

        $this->load->view('login');
    }

    public function proseslogin() {

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        
        $status = $this->mdl->checkAccount($username,$password);
        
        if ($status != FALSE) {

            $session_data = array(
                
                'username' => $status[0]->username,
                'nama' => $status[0]->nama,
                'password' => $status[0]->password,

            );

            $this->session->set_userdata('logged_in', $session_data);
            redirect('Apps');

        } else {
            $data['err'] = 'ada';
            $this->load->view('login', $data);
        }
    }


    
    
}
