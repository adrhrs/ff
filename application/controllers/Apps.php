<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apps extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        $this->load->helper('text');
        $this->load->helper('form');
        $this->load->model('mdl');
        $this->load->library('image_lib');
        $this->load->library('form_validation');

        date_default_timezone_set("Asia/Jakarta");
        
        
        if (!(isset($this->session->userdata['logged_in']))) {
            
            redirect('');
            
        }
        
    }

    public function log() {  

        $time_start = microtime(true); 

        $loadtime = $this->input->post("loadtime");
        $url = $this->input->post("url");
        $err_str = $this->input->post("err_str");

        if($err_str == "") {
            $status = 'SUCCESS';
        } else {
            $status = 'CHECK HOME';
        }

        $data = array(
            'loadtime' => $loadtime,
            'param' => $url,
            'error_msg' => $err_str,
            'status' => $status,
        );

        // if($status == 'CHECK HOME') {
        //     $this->mdl->insertData('log', $data);    
        // }
        
        

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
       
        header('Content-Type: application/json');
        echo json_encode( array($data, $execution_time));
    }
    
    public function logout() {
        
        $sess_array = array(
            'email' => ''
        );
        $this->session->unset_userdata('logged_in', $sess_array);
        redirect('');
    }
    
    public function index() {
        
        redirect('Apps/penjualan');
        
    }

    
    public function produk() {
        
        $data['produsen'] = $this->mdl->getProdusenList();
        $data['produk']   = $this->mdl->getProduk();

        // print_r($data['produk']); exit();
        
        $this->load->view('produk', $data);
    }

    
    
    public function tambahproduk() {
        
        $data = array(
            
            'nama' => $this->input->post('nama'),
            'id_produsen' => $this->input->post('id_produsen'),
            'berat' => $this->input->post('berat'),
            'isi' => $this->input->post('isi'),
            'harga_beli' => $this->input->post('harga_beli'),
            'harga_normal' => $this->input->post('harga_normal'),
            'harga_agen' => $this->input->post('harga_agen'),
            'harga_khusus' => $this->input->post('harga_khusus'),
            'keterangan' => $this->input->post('keterangan'),
            'tipe' => 'produk'
            
        );
        
        $this->mdl->insertData('produk', $data);
        
        $pesan = "<b>Berhasil Menambahkan Produk</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/produk');
        
    }
    
    public function editproduk() {
        
        $data = array(
            
            'nama' => $this->input->post('nama'),
            'id_produsen' => $this->input->post('id_produsen'),
            'berat' => $this->input->post('berat'),
            'isi' => $this->input->post('isi'),
            'harga_beli' => $this->input->post('harga_beli'),
            'harga_normal' => $this->input->post('harga_normal'),
            'harga_agen' => $this->input->post('harga_agen'),
            'harga_khusus' => $this->input->post('harga_khusus'),
            'keterangan' => $this->input->post('keterangan')
            
        );
        
        $id_produk = $this->input->post('id_produk');
        
        $this->mdl->updateData('id_produk', $id_produk, 'produk', $data);
        
        $pesan = "<b>Berhasil Mengedit Produk</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/produk');
        
    }
    
    public function hapusproduk() {
        
        $id_produk = $this->input->post('id_produk');
        $produk = $this->mdl->getProdukByID($id_produk);

        $stat = $this->mdl->cekHapusProduk($id_produk);

        $data = array(

            'nama' => $produk[0]->nama." ( Product Deleted )",

        );

        $this->mdl->updateData('id_produk', $id_produk, 'produk', $data);

        $pesan = "<b>Berhasil Menghapus Produk</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/produk');

        
    }
    
    public function produsen() {
        
        
        $data['produsen'] = $this->mdl->getProdusen();
        
        $this->load->view('produsen', $data);
    }

    
    
    public function tambahprodusen() {
        
        $data = array(
            
            'nama_produsen' => $this->input->post('nama_produsen')
        );
        
        $id_produsen = $this->input->post('id_produsen');
        
        $this->mdl->insertData('produsen', $data);
        
        $pesan = "<b>Berhasil Menambahkan Produsen</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/produsen');
        
    }
    
    public function editprodusen() {
        
        $data = array(
            
            'nama_produsen' => $this->input->post('nama_produsen')
        );
        
        $id_produsen = $this->input->post('id_produsen');
        
        $this->mdl->updateData('id_produsen', $id_produsen, 'produsen', $data);
        
        $pesan = "<b>Berhasil Mengedit Produsen</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/produsen');
        
    }
    
    public function hapusprodusen() {
        
        $id_produsen = $this->input->post('id_produsen');
        
        $produsen = $this->mdl->getProdusenByID($id_produsen);
        $data = array(

            'nama_produsen' => $produsen[0]->nama_produsen." ( Produsen Deleted )",

        );

        $this->mdl->updateData('id_produsen', $id_produsen, 'produsen', $data);
        
        $pesan = "<b>Berhasil Menghapus Produsen</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/produsen');
        
    }

    public function agen() {
        
        $data['agen'] = $this->mdl->getAgen();
        
        $this->load->view('agen', $data);
    }

    public function tambahagen() {
        
        $data = array(
            
            'nama_agen' => $this->input->post('nama_agen'),
            'kategori' => $this->input->post('kategori'),
            'nomor' => $this->input->post('nomor'),
            'alamat' => $this->input->post('alamat'),
            'npwp' => $this->input->post('npwp'),
        );
        
        
        $this->mdl->insertData('agen', $data);
        
        $pesan = "<b>Berhasil Menambahkan Agen</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/agen');
        
    }

    public function editagen() {
        
        $data = array(
            
            'nama_agen' => $this->input->post('nama_agen'),
            'kategori' => $this->input->post('kategori'),
            'nomor' => $this->input->post('nomor'),
            'alamat' => $this->input->post('alamat'),
            'npwp' => $this->input->post('npwp'),   
        );
        
        $id_agen = $this->input->post('id_agen');
        
        $this->mdl->updateData('id_agen', $id_agen, 'agen', $data);
        
        $pesan = "<b>Berhasil Mengedit Agen</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/agen');
        
    }

    public function hapusagen() {
        
        $id_agen = $this->input->post('id_agen');
        $agen = $this->mdl->getAgenByID($id_agen);
        
        $data = array(
            
            'nama_agen' => $agen[0]->nama_agen." ( Agen Deleted )",

        );
        
        $this->mdl->updateData('id_agen', $id_agen, 'agen', $data);

        $pesan = "<b>Berhasil Menghapus Agen</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/agen');
        
    }

    public function supplier() {
        
        $data['supplier'] = $this->mdl->getSupplier();
        $data['produk'] =$this->mdl->getSupplierProduct();
        $data['setting'] = $this->mdl->getSetting();
        
        $this->load->view('supplier', $data);
    }

    public function updatelistpajak() {
        
        $data = array(
            'value_str' => $this->input->post('value_str_sup'),
        );

        $this->mdl->updateData('id_setting', 3, 'setting', $data);

        $data = array(
            'value_str' => $this->input->post('value_str_prod'),
        );

        $this->mdl->updateData('id_setting', 4, 'setting', $data);
        
        $pesan = "<b>Berhasil Mengedit List Pajak</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/supplier');
        
        // $this->load->view('produsen', $data);
    }

    public function tambahsupplier() {
        
        $data = array(
            
            'nama_supplier' => $this->input->post('nama_supplier'),
            'nomor' => $this->input->post('nomor'),
            'alamat' => $this->input->post('alamat'),
            'npwp' => $this->input->post('npwp'),
        );
        
        
        $this->mdl->insertData('supplier', $data);
        
        $pesan = "<b>Berhasil Menambahkan Supplier</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/supplier');
        
    }

    public function editsupplier() {
        
        $data = array(
            
            'nama_supplier' => $this->input->post('nama_supplier'),
            'nomor' => $this->input->post('nomor'),
            'alamat' => $this->input->post('alamat'),
            'npwp' => $this->input->post('npwp'),   
        );
        
        $id_supplier = $this->input->post('id_supplier');
        
        $this->mdl->updateData('id_supplier', $id_supplier, 'supplier', $data);
        
        $pesan = "<b>Berhasil Mengedit Supplier</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/supplier');
        
    }

    public function hapussupplier() {
        
        $id_supplier = $this->input->post('id_supplier');
        
        $supplier = $this->mdl->getSupplierByID($id_supplier);
        
        $data = array(
            
            'nama_supplier' => $supplier[0]->nama_supplier." ( Supplier Deleted )",

        );
        
        $this->mdl->updateData('id_supplier', $id_supplier, 'supplier', $data);
        
        $pesan = "<b>Berhasil Menghapus Supplier</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/supplier');
        
    }

    public function akun() {
        
        $data['akun'] = $this->mdl->getAkun();
        
        $this->load->view('akun', $data);
    }

    public function editakun() {
        
        $data = array(
            
            'nama' => $this->input->post('nama'),
            'username' => $this->input->post('username'),
            'password' => $this->input->post('password'),

        );
        
        
        
        $this->mdl->updateData('id_user', '1', 'user', $data);
        
        $pesan = "<b>Berhasil Mengedit Akun, Login ulang untuk menggunakan pengaturan baru</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/akun');
        
    }

    public function harga($id_agen) {

        $data['agen'] = $this->mdl->getAgenByID($id_agen);
        $data['harga'] = $this->mdl->getHargaAgenByID($id_agen);
        $data['produk']   = $this->mdl->getProduk();
        $data['so'] = $this->mdl->getPenjualanAgenByID($id_agen);
        $data['ci'] = $this->mdl->getCicilanAgenByID($id_agen);

        $data['id'] = $id_agen;
        
        $this->load->view('harga', $data);
    }

    public function setHarga() {

        $id_agen = $this->input->post('id_agen');
        $id_produk = $this->input->post('id_produk');

        $status = $this->mdl->cekharga($id_agen,$id_produk);

        if($status == TRUE) {

            $data = array(
                
                'id_agen' => $id_agen,
                'id_produk' => $id_produk,
                'harga' => $this->input->post('harga'),

            );

            $this->mdl->insertData('harga_agen',$data);

            $pesan = "<b>Berhasil Menambahkan Harga Baru</b>";
            $warna = "success";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            
            redirect('Apps/harga/'.$this->input->post('id_agen'));

        } else {

            $pesan = "<b>Gagal Menambahkan Harga Baru, Harga Telah Terdaftar</b>";
            $warna = "danger";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            
            redirect('Apps/harga/'.$this->input->post('id_agen'));

        }

        

    }

    public function editHarga() {

        $data = array(

            'harga' => $this->input->post('harga'),

        );

        $id_harga = $this->input->post('id_harga');

        $this->mdl->updateData('id_harga',$id_harga,'harga_agen',$data);

        $pesan = "<b>Berhasil Mengedit Harga </b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/harga/'.$this->input->post('id_agen'));

    }

    public function hapusharga() {
        
        $id_harga = $this->input->post('id_harga');
        
        $this->mdl->deleteData('id_harga', $id_harga, 'harga_agen');
        
        $pesan = "<b>Berhasil Menghapus Harga</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/harga/'.$this->input->post('id_agen'));
        
    }

    public function redirectkasir() {

        $stat = $this->mdl->getLastSalesStatus('kasir');

        if($stat == 'close') {

            $data = array(

                'status' => 'open',
                'tipe' => 'kasir'

            );

            $id = $this->mdl->insertDataGetLast('sales_order',$data);

            $data = array(

                'id_transaction' => $id,
                'tipe_transaction' => 'kasir'

            );

            $idf = $this->mdl->insertDataGetLast('finance',$data);

            $data = array(

                'id_finance' => $idf,

            );

            $this->mdl->updateData('id_sales',$id,'sales_order',$data);

            redirect('Apps/kasir/new/'.$id);  

        } else {

            $id = $this->mdl->getLastSalesID('kasir');
            redirect('Apps/kasir/old/'.$id);  

        }

              
    }

    public function kasir($tipe,$id) {
        
        $data['id'] = $id;
        $data['tipe'] = $tipe;

        // $data['produk']   = $this->mdl->getProduk();
        $data['sc'] = $this->mdl->getShoppingCart($id);
        $data['tot'] = $this->mdl->getKasirTotal($id);



        $this->load->view('kasir',$data);
    }

    public function cariProduk() {

        $id_sales = $this->input->post('id_sales');
        $id_po = $this->input->post('id_po');
        $tipe = $this->input->post('tipe');
        $param = $this->input->post('param');

        $res = $this->mdl->findProduk($param);


        $this->session->set_flashdata('res', $res);
        

        if($tipe == 'kasir') {
            redirect('Apps/kasir/ops/'.$id_sales.'');    
        } else if ($tipe == 'produk') {
            redirect('Apps/produk');    
        } else if ($tipe == 'sales') {
            redirect('Apps/tambahSO/ops/'.$id_sales.'');  
        } else if ($tipe == 'stock') {
            $res2 = $this->mdl->getStockProduct($param);
            $this->session->set_flashdata('res2', $res2);
            redirect('Apps/stock');   
        } else if ($tipe == 'po') {
            redirect('Apps/tambahPO/ops/'.$id_po.'');    
        } else if ($tipe == 'opname') {
            redirect('Apps/pergerakan');    
        } else if ($tipe == 'agen') {
            redirect('Apps/harga/'.$id_sales.'');  
        }

        
    }

    public function operasiKasir($ops, $id_sales, $id2){

        

        if($ops == 'tambah') {
            
            $produk = $this->mdl->getProdukByID($id2);
            $data = array(

                'id_sales' => $id_sales,
                'id_produk' => $id2,
                'harga' => $produk[0]->harga_normal,

            );

            $id_line = $this->mdl->insertDataGetLast('line_sales',$data);

            $data = array(

                'id_line' => $id_line,
                'id_produk' => $id2,
                'qty' => 1,
                'direction' => 'out',
                'tipe_inventory' => 'kasir'

            );

            $id_inventory = $this->mdl->insertDataGetLast('inventory',$data);

            $data = array(

                'id_inventory' => $id_inventory

            );

            $this->mdl->updateData('id_line',$id_line,'line_sales',$data);

            $res = $this->mdl->getStockProduct($id2);
            $set = $this->mdl->getSetting();
            $low = (int)$set[0]->value;
            $med = (int)$set[1]->value;
            $stock = (int)$res[0]->stockNow;

            if($stock <= $low) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
                $warna = "danger";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/kasir/ops/'.$id_sales.'');

            } else if($stock <= $med) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
                $warna = "warning";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/kasir/ops/'.$id_sales.'');

            } else {
                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
                $warna = "success";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/kasir/ops/'.$id_sales.'#sc');
            }

        } else if($ops == 'inc') {
            $line = $this->mdl->getLineByID($id2);
            $jml = $line[0]->qty;
            $id_inventory = $line[0]->id_inventory;
            
            $data = array(

                'qty' => $jml+1

            );

            $this->mdl->updateData('id_inventory',$id_inventory,'inventory',$data);

            $id_produk = $line[0]->id_produk;
            $res = $this->mdl->getStockProduct($id_produk);
            $set = $this->mdl->getSetting();
            $low = (int)$set[0]->value;
            $med = (int)$set[1]->value;
            $stock = (int)$res[0]->stockNow;

            if($stock <= $low) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
                $warna = "danger";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/kasir/ops/'.$id_sales.'');

            } else if($stock <= $med) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
                $warna = "warning";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/kasir/ops/'.$id_sales.'');

            } else {
                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
                $warna = "success";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/kasir/ops/'.$id_sales.'#sc');
            }

        } else if($ops == 'dec') {
            $line = $this->mdl->getLineByID($id2);
            $jml = $line[0]->qty;
            $id_inventory = $line[0]->id_inventory;
            $data = array(

                'qty' => $jml-1

            );

            $this->mdl->updateData('id_inventory',$id_inventory,'inventory',$data);
            
            $id_produk = $line[0]->id_produk;
            $res = $this->mdl->getStockProduct($id_produk);
            $set = $this->mdl->getSetting();
            $low = (int)$set[0]->value;
            $med = (int)$set[1]->value;
            $stock = (int)$res[0]->stockNow;

            if($stock <= $low) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
                $warna = "danger";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/kasir/ops/'.$id_sales.'');

            } else if($stock <= $med) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
                $warna = "warning";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/kasir/ops/'.$id_sales.'');

            } else {
                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
                $warna = "success";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/kasir/ops/'.$id_sales.'#sc');
            }

        } else if($ops == 'delete') {

            $line = $this->mdl->getLineByID($id2);
            $id_inventory = $line[0]->id_inventory;

            $this->mdl->deleteData('id_line',$id2,'line_sales');
            $this->mdl->deleteData('id_inventory',$id_inventory,'inventory');

            $pesan = "<b>Berhasil Menghapus Produk</b>";
            $warna = "success";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/kasir/ops/'.$id_sales);
        }

    }

    public function simpanKasir() {
        
        $bayar = $this->input->post('pages');
        $total = $this->input->post('total');
        $id_sales = $this->input->post('id_sales');
        $id_finance = $this->input->post('id_finance');


        $kembalian = 0;
        $tipe = 'kasir';

        if($this->input->post('kembalian') !== NULL ) {

            $kembalian = $this->input->post('kembalian');
            $tipe = 'so';

        }

        $data = array(

            'status' => 'close',

        );

        $this->mdl->updateData('id_sales',$id_sales,'sales_order',$data);

        if($tipe == 'kasir') {

            $fin = $this->mdl->getPenjualanKasirByID($id_sales);
            $kembalian = $bayar - (int)$fin[0]->harga2;
            
        }

    
        $data = array(

            'bayar' => $bayar,
            'kembalian' => $kembalian

        );



        $this->mdl->updateData('id_finance',$id_finance,'finance',$data);

        

        redirect('Apps/nota/'.$tipe.'/'.$id_sales);
    }

    public function setDiscount() {

        $id_sales = $this->input->post('id_sales');
        $id_finance = $this->input->post('id_finance');
        $discount = $this->input->post('discount');
        $keterangan = $this->input->post('keterangan');

        $tipe = $this->input->post('tipe');

        $data = array(

            'ket_kasir' => $keterangan

        );

        $this->mdl->updateData('id_sales',$id_sales,'sales_order',$data);

        $data = array(

            'discount' => $discount

        );

        $this->mdl->updateData('id_finance',$id_finance,'finance',$data);

        $pesan = "<b>Berhasil Menambahkan Discount</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');

        if($tipe == 'so') {

            redirect('Apps/tambahSO/ops/'.$id_sales);

        } else {

            redirect('Apps/kasir/ops/'.$id_sales);    

        }
    }

    public function setPajak() {

        $id_sales = $this->input->post('id_sales');
        $id_finance = $this->input->post('id_finance');
        $pajak = $this->input->post('pajak');
        $tipe = $this->input->post('tipe');

        $data = array(

            'pajak' => $pajak,

        );

        $this->mdl->updateData('id_finance',$id_finance,'finance',$data);

        $pesan = "<b>Berhasil Menambahkan Pajak</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');

        if($tipe == 'so') {

            redirect('Apps/tambahSO/ops/'.$id_sales);

        } else {

            redirect('Apps/kasir/ops/'.$id_sales);    

        }
    }

    public function penjualan() {

        $time_start = microtime(true); 

        $monday = date( 'Y-m-d', strtotime( 'first day of this month' ) );
        $sunday = date( 'Y-m-d', strtotime( 'first day of next month' ) );
        
        $data['kasir'] = $this->mdl->getPenjualanKasir(99999, $monday, $sunday);
        $data['so'] = $this->mdl->getPenjualanAgen(99999, $monday, $sunday);
        $data['ci'] = $this->mdl->getCicilan(99999);

        $data['so2'] = $this->mdl->getPenjualanAgen(50, $monday, $sunday);

        $data['agen'] = $this->mdl->getAgen();

        $data['dur'] = $monday." - ".$sunday;

        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start)*1000;

        $data['backend_time'] = $execution_time;

        $this->load->view('penjualan',$data);

    }

    public function pembelian() {

        $monday = date( 'Y-m-d', strtotime( 'first day of this month' ) );
        $sunday = date( 'Y-m-d', strtotime( 'first day of next month' ) );
        
        $data['supplier'] = $this->mdl->getSupplier();
        $data['po'] = $this->mdl->getPembelianSupplier(50, $monday, $sunday);

        $this->load->view('pembelian',$data);

    }

    function returnDate($date) {
        $year = substr($date,0,4);
        $month = substr($date,5,2);
        $date = substr($date,8,2);
    
        return $date.$month.$year;
      }

    public function filterHistori() {

        $type = $this->input->post('type');
        $id = $this->input->post('id_agen');
        $date_finish = $this->returnDate($this->input->post('date_finish'));
        $date_start = $this->returnDate($this->input->post('date_start'));

        redirect('Apps/histori/'.$type.'/'.$id.'/'.$date_start.'/'.$date_finish.'/1');

        

    }

    public function histori($type, $id, $date_start, $date_finish, $page) {

        if($type == 'penjualan') {

            $data['so'] = $this->mdl->getPenjualanAgenFiltered($id,$date_start, $date_finish,$page,10);

            $dt = $this->mdl->getPenjualanAgenFiltered($id,$date_start, $date_finish,1,9999999);
            $data['dt'] = $dt;
            $data['tot'] = count($dt); 

            $data['tot_page'] = ceil(count($dt)/10);
            $data['agen'] = $this->mdl->getAgen();        
            $data['type'] = $type;
            $data['id_agen'] = $id;
            $data['date_start'] = $date_start;
            $data['date_finish'] = $date_finish;
            $data['page'] = $page;

        } else if($type == 'pembelian') {

            $data['po'] = $this->mdl->getPembelianFiltered($id,$date_start, $date_finish,$page,10);

            $dt = $this->mdl->getPembelianFiltered($id,$date_start, $date_finish,1,9999999);
            $data['dt'] = $dt;
            $data['tot'] = count($dt); 

            $data['tot_page'] = ceil(count($dt)/10);
            $data['supplier'] = $this->mdl->getSupplier();        
            $data['type'] = $type;
            $data['id_supplier'] = $id;
            $data['date_start'] = $date_start;
            $data['date_finish'] = $date_finish;
            $data['page'] = $page;

        } else {

            $data['kasir'] = $this->mdl->getPenjualanKasir(99999,'1970-01-01','2050-01-01');

        }

        $data['type'] = $type;

        $this->load->view('histori',$data);


    }

     public function nota($tipe,$id_sales) {

        


        $data['statx'] = 'kasir';

        if($tipe == 'po') {

            $data['sc'] = $this->mdl->getShoppingCartPurchase($id_sales);
            $data['tot'] = $this->mdl->getKasirTotalPurchase($id_sales);
            $data['sup'] = $this->mdl->getSupplierByID($data['sc'][0]->id_people);

        } else {

            $sales = $this->mdl->getSalesByID($id_sales);
            $id_agen = $sales[0]->id_agen;


            if($id_agen > 0) {
                $data['ci'] = $this->mdl->getCicilanAgenByID($id_agen);
                $data['so'] = $this->mdl->getPenjualanAgenByID($id_agen);
                $data['ag'] = $this->mdl->getAgenByID($id_agen);
                $data['statx'] = 'agen';
            }

            

            $data['sc'] = $this->mdl->getShoppingCart($id_sales);
            $data['tot'] = $this->mdl->getKasirTotal($id_sales);
        }

        // print_r($data['statx']); exit();

        $data['tipe'] = $tipe;
        $data['id'] = $id_sales;

        $this->load->view('nota',$data);
    }


    public function exportExcel($id_sales) {

        $data['list'] = $this->mdl->getListPajakProduct();
        
        $data['sc'] = $this->mdl->getShoppingCart($id_sales);
        $data['tot'] = $this->mdl->getKasirTotal($id_sales);
        $data['agen']   = $this->mdl->getAgenByID($data['sc'][0]->id_agen);

        $data['filename'] = "Transaksi " . $data['agen'][0]->nama_agen." / ".$data['sc'][0]->id_sales;



        $this->load->view('export',$data);

    }


    public function hapusNota() {

        $tipe = $this->input->post('tipe');

        if($tipe == 'po') {

            $id_purchase = $this->input->post('id_purchase');
            $purchase = $this->mdl->getShoppingCartPurchase($id_purchase);

            if(count($purchase) == 0) {
                $purchase = $this->mdl->getPurchaseByID($id_purchase);
            }

            $id_finance = $purchase[0]->id_finance;
         
            $this->mdl->deleteData('id_purchase', $id_purchase, 'purchase_order');
            $this->mdl->deleteData('id_purchase', $id_purchase, 'line_purchase');
            $this->mdl->deleteData('id_finance', $id_finance, 'finance');


            for ($i=0; $i < count($purchase) ; $i++) { 
                $id_inventory = $purchase[$i]->id_inventory;
                $this->mdl->deleteData('id_inventory',$id_inventory,'inventory');  
            }

            $pesan = "<b>Berhasil Menghapus Transaksi</b>";
            $warna = "success";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            
            redirect('Apps/pembelian');
            


        } else {

            $id_sales = $this->input->post('id_sales');
            $sales = $this->mdl->getShoppingCart($id_sales);

            if(count($sales) == 0) {
                $sales = $this->mdl->getSalesByID($id_sales);                
            }

            $id_finance = $sales[0]->id_finance;

            // print_r($id_finance); exit();
         
            $this->mdl->deleteData('id_sales', $id_sales, 'sales_order');
            $this->mdl->deleteData('id_sales', $id_sales, 'line_sales');
            $this->mdl->deleteData('id_finance', $id_finance, 'finance');


            for ($i=0; $i < count($sales) ; $i++) { 
                $id_inventory = $sales[$i]->id_inventory;
                $this->mdl->deleteData('id_inventory',$id_inventory,'inventory');  
            }

            $pesan = "<b>Berhasil Menghapus Transaksi</b>";
            $warna = "success";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            
            redirect('Apps/penjualan');

        }

        
        
        

    }

    public function redirectso($id_agen) {

        $stat = $this->mdl->getLastSalesStatus('so');

        if($stat == 'close') {

            $data = array(

                'status' => 'open',
                'tipe' => 'so',
                'id_agen' => $id_agen

            );

            $id_sales = $this->mdl->insertDataGetLast('sales_order',$data);

            $data = array(

                'tipe_transaction' => 'so',
                'pajak' => 0,
                'id_people' => $id_agen,
                'id_transaction' => $id_sales

            );

            $id_finance = $this->mdl->insertDataGetLast('finance',$data);

            $data = array(

                'id_finance' => $id_finance

            );

            $this->mdl->updateData('id_sales',$id_sales,'sales_order',$data);

            redirect('Apps/tambahSO/new/'.$id_sales);  

        } else {

            $id_sales = $this->mdl->getLastSalesID('so');
            redirect('Apps/tambahSO/old/'.$id_sales);  

        }

    }

    public function tambahSO($tipe,$id_sales) {
        
        $data['id'] = $id_sales;
        $data['tipe'] = $tipe;

        $sales = $this->mdl->getSalesByID($id_sales);
        $id_agen = $sales[0]->id_agen;
        
        $data['so'] = $this->mdl->getPenjualanAgenByID($id_agen);
        $data['ci'] = $this->mdl->getCicilanAgenByID($id_agen);

        // $data['produk']   = $this->mdl->getProduk();
        $data['agen']   = $this->mdl->getAgenByID($id_agen);
        $data['sc'] = $this->mdl->getShoppingCart($id_sales);
        $data['tot'] = $this->mdl->getKasirTotal($id_sales);

        $data['hrg'] = $this->mdl->getHargaSpesial();

        $this->load->view('tambahSO',$data);
    }

    public function newTambahProduk() {

        $id_produk = $this->input->post('id_produk');
        $id_sales = $this->input->post('id_sales');
        $qty = $this->input->post('qty');
        $ops = 'tambah';

        $produk = $this->mdl->getProdukByID($id_produk);
        $sales = $this->mdl->getSalesByID($id_sales);
        $id_agen = $sales[0]->id_agen;
        $agen = $this->mdl->getAgenByID($id_agen);
        $tipe_agen = $agen[0]->kategori;

        $harga_dipake = $produk[0]->harga_agen;

        if($tipe_agen == 'khusus') {

            $harga_dipake = $produk[0]->harga_khusus;

        }

        $spesial = $this->mdl->getHargaSpesialDetail($id_produk, $id_agen);

        if(count($spesial) > 0) {

            $harga_dipake = $spesial[0]->harga;

        }


        $data = array(

            'id_sales' => $id_sales,
            'id_produk' => $id_produk,
            'harga' => $harga_dipake,

        );

        $id_line = $this->mdl->insertDataGetLast('line_sales',$data);

        $data = array(

            'id_line' => $id_line,
            'id_produk' => $id_produk,
            'qty' => $qty,
            'direction' => 'out',
            'tipe_inventory' => 'so'

        );

        $id_inventory = $this->mdl->insertDataGetLast('inventory',$data);

        $data = array(

            'id_inventory' => $id_inventory

        );

        $this->mdl->updateData('id_line',$id_line,'line_sales',$data);


        $res = $this->mdl->getStockProduct($id_produk);
        $set = $this->mdl->getSetting();
        $low = (int)$set[0]->value;
        $med = (int)$set[1]->value;
        $stock = (int)$res[0]->stockNow;

        if($stock <= $low) {

            $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
            $warna = "danger";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/tambahSo/ops/'.$id_sales.'');

        } else if($stock <= $med) {

            $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
            $warna = "warning";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/tambahSo/ops/'.$id_sales.'');

        } else {
            $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
            $warna = "success";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/tambahSo/ops/'.$id_sales.'#sc');
        }


    }

    public function operasiSO($ops, $id_sales, $id2){

        if($ops == 'tambah') {

            $produk = $this->mdl->getProdukByID($id2);
            $sales = $this->mdl->getSalesByID($id_sales);
            $id_agen = $sales[0]->id_agen;
            $agen = $this->mdl->getAgenByID($id_agen);
            $tipe_agen = $agen[0]->kategori;

            

            $harga_dipake = $produk[0]->harga_agen;

            if($tipe_agen == 'khusus') {

                $harga_dipake = $produk[0]->harga_khusus;

            }

            $spesial = $this->mdl->getHargaSpesialDetail($id2, $id_agen);

            if(count($spesial) > 0) {

                $harga_dipake = $spesial[0]->harga;

            }


            $data = array(

                'id_sales' => $id_sales,
                'id_produk' => $id2,
                'harga' => $harga_dipake,

            );

            $id_line = $this->mdl->insertDataGetLast('line_sales',$data);

            $data = array(

                'id_line' => $id_line,
                'id_produk' => $id2,
                'qty' => 1,
                'direction' => 'out',
                'tipe_inventory' => 'so'

            );

            $id_inventory = $this->mdl->insertDataGetLast('inventory',$data);

            $data = array(

                'id_inventory' => $id_inventory

            );

            $this->mdl->updateData('id_line',$id_line,'line_sales',$data);


            $res = $this->mdl->getStockProduct($id2);
            $set = $this->mdl->getSetting();
            $low = (int)$set[0]->value;
            $med = (int)$set[1]->value;
            $stock = (int)$res[0]->stockNow;

            if($stock <= $low) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
                $warna = "danger";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahSo/ops/'.$id_sales.'');

            } else if($stock <= $med) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
                $warna = "warning";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahSo/ops/'.$id_sales.'');

            } else {
                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
                $warna = "success";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahSo/ops/'.$id_sales.'#sc');
            }

        } else if($ops == 'inc') {
            
            $line = $this->mdl->getLineByID($id2);
            $jml = $line[0]->qty;
            $id_inventory = $line[0]->id_inventory;
            $data = array(

                'qty' => $jml+1

            );

            $this->mdl->updateData('id_inventory',$id_inventory,'inventory',$data);

            $id_produk = $line[0]->id_produk;
            $res = $this->mdl->getStockProduct($id_produk);
            $set = $this->mdl->getSetting();
            $low = (int)$set[0]->value;
            $med = (int)$set[1]->value;
            $stock = (int)$res[0]->stockNow;

            if($stock <= $low) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
                $warna = "danger";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahSo/ops/'.$id_sales.'');

            } else if($stock <= $med) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
                $warna = "warning";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahSo/ops/'.$id_sales.'');

            } else {
                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
                $warna = "success";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahSo/ops/'.$id_sales.'#sc');
            }
            

        } else if($ops == 'dec') {
            
            $line = $this->mdl->getLineByID($id2);
            $jml = $line[0]->qty;
            $id_inventory = $line[0]->id_inventory;
            $data = array(

                'qty' => $jml-1

            );

            $this->mdl->updateData('id_inventory',$id_inventory,'inventory',$data);
            
            $id_produk = $line[0]->id_produk;
            $res = $this->mdl->getStockProduct($id_produk);
            $set = $this->mdl->getSetting();
            $low = (int)$set[0]->value;
            $med = (int)$set[1]->value;
            $stock = (int)$res[0]->stockNow;

            if($stock <= $low) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
                $warna = "danger";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahSo/ops/'.$id_sales.'');

            } else if($stock <= $med) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
                $warna = "warning";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahSo/ops/'.$id_sales.'');

            } else {
                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
                $warna = "success";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahSo/ops/'.$id_sales.'#sc');
            }

        } else if($ops == 'delete') {

            $line = $this->mdl->getLineByID($id2);
            $id_inventory = $line[0]->id_inventory;

            $this->mdl->deleteData('id_line',$id2,'line_sales');
            $this->mdl->deleteData('id_inventory',$id_inventory,'inventory');

            $pesan = "<b>Berhasil Menghapus Produk</b>";
            $warna = "success";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/tambahSO/ops/'.$id_sales);
        }

    }

    public function setQty() {

        $id_line = $this->input->post('id_line');
        $id_sales = $this->input->post('id_sales');

        $line = $this->mdl->getLineByID($id_line);
        $id_inventory = $line[0]->id_inventory;

        $qty = $this->input->post('qty');

        $data = array(

            'qty' => $qty,

        );

        $this->mdl->updateData('id_inventory',$id_inventory,'inventory',$data);

        $id_produk = $line[0]->id_produk;
        $res = $this->mdl->getStockProduct($id_produk);
        $set = $this->mdl->getSetting();
        $low = (int)$set[0]->value;
        $med = (int)$set[1]->value;
        $stock = (int)$res[0]->stockNow;

        $tipe = 'tambahSo';

        if($this->input->post('tipe') == 'kasir') {
            $tipe = 'kasir';
        }

        if($stock <= $low) {

            $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
            $warna = "danger";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/'.$tipe.'/ops/'.$id_sales.'');

        } else if($stock <= $med) {

            $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
            $warna = "warning";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/'.$tipe.'/ops/'.$id_sales.'');

        } else {
            $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
            $warna = "success";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/'.$tipe.'/ops/'.$id_sales.'#sc');
        }

    }

    public function setHargaProduk() {

        $id_line = $this->input->post('id_line');
        $id_sales = $this->input->post('id_sales');
        $dp = $this->input->post('dp');
        $harga = $this->input->post('harga');

        $data = array(

            'harga' => $harga,
            'diskon_produk' => $dp

        );

        $this->mdl->updateData('id_line',$id_line,'line_sales',$data);

        redirect('Apps/tambahSO/ops/'.$id_sales.'#sc');

    }

    public function tambahCicilan() {

        $id_agen = $this->input->post('id_agen');
        $bayar = $this->input->post('bayar');

        $data = array(

            'id_people' => $id_agen,
            'bayar' => $bayar,
            'tipe_transaction' => 'cicil'

        );

        $this->mdl->insertData('finance',$data);

        $pesan = "<b>Berhasil Menambahkan Cicilan</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        redirect('Apps/penjualan');

    }

    public function editFinance() {

        $id_finance = $this->input->post('id_finance');
        $bayar = $this->input->post('bayar');
        
        $data = array(

            'bayar' => $bayar,

        );

        $this->mdl->updateData('id_finance',$id_finance,'finance',$data);
        
        $pesan = "<b>Berhasil Mengedit Cicilan</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');

        if((int)$this->input->post('id_agen') > 0) {
            redirect('Apps/harga/'.$this->input->post('id_agen'));
        }
        
        redirect('Apps/penjualan');

    }

    public function hapusFinance() {

        $id_finance = $this->input->post('id_finance');
        
        $this->mdl->deleteData('id_finance', $id_finance, 'finance');
        
        $pesan = "<b>Berhasil Menghapus Cicilan</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/penjualan');

    }

    public function hapusCashOpname($id_finance) {

        
        $this->mdl->deleteData('id_finance', $id_finance, 'finance');
        
        $pesan = "<b>Berhasil Menghapus Cash Opname</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/cashflow');

    }

    public function pergerakan() {
        
        $data['mv'] = $this->mdl->getMovementSales();
        $data['pr'] = $this->mdl->getMovementPurchase();
        // $data['produk']   = $this->mdl->getProduk();
        $data['op']   = $this->mdl->getStockOpname();
    
        
        $this->load->view('pergerakan', $data);
    }

    public function stockOpname() {
        
        $id_produk = $this->input->post('id_produk');
        $direction = $this->input->post('direction');
        $qty = $this->input->post('qty');

        $data = array(

            'id_produk' => $id_produk,
            'qty' => $qty,
            'direction' => $direction,
            'tipe_inventory' => 'opname'

        );

        $this->mdl->insertData('inventory',$data);

        $pesan = "<b>Stock Opname Berhasil</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/pergerakan');
    }

    public function hapusInventory() {

        $id_inventory = $this->input->post('id_inventory');

        $this->mdl->deleteData('id_inventory', $id_inventory, 'inventory');
        
        $pesan = "<b>Berhasil Menghapus Stock Opname</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
        
        redirect('Apps/pergerakan');

    }

    public function stock() {

        $data['set'] = $this->mdl->getSetting();
        $data['st'] = $this->mdl->getStockList(30);

        // $data['tr'] = $this->mdl->getTrackProduk();

        $this->load->view('stock',$data);

    }

    public function stockCheck() {

        // $data['set'] = $this->mdl->getSetting();
        $st = $this->mdl->getStockList(9999);

        for ($i=0; $i < count($st) ; $i++) { 

            if((int)$st[$i]->stockNow<=0) {
                $dir = 'in';
            } else {
                $dir = 'out';
            }
            
            // echo $st[$i]->id_produk."|".$st[$i]->stockNow*(-1);
            echo "INSERT INTO `inventory` (`id_inventory`, `id_line`, `id_produk`, `qty`, `direction`, `tipe_inventory`, `tanggal`) VALUES (NULL, '0', '".$st[$i]->id_produk."', '".abs($st[$i]->stockNow)."', '".$dir."', 'opname', CURRENT_TIMESTAMP);";
            echo "<br>";
        }

        // $data['tr'] = $this->mdl->getTrackProduk();

        // $this->load->view('stock',$data);

    }

    public function redirectpo($id_supplier) {

        $stat = $this->mdl->getLastPurchaseStatus();

        if($stat == 'close') {

            $data = array(

                'status' => 'open',
                'tipe' => 'po',
                'id_supplier' => $id_supplier

            );

            $id_purchase = $this->mdl->insertDataGetLast('purchase_order',$data);

            $data = array(

                'tipe_transaction' => 'po',
                'pajak' => 0,
                'id_people' => $id_supplier,
                'id_transaction' => $id_purchase

            );

            $id_finance = $this->mdl->insertDataGetLast('finance',$data);

            $data = array(

                'id_finance' => $id_finance

            );

            $this->mdl->updateData('id_purchase',$id_purchase,'purchase_order',$data);

            redirect('Apps/tambahPO/new/'.$id_purchase);  

        } else {

            $id_purchase = $this->mdl->getLastPurchaseID();
            redirect('Apps/tambahPO/old/'.$id_purchase);  

        }

    }

    public function tambahPO($tipe,$id_purchase) {
        
        $data['id'] = $id_purchase;
        $data['tipe'] = $tipe;

        $purchase = $this->mdl->getPurchaseByID($id_purchase);
        $id_supplier = $purchase[0]->id_supplier;

        // $data['produk']   = $this->mdl->getProduk();
        $data['supplier']   = $this->mdl->getSupplierByID($id_supplier);

        $data['sc'] = $this->mdl->getShoppingCartPurchase($id_purchase);
        $data['tot'] = $this->mdl->getKasirTotalPurchase($id_purchase);

        $this->load->view('tambahPO',$data);
    }

    public function operasiPO($ops, $id_purchase, $id2){

        if($ops == 'tambah') {

            $produk = $this->mdl->getProdukByID($id2);
            $data = array(

                'id_purchase' => $id_purchase,
                'id_produk' => $id2,
                'harga' => $produk[0]->harga_beli,

            );

            $id_line = $this->mdl->insertDataGetLast('line_purchase',$data);

            $data = array(

                'id_line' => $id_line,
                'id_produk' => $id2,
                'qty' => 1,
                'direction' => 'in',
                'tipe_inventory' => 'po'

            );

            $id_inventory = $this->mdl->insertDataGetLast('inventory',$data);

            $data = array(

                'id_inventory' => $id_inventory

            );

            $this->mdl->updateData('id_line',$id_line,'line_purchase',$data);


            $res = $this->mdl->getStockProduct($id2);
            $set = $this->mdl->getSetting();
            $low = (int)$set[0]->value;
            $med = (int)$set[1]->value;
            $stock = (int)$res[0]->stockNow;

            if($stock <= $low) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
                $warna = "danger";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahPO/ops/'.$id_purchase.'');

            } else if($stock <= $med) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
                $warna = "warning";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahPO/ops/'.$id_purchase.'');

            } else {
                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
                $warna = "success";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahPO/ops/'.$id_purchase.'#sc');
            }

        } else if($ops == 'inc') {
            
            $line = $this->mdl->getlinePurchaseByID($id2);
            $jml = $line[0]->qty;
            $id_inventory = $line[0]->id_inventory;
            $data = array(

                'qty' => $jml+1

            );

            $this->mdl->updateData('id_inventory',$id_inventory,'inventory',$data);

            $id_produk = $line[0]->id_produk;
            $res = $this->mdl->getStockProduct($id_produk);
            $set = $this->mdl->getSetting();
            $low = (int)$set[0]->value;
            $med = (int)$set[1]->value;
            $stock = (int)$res[0]->stockNow;

            if($stock <= $low) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
                $warna = "danger";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahPO/ops/'.$id_purchase.'');

            } else if($stock <= $med) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
                $warna = "warning";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahPO/ops/'.$id_purchase.'');

            } else {
                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
                $warna = "success";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahPO/ops/'.$id_purchase.'#sc');
            }
            

        } else if($ops == 'dec') {
            
            $line = $this->mdl->getlinePurchaseByID($id2);
            $jml = $line[0]->qty;
            $id_inventory = $line[0]->id_inventory;
            $data = array(

                'qty' => $jml-1

            );

            $this->mdl->updateData('id_inventory',$id_inventory,'inventory',$data);
            
            $id_produk = $line[0]->id_produk;
            $res = $this->mdl->getStockProduct($id_produk);
            $set = $this->mdl->getSetting();
            $low = (int)$set[0]->value;
            $med = (int)$set[1]->value;
            $stock = (int)$res[0]->stockNow;

            if($stock <= $low) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
                $warna = "danger";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahPO/ops/'.$id_purchase.'');

            } else if($stock <= $med) {

                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
                $warna = "warning";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahPO/ops/'.$id_purchase.'');

            } else {
                $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
                $warna = "success";
                $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
                redirect('Apps/tambahPO/ops/'.$id_purchase.'#sc');
            }

        } else if($ops == 'delete') {

            $line = $this->mdl->getlinePurchaseByID($id2);
            $id_inventory = $line[0]->id_inventory;

            $this->mdl->deleteData('id_line',$id2,'line_purchase');
            $this->mdl->deleteData('id_inventory',$id_inventory,'inventory');

            $pesan = "<b>Berhasil Menghapus Produk</b>";
            $warna = "success";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/tambahPO/ops/'.$id_purchase);
        }

    }

    public function setQtyPurchase() {

        $id_line = $this->input->post('id_line');
        $id_purchase = $this->input->post('id_purchase');

        $line = $this->mdl->getLinePurchaseByID($id_line);
        $id_inventory = $line[0]->id_inventory;

        $qty = $this->input->post('qty');

        $data = array(

            'qty' => $qty,

        );

        $this->mdl->updateData('id_inventory',$id_inventory,'inventory',$data);

        $id_produk = $line[0]->id_produk;
        $res = $this->mdl->getStockProduct($id_produk);
        $set = $this->mdl->getSetting();
        $low = (int)$set[0]->value;
        $med = (int)$set[1]->value;
        $stock = (int)$res[0]->stockNow;

        if($stock <= $low) {

            $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, segera lakukan re-stock produk secepatnya  <span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span><span class='fa fa-warning pull-right'></span>";
            $warna = "danger";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/tambahPO/ops/'.$id_purchase.'');

        } else if($stock <= $med) {

            $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>, produk akan habis dalam waktu dekat, silahkan melakukan re-stock barang <span class='fa fa-warning pull-right'></span>";
            $warna = "warning";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/tambahPO/ops/'.$id_purchase.'');

        } else {
            $pesan = "Berhasil Menambahkan Produk, Stock produk saat ini adalah <b>".$stock."</b>";
            $warna = "success";
            $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');
            redirect('Apps/tambahPO/ops/'.$id_purchase.'#sc');
        }

    }

    public function setHargaProdukPurchase() {

        $id_line = $this->input->post('id_line');
        $id_purchase = $this->input->post('id_purchase');
        $dp = $this->input->post('dp');
        $harga = $this->input->post('harga');

        $data = array(

            'harga' => $harga,
            'diskon_produk' => $dp

        );

        $this->mdl->updateData('id_line',$id_line,'line_purchase',$data);

        redirect('Apps/tambahPO/ops/'.$id_purchase.'#sc');

    }

    public function simpanKasirPurchase() {
        
        $bayar = $this->input->post('pages');
        $total = $this->input->post('total');
        $id_purchase = $this->input->post('id_purchase');
        $id_finance = $this->input->post('id_finance');

        $kembalian = 0;
        $tipe = 'kasir';

        if($this->input->post('kembalian') !== NULL ) {

            $kembalian = $this->input->post('kembalian');
            $tipe = 'po';

        }

        $data = array(

            'status' => 'close',

        );

        $this->mdl->updateData('id_purchase',$id_purchase,'purchase_order',$data);

        $data = array(

            'bayar' => $bayar,
            'kembalian' => $kembalian

        );



        $this->mdl->updateData('id_finance',$id_finance,'finance',$data);

        redirect('Apps/nota/'.$tipe.'/'.$id_purchase);
    }

    public function setDiscountPurchase() {

        $id_purchase = $this->input->post('id_purchase');
        $id_finance = $this->input->post('id_finance');
        $discount = $this->input->post('discount');
        $keterangan = $this->input->post('keterangan');

        $tipe = $this->input->post('tipe');

        $data = array(

            'ket_kasir' => $keterangan

        );

        $this->mdl->updateData('id_purchase',$id_purchase,'purchase_order',$data);

        $data = array(

            'discount' => $discount

        );

        $this->mdl->updateData('id_finance',$id_finance,'finance',$data);

        $pesan = "<b>Berhasil Menambahkan Discount</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');

        redirect('Apps/tambahPO/ops/'.$id_purchase);
    }

    public function setPajakPurchase() {

        $id_purchase = $this->input->post('id_purchase');
        $id_finance = $this->input->post('id_finance');
        $pajak = $this->input->post('pajak');
        $tipe = $this->input->post('tipe');

        $data = array(

            'pajak' => $pajak,

        );

        $this->mdl->updateData('id_finance',$id_finance,'finance',$data);

        $pesan = "<b>Berhasil Menambahkan Pajak</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');

        redirect('Apps/tambahPO/ops/'.$id_purchase);
    }

    public function cashflow() {
        $data['cf'] = $this->mdl->getCashFlow();
        
        $this->load->view('cashflow',$data);

    }

    public function cashOpname() {

        $data = array(

            'bayar' => $this->input->post('bayar'),
            'tipe_transaction' => $this->input->post('tipe_transaction'),

            

        );

        $this->mdl->insertData('finance',$data);

        $pesan = "<b>Berhasil Menambahkan Cash Opname</b>";
        $warna = "success";
        $this->session->set_flashdata('msg', '<div class="animated flash alert alert-' . $warna . '">' . $pesan . '</div>');

        redirect('Apps/cashflow');

    }

}   

?>