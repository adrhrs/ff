-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2019 at 08:21 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ff`
--

-- --------------------------------------------------------

--
-- Table structure for table `agen`
--

CREATE TABLE `agen` (
  `id_agen` int(11) NOT NULL,
  `nama_agen` varchar(1232) NOT NULL,
  `kategori` varchar(312) NOT NULL,
  `nomor` varchar(23) NOT NULL,
  `alamat` varchar(2312) NOT NULL,
  `npwp` bigint(231) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agen`
--

INSERT INTO `agen` (`id_agen`, `nama_agen`, `kategori`, `nomor`, `alamat`, `npwp`) VALUES
(1, 'Bu I''ah', 'biasa', '0', '0', 0),
(2, 'P. Heri Blora', 'biasa', '0', '0', 0),
(3, 'Bu Supeno', 'biasa', '0', '0', 0),
(4, 'Toko Saremas', 'biasa', '0', '0', 0),
(5, 'Toko Subur Senori', 'biasa', '0', '0', 0),
(6, 'Bu Ani', 'biasa', '0', '0', 0),
(7, 'Bu Is', 'biasa', '0', '0', 0),
(8, 'P. Suhud', 'biasa', '0', '0', 0),
(9, 'Bu Wiwin', 'biasa', '0', '0', 0),
(10, 'P. Rokib MU', 'biasa', '0', '0', 0),
(11, 'P. Ridwan', 'biasa', '0', '0', 0),
(12, 'Bu Su Rengel', 'biasa', '0', '0', 0),
(13, 'P. Kasmari', 'biasa', '0', '0', 0),
(14, 'P. Sumit', 'biasa', '0', '0', 0),
(15, 'P.Kanang', 'biasa', '0', '0', 0),
(16, 'Toko Rezeki', 'biasa', '0', '0', 0),
(17, 'P. Taman', 'biasa', '0', '0', 0),
(18, 'P. Yusuf Puan', 'biasa', '0', '0', 0),
(19, 'B. Yayuk MU', 'biasa', '0', '0', 0),
(20, 'B. Yus Plumpang', 'biasa', '0', '0', 0),
(21, 'B. Lurah', 'biasa', '0', '0', 0),
(22, 'B. Nur Pakuwon', 'biasa', '0', '0', 0),
(23, 'Aris Grabakan', 'biasa', '0', '0', 0),
(24, 'P. Yasin BJN', 'biasa', '0', '0', 0),
(25, 'B. Mardiyah', 'biasa', '0', '0', 0),
(26, 'P. Sukir', 'biasa', '0', '0', 0),
(27, 'Toko Tiga Jaya', 'biasa', '0', '0', 0),
(28, 'P. Salam MU', 'biasa', '0', '0', 0),
(29, 'Gimung', 'biasa', '0', '0', 0),
(30, 'B. Atik', 'biasa', '0', '0', 0),
(31, 'B. Utia', 'biasa', '0', '0', 0),
(32, 'B. Pas Ngrejeng', 'biasa', '0', '0', 0),
(33, 'P. Dar Montong', 'biasa', '0', '0', 0),
(34, 'P. Ari Mbeti', 'biasa', '0', '0', 0),
(35, 'P. Maftuin', 'biasa', '0', '0', 0),
(36, 'B. Tatik Tasmad', 'biasa', '0', '0', 0),
(37, 'Toko Sumber Rejeki', 'biasa', '0', '0', 0),
(38, 'B. Ning Pasar Pramuka', 'biasa', '0', '0', 0),
(39, 'Bu Lis Sukolilo', 'biasa', '0', '0', 0),
(40, 'P. Muhtadin', 'biasa', '0', '0', 0),
(41, 'Assalamu''alaikum Mart', 'biasa', '0', '0', 0),
(42, 'P. Kup', 'biasa', '0', '0', 0),
(43, 'P. Rusdi', 'biasa', '0', '0', 0),
(44, 'P. Romadhon Sarang', 'khusus', '0', '0', 0),
(45, 'P. Wo Kragan', 'khusus', '0', '0', 0),
(46, 'P. Ajik', 'khusus', '0', '0', 0),
(47, 'P. Nasir', 'khusus', '0', '0', 0),
(48, 'P. Iwan Saringembat', 'khusus', '0', '0', 0),
(49, 'Karunia Jaya Tk', 'khusus', '0', '0', 0),
(50, 'P. Takuin', 'khusus', '0', '0', 0),
(51, 'B. Tris Kalitidu', 'khusus', '0', '0', 0),
(52, 'B. Tik Plumpang', 'khusus', '0', '0', 0),
(53, 'P. Arifin Blora', 'khusus', '0', '0', 0),
(54, 'P. Gun Lasem', 'khusus', '0', '0', 0),
(55, 'B. Lis Cepu', 'khusus', '0', '0', 0),
(56, 'B. Endang BJN', 'khusus', '0', '0', 0),
(57, 'P. Rokib Dander', 'khusus', '0', '0', 0),
(58, 'Tambakboyo', 'khusus', '0', '0', 0),
(59, 'P. Sukir', 'khusus', '0', '0', 0),
(60, 'Mb. Kila', 'khusus', '0', '0', 0),
(61, 'Toko Murni', 'khusus', '0', '0', 0),
(62, 'P. Gun Kerek', 'khusus', '0', '0', 0),
(63, 'Alfi Minohorejo', 'khusus', '0', '0', 0),
(64, 'B. Bambang MU', 'khusus', '0', '0', 0),
(65, 'P. Oni', 'khusus', '0', '0', 0),
(66, 'B. Winda', 'khusus', '0', '0', 0),
(67, 'P. Salam MU', 'khusus', '0', '0', 0),
(68, 'Bu Eni SMP 2', 'khusus', '0', '0', 0),
(69, 'Tendik', 'khusus', '0', '0', 0),
(70, 'Toko Mahkota', 'khusus', '0', '0', 0),
(71, 'Toko Ratna', 'khusus', '0', '0', 0),
(72, 'B. Imam Pasar Pramuka', 'khusus', '0', '0', 0),
(73, 'Mb. Nur Pasar Pramuka', 'khusus', '0', '0', 0),
(74, 'P. Warno Blimbing', 'khusus', '0', '0', 0),
(75, 'B. Tatik', 'khusus', '0', '0', 0),
(76, 'P. Sundoyo', 'khusus', '0', '0', 0),
(77, 'B. Ah', 'khusus', '0', '0', 0),
(78, 'P. Edi Hemato', 'khusus', '0', '0', 0),
(79, 'Toko Monita', 'khusus', '0', '0', 0),
(80, 'B. Kasno Karang Indah', 'khusus', '0', '0', 0),
(81, 'Toko Nuansa', 'khusus', '0', '0', 0),
(82, 'Toko Aira MU', 'khusus', '0', '0', 0),
(83, 'Burger Sambong', 'khusus', '0', '0', 0),
(84, 'P. Imam Minaku', 'khusus', '0', '0', 0),
(85, 'P. Imam LMG', 'khusus', '0', '0', 0),
(86, 'Rifki Panyuran', 'khusus', '0', '0', 0),
(87, 'P. Mun Kulit', 'khusus', '0', '0', 0),
(88, 'Toko Dewa Jatirogo', 'khusus', '0', '0', 0),
(89, 'Toko Amanah Rengel', 'khusus', '0', '0', 0),
(90, 'Samudra Supermarket', 'khusus', '0', '0', 0),
(91, 'Ji Pasar Grodo', 'khusus', '0', '0', 0),
(92, 'Nur Aziz', 'khusus', '0', '0', 0),
(93, 'Bu Kasto BJN', 'khusus', '0', '0', 0),
(94, 'Kholis Kalitidu', 'khusus', '0', '0', 0),
(95, 'Saeful Padangan', 'khusus', '0', '0', 0),
(96, 'B. Hima Padangan', 'khusus', '0', '0', 0),
(97, 'P. Dikun', 'khusus', '0', '0', 0),
(98, 'P. Salam BJN', 'khusus', '0', '0', 0),
(99, 'P. Siswanto', 'khusus', '0', '0', 0),
(100, 'P. Haqi', 'khusus', '0', '0', 0),
(101, 'P. Nung', 'khusus', '0', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `finance`
--

CREATE TABLE `finance` (
  `id_finance` int(11) NOT NULL,
  `id_transaction` int(11) NOT NULL,
  `id_people` int(11) NOT NULL,
  `tipe_transaction` varchar(123) NOT NULL,
  `bayar` int(11) NOT NULL,
  `kembalian` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `pajak` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `harga_agen`
--

CREATE TABLE `harga_agen` (
  `id_harga` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_agen` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id_inventory` int(11) NOT NULL,
  `id_line` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `direction` varchar(123) NOT NULL,
  `tipe_inventory` varchar(123) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `line_purchase`
--

CREATE TABLE `line_purchase` (
  `id_line` int(11) NOT NULL,
  `id_purchase` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_inventory` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `diskon_produk` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `line_sales`
--

CREATE TABLE `line_sales` (
  `id_line` int(11) NOT NULL,
  `id_sales` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `id_inventory` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `diskon_produk` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(11) NOT NULL,
  `nama` varchar(1232) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_normal` int(11) NOT NULL,
  `harga_agen` int(11) NOT NULL,
  `harga_khusus` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `isi` int(11) NOT NULL,
  `keterangan` varchar(2123) NOT NULL,
  `tipe` varchar(23) NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_produsen` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id_produk`, `nama`, `harga_beli`, `harga_normal`, `harga_agen`, `harga_khusus`, `berat`, `isi`, `keterangan`, `tipe`, `added_on`, `id_produsen`) VALUES
(1, 'Scallop 500 Gr', 17972, 20000, 18800, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(2, 'Scallop 200 Gr', 7474, 9500, 8400, 0, 0, 50, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(3, 'Otak-Otak Isi 500 Gr', 15164, 18000, 16500, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(4, 'Kaki Naga 500 Gr', 12293, 15000, 13300, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(5, 'Foody Tempura 500 Gr', 11630, 15000, 13500, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(6, 'Foody Bintang 500 Gr', 11162, 15000, 0, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(7, 'Foody Ikan 500 Gr', 11162, 15000, 0, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(8, 'Bola Ikan 500 Gr', 18792, 23000, 21100, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(9, 'Spring Roll 250 Gr', 14640, 0, 0, 0, 0, 24, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(10, 'Siomay Ikan 400 Gr', 15716, 0, 0, 0, 0, 25, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(11, 'Fish Tofu 500 Gr', 15680, 0, 0, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(12, 'Fish Cake 500 Gr', 14210, 0, 0, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(13, 'Bola Udang 500 Gr', 22404, 28000, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(14, 'Seafood Samosa 250 Gr', 12348, 0, 0, 0, 0, 24, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(15, 'Crispy Deli 250 Gr', 14640, 0, 0, 0, 0, 24, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(16, 'Sisik Naga 500 Gr', 13612, 16500, 14800, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(17, 'Otak-Otak Geezy 0 Gr', 12250, 17000, 16000, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(18, 'Bola Kepiting 0 Gr', 22404, 28000, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(19, 'Bola Cumi 0 Gr', 22404, 28000, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(20, 'Bola Lobster 0 Gr', 22404, 28000, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(21, 'Crab Stick Mini 1000 Gr', 30380, 36000, 0, 0, 0, 6, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(22, 'Mini Chikuwa Daitsabu 1000 Gr', 35983, 45000, 0, 0, 0, 6, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(23, 'Fish Roll Otak Singapore 1000 Gr', 31360, 39000, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(24, 'Bola Salmon 500 Gr', 22404, 28000, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 6),
(25, 'Kentang Los 2,5Kg', 50000, 75000, 72500, 0, 0, 5, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 7),
(26, 'Kentang Los 2 Kg', 40000, 60000, 0, 0, 0, 6, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 7),
(27, 'Chop Chop Short 700 Gr', 19377, 22000, 21000, 0, 0, 18, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 8),
(28, 'Jofrans Bakso Sapi 500 Gr', 25190, 0, 0, 0, 0, 15, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 8),
(29, 'Jofrans Bratwust Sapi 500 Gr', 24914, 32000, 0, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 8),
(30, 'Tempura 500 Gr', 8500, 9500, 9000, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 9),
(31, 'Sukoy 0 Gr', 9250, 10250, 9750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 9),
(32, 'Scallops 0 Gr', 9250, 10250, 9750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 9),
(33, 'B. Tuna 0 Gr', 7250, 8500, 8000, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 9),
(34, 'Telur Naga 0 Gr', 7250, 8500, 8000, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 9),
(35, 'Tempura Burger 0 Gr', 9000, 10000, 9500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 9),
(36, 'Bintang 0 Gr', 8750, 0, 9250, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 9),
(37, 'Ooye N. Ayam 500 Gr', 13359, 15500, 14500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 10),
(38, 'Ooye N. Stik 500 Gr', 13362, 15500, 14000, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 10),
(39, 'Ooye Sosis Ayam 500 Gr', 12901, 15500, 14000, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 10),
(40, 'B. Go', 8300, 10000, 9050, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 11),
(41, 'Otak Otak Go', 9000, 11500, 10500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 11),
(42, 'Scallop Go', 9300, 12500, 11000, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 11),
(43, 'Hemato N. Ayam 500 Gr', 14013, 16000, 14750, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 12),
(44, 'Hemato Basis 21 pc 500 Gr', 12603, 15000, 13850, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 12),
(45, 'Hemato Basis Merah 21 pc 500 Gr', 12603, 15000, 13850, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 12),
(46, 'Hemato Sosis Sapi 15 pc 500 Gr', 13543, 16000, 14700, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 12),
(47, 'Hemato Burger Sapi Besar 0 Gr', 6301, 8500, 7800, 0, 0, 32, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 12),
(48, 'Hemato Burger Sapi Kecil 0 Gr', 7148, 9500, 8800, 0, 0, 25, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 12),
(49, 'Hemato N. Ayam 1000 Gr', 27933, 31000, 30000, 0, 0, 6, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 12),
(50, 'Yona Sosis Bakar', 25394, 32000, 0, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 13),
(51, 'Yona Beef Patties 500 Gr', 28309, 32000, 0, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 13),
(52, 'Yona Sosis Sapi 15 Pc 450 Gr', 27463, 0, 0, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 13),
(53, 'Top M 750 G', 20443, 23000, 21750, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 14),
(54, 'Top M 500 Gr ', 13698, 16000, 14750, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 14),
(55, 'Top M Long 500 Gr', 13698, 16000, 14750, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 14),
(56, 'Top C 500 Gr', 14326, 16500, 14750, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 14),
(57, 'Scallop Top 500 Gr', 15685, 18000, 17000, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 14),
(58, 'B. Top K 0 Gr', 13803, 15000, 13750, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 14),
(59, 'B. Top B 0 Gr', 12548, 16000, 15000, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 14),
(60, 'Tempura Ngetop 0 Gr', 15162, 18000, 17000, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 14),
(61, 'Kornet 0 Gr', 12862, 15500, 15000, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 14),
(62, 'Ngetop Stickie 0 Gr', 14639, 0, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 14),
(63, 'Mantao 0 gr', 5900, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(64, 'Lumpia 0 gr', 5900, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(65, 'Martabak 0 gr', 5900, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(66, 'Siomay 0 gr', 5900, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(67, 'Tofu 0 gr', 5900, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(68, 'T. Puyuh 0 gr', 5900, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(69, 'Roti Paha 0 gr', 5900, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(70, 'Sate Cireng 0 gr', 5900, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(71, 'Bakpao 0 gr', 5900, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(72, 'Stik Mie 0 gr', 5900, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(73, 'P. Ndeso 0 gr', 6000, 7000, 6750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(74, 'B.Djogja 0 gr', 6000, 7000, 6750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(75, 'Basgor 0 gr', 6000, 7000, 6750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(76, 'Apollo  0 gr', 5500, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(77, 'Sate Dora 0 gr', 5500, 7000, 6750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(78, 'Stik Udang 0 gr', 5500, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(79, 'Love 0 gr', 5500, 7500, 6750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(80, 'Doraemon 0 gr', 5500, 7500, 6750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(81, 'Es Horizon 0 gr', 6150, 8000, 7250, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(82, 'Nugget Star 0 gr', 0, 7500, 6750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(83, 'B. Ikan Horizon 0 gr', 0, 9000, 8250, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(84, 'Otak Otak Horizon 0 gr', 0, 8500, 7250, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(85, 'Scallop Horizon 0 gr', 6000, 8000, 7000, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(86, 'Paha 0 gr', 5750, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(87, 'Ktq 0 gr', 5750, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(88, 'Tahu 0 gr', 5750, 7500, 6750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(89, 'Pizza 0 gr', 5750, 7500, 6750, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(90, 'Sate A1 0 gr', 5750, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(91, 'Sate Kabita 0 gr', 6000, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(92, 'B. Kriuk 0 gr', 6050, 7000, 6700, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(93, 'Cireng Besar 0 gr', 6400, 8000, 7500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(94, 'Cireng Kecil 0 gr', 5500, 7000, 6000, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(95, 'Kulit 0 gr', 33000, 2500, 40000, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(96, 'Stik Ajaib 0 gr', 5400, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(97, 'Mie Pentung 0 gr', 5500, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(98, 'Sos Mie 0 gr', 5400, 7000, 6200, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(99, 'Udang Gulung 0 gr', 5500, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(100, 'Rosis 0 gr', 5500, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(101, 'Cireng Besar Selma 0 gr', 6300, 8000, 7250, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(102, 'Cireng Kecil Selma 0 gr', 4900, 7000, 6000, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(103, 'Papeda 0 gr', 5750, 7000, 6500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(104, 'Tusuk Panjang 0 gr', 57000, 62000, 0, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(105, 'Tusuk Pendek 0 gr', 59000, 64000, 0, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 15),
(106, 'Chicken Sausage 375 Gr', 12940, 16000, 14500, 0, 0, 15, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(107, 'Chicken Sausage 150 Gr', 6024, 7500, 6750, 0, 0, 25, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(108, 'Chicken Sausage 1000 Gr', 32796, 40500, 36750, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(109, 'Chicken Meatball 500 Gr', 20659, 25500, 23150, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(110, 'Chicken Meatball 200 Gr', 8969, 11000, 10050, 0, 0, 25, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(111, 'Chicken Sausage 3Pc 75 Gr', 3168, 3900, 3550, 0, 0, 80, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(112, 'Beef Sausage Serbaguna 150 Gr', 10709, 13500, 12000, 0, 0, 45, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(113, 'Beef Sausage Serbaguna 375 Gr', 24586, 30500, 27550, 0, 0, 24, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(114, 'Beef Sausage Goreng 150 Gr', 9571, 0, 10725, 0, 0, 25, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(115, 'Beed Sausage Goreng 375 Gr', 21485, 0, 24075, 0, 0, 15, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(116, 'Champ Nugget Abc 500 Gr', 25835, 32000, 28950, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(117, 'Champ Nugget Abc 250 Gr', 13341, 16500, 14950, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(118, 'Nugget Champ 250 Gr', 14546, 18000, 16300, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(119, 'Nugget Champ 500 Gr', 27888, 34500, 31250, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(120, 'Nugget Champ 1000Gr', 51045, 63000, 57200, 0, 0, 5, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(121, 'Champ Chicken Stick 250 Gr', 14546, 18000, 16300, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(122, 'Champ Chicken Stick 500 Gr', 27888, 34500, 31250, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(123, 'Champ Chicken Stick 1000 Gr', 51045, 63000, 57200, 0, 0, 5, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(124, 'Champ Nugget Coin 250 Gr', 13341, 16500, 14950, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(125, 'Champ Nugget Coin 500 Gr', 25835, 32000, 28950, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(126, 'Champ Nugget Jagung 500 Gr', 27196, 0, 30475, 0, 0, 11, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(127, 'Champ Nugget Sosis 500 Gr', 27196, 0, 30475, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(128, 'Champ Burger 315 Gr', 13163, 16500, 14750, 0, 0, 13, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 16),
(129, 'Akumo Chicken Nugget 250 Gr', 9861, 12500, 11050, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 17),
(130, 'Akumo Chicken Nugget 500 Gr', 18897, 24000, 21175, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 17),
(131, 'Akumo Chicken Stick 250 Gr', 9861, 12500, 11050, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 17),
(132, 'Akumo Chicken Stick 500 Gr', 18897, 24000, 21175, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 17),
(133, 'Stik Okey 500 Gr', 16509, 19500, 18500, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 18),
(134, 'Nugget Okey 500 Gr', 16509, 19500, 18500, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 18),
(135, 'Sosis Okey 500 Gr', 15171, 18000, 17000, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 18),
(136, 'Sosis Okey 1000 Gr', 29895, 35000, 33500, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 18),
(137, 'Ashimo 500 Gr', 13208, 15500, 14800, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 18),
(138, 'Ashimo 1 Kg', 25433, 30000, 28500, 0, 0, 0, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 18),
(139, 'Shoestring 500 Gr', 13074, 16000, 14650, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 20),
(140, 'Shoestring 1000 Gr', 24943, 31000, 27950, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 20),
(141, 'Shoestring 2,5Kg', 45267, 0, 50725, 0, 0, 4, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 20),
(142, 'Straight Cut 500 Gr', 16242, 0, 18200, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 20),
(143, 'Straight Cut 1000 Gr', 31100, 0, 34850, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 20),
(144, 'Batter Coated 500 Gr', 20257, 0, 22700, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 20),
(145, 'Batter Coated 1000 Gr', 34826, 0, 39025, 0, 0, 12, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 20),
(146, 'Karage 500 Gr', 37436, 41950, 46000, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(147, 'Karage 250 Gr', 20838, 23350, 26000, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(148, 'Schnitzel 500 Gr', 38217, 42825, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(149, 'Nugget Fiesta 500 Gr', 34625, 38800, 43000, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(150, 'Nugget Fiesta 250 Gr', 18607, 20850, 23000, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(151, 'Stikie 500 Gr', 34625, 38800, 43000, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(152, 'Stikie 250 Gr', 18607, 20850, 23000, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(153, 'Fried Chicken 500 Gr', 31903, 35750, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(154, 'Spicy Chick 500 Gr', 35027, 39250, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(155, 'Spicy Wing 500 Gr', 42567, 47700, 52500, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(156, 'Nugget Happy Star 500 Gr', 36544, 40950, 45000, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(157, 'Nugget Happy Star 250 Gr', 18785, 21050, 23000, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(158, 'Nugget Dino 500 Gr', 36499, 40900, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(159, 'Chicken Pok Pok 500 Gr', 37704, 42250, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(160, 'Crispy Crunch 300 Gr', 17982, 20150, 22000, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(161, 'Chicken Tofu 500 Gr', 29338, 32875, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(162, 'Nugget Cheese 123 500 Gr', 34714, 38900, 43000, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(163, 'Nugget Cheese 123 250 Gr', 18562, 20800, 23000, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(164, 'Nugget Pizzabc 500 Gr', 34714, 38900, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(165, 'Nugget Zoo 500 Gr', 34714, 38900, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(166, 'Delistripe 500 Gr', 47543, 53275, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(167, 'Cheesy Chick.With Broccoli 500 Gr', 44932, 50350, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(168, 'Keecho 500 Gr', 42144, 47225, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(169, 'Cheesy Lover 500 Gr', 44932, 50350, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(170, 'Fiesta Crispy Burger 360 Gr', 24920, 27925, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(171, 'Chicken Bolognese 200 Gr', 13498, 15125, 0, 0, 0, 20, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(172, 'Mixed Vegetables Nugget 500 Gr', 44932, 50350, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(173, 'Macaroni Schotel 375 Gr', 19387, 21725, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(174, 'Chicken Loaf 500 Gr', 38976, 43675, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19),
(175, 'Honey Mustard 500 Gr', 43014, 48200, 0, 0, 0, 10, 'Tidak Ada Keterangan', 'produk', '2019-02-02 07:11:27', 19);

-- --------------------------------------------------------

--
-- Table structure for table `produsen`
--

CREATE TABLE `produsen` (
  `id_produsen` int(11) NOT NULL,
  `nama_produsen` varchar(1232) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `produsen`
--

INSERT INTO `produsen` (`id_produsen`, `nama_produsen`) VALUES
(6, 'Minaku'),
(7, 'Boga Matahari'),
(8, 'Adil Mart'),
(9, 'ILM'),
(10, 'Ooye'),
(11, 'Artha Abadi'),
(12, 'Hemato'),
(13, 'Yona'),
(14, 'Wonokoyo'),
(15, 'Home Industries'),
(16, 'Champ'),
(17, 'Akumo'),
(18, 'Okey'),
(19, 'Fiesta'),
(20, 'Fiesta French Fries');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id_purchase` int(11) NOT NULL,
  `id_supplier` int(11) NOT NULL,
  `id_finance` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(123) NOT NULL,
  `ket_kasir` varchar(2312) NOT NULL,
  `tipe` varchar(312) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_order`
--

CREATE TABLE `sales_order` (
  `id_sales` int(11) NOT NULL,
  `id_agen` int(11) NOT NULL,
  `id_finance` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(123) NOT NULL,
  `ket_kasir` varchar(2312) NOT NULL,
  `tipe` varchar(312) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL,
  `nama_setting` varchar(232) NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setting`
--

INSERT INTO `setting` (`id_setting`, `nama_setting`, `value`) VALUES
(1, 'low_stock', 10),
(2, 'med_stock', 50);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `id_supplier` int(11) NOT NULL,
  `nama_supplier` varchar(1232) NOT NULL,
  `nomor` varchar(23) NOT NULL,
  `alamat` varchar(2312) NOT NULL,
  `npwp` bigint(231) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`id_supplier`, `nama_supplier`, `nomor`, `alamat`, `npwp`) VALUES
(1, 'PT Minaku', '0', '0', 0),
(2, 'UD. Boga Matahari', '0', '0', 0),
(3, 'PT. Adil Mart', '0', '0', 0),
(4, 'PT ILM', '0', '0', 0),
(5, 'PT Sera Food', '0', '0', 0),
(6, 'CV. Anugrah Artha Abadi', '0', '0', 0),
(7, 'PT Dagsap Endura Eature', '0', '0', 0),
(8, 'PT Wonokoyo', '0', '0', 0),
(9, 'Home Industri', '0', '0', 0),
(10, 'Produk', '0', '0', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(123) NOT NULL,
  `password` varchar(221) NOT NULL,
  `nama` varchar(231) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `nama`) VALUES
(1, 'admin', 'asd', 'Administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agen`
--
ALTER TABLE `agen`
  ADD PRIMARY KEY (`id_agen`);

--
-- Indexes for table `finance`
--
ALTER TABLE `finance`
  ADD PRIMARY KEY (`id_finance`);

--
-- Indexes for table `harga_agen`
--
ALTER TABLE `harga_agen`
  ADD PRIMARY KEY (`id_harga`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id_inventory`);

--
-- Indexes for table `line_purchase`
--
ALTER TABLE `line_purchase`
  ADD PRIMARY KEY (`id_line`);

--
-- Indexes for table `line_sales`
--
ALTER TABLE `line_sales`
  ADD PRIMARY KEY (`id_line`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`);

--
-- Indexes for table `produsen`
--
ALTER TABLE `produsen`
  ADD PRIMARY KEY (`id_produsen`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id_purchase`);

--
-- Indexes for table `sales_order`
--
ALTER TABLE `sales_order`
  ADD PRIMARY KEY (`id_sales`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agen`
--
ALTER TABLE `agen`
  MODIFY `id_agen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `finance`
--
ALTER TABLE `finance`
  MODIFY `id_finance` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;
--
-- AUTO_INCREMENT for table `harga_agen`
--
ALTER TABLE `harga_agen`
  MODIFY `id_harga` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id_inventory` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `line_purchase`
--
ALTER TABLE `line_purchase`
  MODIFY `id_line` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `line_sales`
--
ALTER TABLE `line_sales`
  MODIFY `id_line` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1005;
--
-- AUTO_INCREMENT for table `produsen`
--
ALTER TABLE `produsen`
  MODIFY `id_produsen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id_purchase` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sales_order`
--
ALTER TABLE `sales_order`
  MODIFY `id_sales` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `id_supplier` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
